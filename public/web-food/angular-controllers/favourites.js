 
//========================================================== FAVURITE CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('favouriteController', function($http,$scope,$window) {
		     
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/favourite-store?include_count_blocks=true',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			$scope.favourite =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 

 
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
 
 
  
  
		
	});



 
