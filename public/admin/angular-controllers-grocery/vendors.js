 
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userController', function ($http,$scope,$window,toastr) {
		
        var pro = this; 
        $('#loading').css('display', 'block'); 
		

					console.log("URL - GET - "+APP_URL+'/api/v1/users?user_type=3' );
		     


		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=3&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {

							console.log("URL - DELETE - "+APP_URL+'/api/v1/users/'+$scope.user_id );
	 

var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id+'&auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
					console.log("ERROR RESPONSE="+JSON.stringify(data)); 
			         
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		

					console.log("URL - GET - "+api_url+'&user_type=3');
		     


		var request = $http({
            method: "GET",
            url: api_url+'&user_type=3&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		
	
 	});
	
	
	
	
	
 	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr) {
 
 var auth_app_type = document.getElementById('auth_app_type').value;
	     // get basic form to add product ============================================================== clean done
	     
 
         $('#loading').css('display', 'block');
		  

		  			console.log("URL - GET - "+APP_URL+'/v1/form/user?user_type=3&auth_app_type='+auth_app_type );
		  

		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=3&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  




		
		  
		
		
		
		   // FUNCTION FOR STORE USER  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 
		   
		    for(var i=0;i<$scope.addUser[0].fields.length;i++){ 
			     if($scope.addUser[0].fields[i].identifier == 'user_type'){
				    $scope.addUser[0].fields[i].value = '3';
			     } 
              }
			   var photo = $('#photo').val();
 
 for(var i=0;i<$scope.addUser[0].fields.length;i++){
 if($scope.addUser[0].fields[i].identifier == 'photo'){
	 $scope.addUser[0].fields[i].value = photo;
	  // document.getElementById('preview_image').src = APP_URL+'/images/items/'+item_photo;
	   //console.log("this is deepu path - "+APP_URL+'/images/items/' + $('#item_photo').val(data));
 } 
 }



 			console.log("URL - POST - "+APP_URL+'/api/v1/form/user?auth_app_type='+auth_app_type);
		    console.log("POST DATA - "+JSON.stringify( $scope.addUser ));
			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_app_type='+auth_app_type,
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            $scope.data  = data;
            $('#loading').css('display', 'none');
			    

			        if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
					$scope.customer_id = data.data.user_id
			  $window.location.href = 'vendor/'+$scope.customer_id;
					}
            else { toastr.error(data.message, 'Error'); return false; }


			 
			
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
		   };			   
		  
	});



 
	

	
//=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userEditController', function ($http, $scope, $window,toastr) {
 
 var auth_app_type = document.getElementById('auth_app_type').value;
 
  $scope.customer_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
	 
	        $('#loading').css('display', 'block');
 
 
		  

		  			console.log("URL - GET - "+APP_URL+'/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type+'&user_type=3' );
 


		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type+'&user_type=3',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
        	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.editUser  = data;
            $('#loading').css('display', 'none');  
			 for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
			 }
        })
		.error(function (data, status, headers, config) {
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  


		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){
			    
				
				 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
			  



			  			console.log("URL - PUT - "+APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type );
		    console.log("POST DATA - "+JSON.stringify( $scope.editUser));


			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            $scope.data  = data; 
           

               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
            $('#loading').css('display', 'none');    
			 
        })
		.error(function (data, status, headers, config) {  	
		console.log("ERROR RESPONSE="+JSON.stringify(data)); 
             toastr.error(data.message, 'Error');
		      
        });  
 
		   };			   
		   
		   
		   
		   //==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };


							console.log("URL - POST - "+APP_URL+'/api/v1/users/update-password/'+$scope.customer_id );
		    console.log("POST DATA - "+JSON.stringify( $scope.passwordValue ));

		    var auth_app_type = document.getElementById('auth_app_type').value;
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.customer_id+'?auth_app_type='+auth_app_type,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
		   
			}
           };
	});

