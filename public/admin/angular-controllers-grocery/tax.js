
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('taxController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		

					console.log("URL - GET - "+APP_URL+'/api/v1/tax' );
		 


		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/tax?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.tax = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		 /*=============================FUNCTION FOR UPDATE TAX=============================================================*/
		pro.updateTax = function(index,tax_id){ 
			var auth_app_type = document.getElementById('auth_app_type').value;
		    
		    pro.tax_id = tax_id; 
			pro.title = document.getElementsByName('title' + index + tax_id)[0].value;
			pro.percentage = document.getElementsByName('percentage' + index + tax_id)[0].value;
			 
			pro.taxData = { "title": pro.title ,  "percentage": pro.percentage }; 


			console.log("URL - PUT - "+APP_URL+'/api/v1/tax/'+pro.tax_id );
		    console.log("POST DATA - "+JSON.stringify( pro.taxData ));

		    
			var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/tax/'+pro.tax_id+'&auth_app_type='+auth_app_type,
            data: pro.taxData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
            $('#loading').css('display', 'none');  
		
            if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
			 location.reload();		
			 
        })
		.error(function (data, status, headers, config) { 
		 toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		}
		 
   });