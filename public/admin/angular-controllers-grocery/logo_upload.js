
	
	
	//======================================================LOGO UPLOAD CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('logoController', function ($http,$scope,$window,toastr) {
		
		var auth_app_type = document.getElementById('auth_app_type').value;
		
		
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/setting',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.settingsData  = data;
			for(var i=0;i<=$scope.settingsData.data.length;i++){
				if($scope.settingsData.data[i].key_title == 'business_logo'){
					var logo = $scope.settingsData.data[i].key_value;
					var id = $scope.settingsData.data[i].id;
					$scope.business_logo_value = logo;
                     $scope.setting_id = id;					
				}
			}
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
 
  
		
		 
 
 $scope.upload_logo = function(){
	 
	 $scope.logo_image = $('#item_photo').val(); 
	 $scope.key_id = $scope.setting_id; 
	 $scope.key_value = $scope.logo_image;
	 $scope.key_data = {"key_value":$scope.key_value};
	 console.log(JSON.stringify($scope.key_data)); 
	 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/setting/'+$scope.key_id,
            data:  $scope.key_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;

			  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }

            $('#loading').css('display', 'none');  
			//location.reload();
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
	 
 }
		
		
 	});
	
	
	
	
	
	
 
 
 
