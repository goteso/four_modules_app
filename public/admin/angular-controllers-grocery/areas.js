 
//==========================================================TAG CONTROLLER============================================================================================================
//====================================================================================================================================================================================
   app.controller('areaController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		console.log('URL - GET -'+APP_URL+'/api/v1/areas?auth_app_type='+auth_app_type );
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/areas',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.areas = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		 var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: api_url+'&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.areas = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		
		
		  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,
                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					
		
		
		
		
		
		//========= FUNCTIONS FOR GET LOCATIONS FOR STORE A AREA====================================================================================== 
		  pro.locationSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/locations?search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedLocationChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
	            pro.parent_location_id =  values.location_id;  
            }
			
			 pro.searchLocationChange = function(text){ 
				   pro.parent_location_id = ''; 
			}
			
		//========= FUNCTIONS FOR GET LOCATIONS FOR STORE A AREA====================================================================================== 
		  pro.parentSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/areas?parents_only=true&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedParentChange = function(data1) {
                $log.info('Item changed to ' + JSON.stringify(data1)); 
	            pro.parent_area_id =  data1.area_id; 
            }
			
			 pro.searchParentChange = function(text){ 
				   pro.parent_area_id = ''; 
			}
			
			
		//========= FUNCTIONS FOR STORE A Arera=============================================================================================== 
		 pro.storeArea = function(){ 
		 	var auth_app_type = document.getElementById('auth_app_type').value;
           pro.location_id = pro.parent_location_id;
		   pro.parent_id = pro.parent_area_id;
		   pro.title = $('#title').val();
		   pro.pincode = $('#pincode').val();
		   pro.latitude = $('#us3-lat').val();
		   pro.longitude = $('#us3-lon').val();
		   
		   pro.area_data = {title : pro.title, latitude : pro.latitude, longitude : pro.longitude, pincode : pro.pincode, parent_id : pro.parent_id , location_id : pro.location_id};
		   console.log('URL - POST -'+APP_URL+'/api/v1/areas');
		    console.log('POST DATA -'+JSON.stringify(pro.area_data));
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/areas?auth_app_type='+auth_app_type,
            data:  pro.area_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

             location.reload();			 
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A TAG FORM=============================================================================================== 
		 pro.editArea = function(values){  
           pro.area_id = values.area_id;
		   pro.area_title = values.title;
          pro.latitude = values.latitude;
		   pro.longitude = values.longitude; 
            pro.pincode = values.pincode;
		   pro.parent_id = values.parent_id; 
		   pro.location_id = values.location_id;
		   pro.parent_location_title = values.parent_location_title;
		   pro.parent_area_title = values.parent_area_title;		   
		    
		    $('#edit').modal('show');   
		 }
		 
		 
		 
		  pro.locationpickerOptions = {
                        location: {
                            latitude: $('#us3-lat_edit').val() ,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat_edit'),
                            longitudeInput: $('#us3-lon_edit'),
                            radiusInput: $('#us3-radius_edit'),
                            locationNameInput: $('#us3-address_edit') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR GET LOCATIONS FOR STORE A AREA====================================================================================== 
		  pro.editLocationSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/locations?parents_only=true&search_text="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedEditLocationChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
	            pro.parent_edit_location_id =  values.location_id;  
				 if(pro.parent_edit_location_id != undefined || pro.parent_edit_location_id != null){
				   pro.location_id = pro.parent_edit_location_id;
			    }	
            };
			
			 pro.searchEditLocationChange = function(text){  
				   pro.location_id = ''; 
			};
		 
		 //========= FUNCTIONS FOR EDIT  A AREA FORM================================================
		  pro.editAreaSearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/areas?parents_only=true&exclude_id="+pro.area_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {  
                return response.data.data.data;
            })
        };
  
        pro.selectedEditAreaChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
			 pro.parent_edit_area_id =   data.area_id; 
               if(pro.parent_edit_area_id != undefined || pro.parent_edit_area_id != null){
				   pro.parent_id = pro.parent_edit_area_id;
			    }			  
        } 
		 
		 
		  pro.searchEditAreaChange = function(text){ 
				   pro.parent_id = ''; 
			}
		 //========= FUNCTIONS FOR UPDATE A TAG=============================================================================================== 
		 pro.updateArea = function(id){ 
		   var auth_app_type = document.getElementById('auth_app_type').value;
           pro.area_id = id; 
		   pro.title = $('#title_edit').val();
           pro.latitude = $('#us3-lat_edit').val();
		   pro.longitude = $('#us3-lon_edit').val();
            pro.pincode = $('#pincode_edit').val();
		   pro.parent_id = $('#parent_edit_id').val();
		   pro.location_id = $('#location_edit_id').val();	
		    
		   pro.area_data = {title : pro.title, latitude : pro.latitude, longitude : pro.longitude, pincode : pro.pincode, parent_id : pro.parent_id , location_id : pro.location_id};
		  

		  console.log('URL - PUT -'+APP_URL+'/api/v1/areas/'+pro.area_id);
		  console.log('POST DATA -'+JSON.stringify(pro.area_data));  
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/areas/'+pro.area_id+'?auth_app_type='+auth_app_type,
            data:  pro.area_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
           

             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


            location.reload();			 
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		  // FUNCTION FOR DELETE  AREA  ============================================================== clean done
	pro.deleteArea = function(areaId) {
		    
			pro.area_id = areaId;

			console.log('URL - DELETE -'+APP_URL+'/api/v1/areas/'+pro.area_id);
		 
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/areas/'+pro.area_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			         console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
   });