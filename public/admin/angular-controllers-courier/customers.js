 
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userController', function ($http,$scope,$window,toastr) {
		var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 


   	console.log("URL - GET - "+APP_URL+'/api/v1/users?user_type=2&auth_app_type='+auth_app_type );
	            
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=2&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteUser = function(userId) { 
			$scope.user_id = userId; 
var auth_app_type = document.getElementById('auth_app_type').value;
			 	console.log("URL - DELETE - "+APP_URL+'/api/v1/users/'+$scope.user_id );
	         


			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			var auth_app_type = document.getElementById('auth_app_type').value;
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&user_type=2&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		// FUNCTION FOR STATUS CHANGE  ============================================================== clean done
				  pro.switch = function(value,id){
        
        pro.user_id = id;
        pro.user_status = value;
         pro.user_status_value = {"status":pro.user_status};
         console.log(JSON.stringify(pro.user_status_value));
          console.log("URL--"+pro.user_id);
         var request = $http({
                method: "PUT",
                url: APP_URL+'/api/v1/customers-courier/user-active-status/update/'+pro.user_id,
                data:  pro.user_status_value,
                headers: { 'Accept':'application/json' }
           });

           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {
              pro.users = data;
                 if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
           else { toastr.error(data.message, 'Error'); return false; }
            // $('#loading').css('display', 'none');
                document.getElementById("res").value = JSON.stringify(data);
           })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, 'Error');                
                document.getElementById("res").value = JSON.stringify(data);
           });
    
        
    }
		
 	});
	
	
	
	
	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr) {
 
 var auth_app_type = document.getElementById('auth_app_type').value;
 
	     // get basic form to add product ============================================================== clean done
	     
      $('#loading').css('display', 'block');

      console.log("URL - GET - "+APP_URL+'/v1/form/user?user_type=2&auth_app_type='+auth_app_type );
	            
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=2&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  




		
		  
		
		
		
		   // FUNCTION FOR STORE USER  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 
		      for(var i=0;i<$scope.addUser[0].fields.length;i++){ 
			     if($scope.addUser[0].fields[i].identifier == 'user_type'){
				    $scope.addUser[0].fields[i].value = '2';
			     }
              }


				 var photo = $('#photo').val();
				 
				 for(var i=0;i<$scope.addUser[0].fields.length;i++){
				 if($scope.addUser[0].fields[i].identifier == 'photo'){
					 $scope.addUser[0].fields[i].value = photo; 
				 } 
				}
 
 

  console.log("URL - POST - "+APP_URL+'/api/v1/form/user?auth_app_type='+auth_app_type );
	            console.log("POST DATA -"+JSON.stringify($scope.addUser));

			var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_app_type='+auth_app_type,
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data;
      



            $('#loading').css('display', 'none');
			   		         
			   		         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			 $scope.customer_id = data.data.user_id

		
			  $window.location.href = 'customer/'+$scope.customer_id;
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
		   };			   
		  
	});



 
	

	
//=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userEditController', function ($http, $scope, $window,toastr) {
 
 var auth_app_type = document.getElementById('auth_app_type').value;
 
  $scope.customer_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
	 
	        $('#loading').css('display', 'block');


	          console.log("URL - GET - "+APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=2&auth_app_type="+auth_app_type );
	       
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=2&auth_app_type="+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
        	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.editUser  = data;
			 $('#loading').css('display', 'none');  
			  for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
	 }
	  
        })
		.error(function (data, status, headers, config) {
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  


		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){ 
		   	 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
  
 

		   console.log("URL - PUT - "+APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type);
		   console.log("POST DATA - "+JSON.stringify($scope.editUser));
			    
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

 

 
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
              console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

 
            $('#loading').css('display', 'none');    
			  
        })
		.error(function (data, status, headers, config) {  	
		 console.log("ERROR RESPONSE="+JSON.stringify(data)); 
             toastr.error(data.message, 'Error');
		       
        });  
 
		   };



 //==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
				return false;
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.customer_id,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

			 console.log("URL - PUT - "+APP_URL+'/api/v1/users/update-password/'+$scope.customer_id);
		   console.log("POST DATA - "+JSON.stringify($scope.passwordValue));

		   

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		       console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
		   
			}
           };

		   
	});



