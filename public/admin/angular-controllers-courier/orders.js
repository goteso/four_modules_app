 
//========================================================== ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('ordersController', function($http,$scope,$window,$log, toastr) {
		  
	 var pro = this; 
	    $('#loading').css('display', 'block');
	    var auth_app_type = document.getElementById('auth_app_type').value;
	   

	   			console.log("URL - GET - "+APP_URL+'/api/v1/orders?include_count_blocks=true&status=any&auth_app_type='+auth_app_type );
		     
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 

		
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&status=any&auth_app_type='+auth_app_type,  

            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	console.log("ERROR RESPONSE="+JSON.stringify(data));        
	    }); 
			
 
 
 
        pro.getOrdersByStatus = function(value){
			  var auth_app_type = document.getElementById('auth_app_type').value;
			pro.order_status = value;
						console.log("URL - GET - "+APP_URL+'/api/v1/orders?include_count_blocks=true&auth_app_type='+auth_app_type+'&status='+pro.order_status );
		     
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_app_type='+auth_app_type+'&status='+pro.order_status,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	console.log("ERROR RESPONSE="+JSON.stringify(data));       
	    }); 
			
		}
  
  
  // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
					console.log("URL - GET - "+api_url+'&include_count_blocks=true&auth_app_type='+auth_app_type+'&status='+pro.order_status );
		   var auth_app_type = document.getElementById('auth_app_type').value;   
		var request = $http({
            method: "GET",
            url: api_url+'&include_count_blocks=true&auth_app_type='+auth_app_type+'&status='+pro.order_status,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.orders = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
  
		
	});



 


