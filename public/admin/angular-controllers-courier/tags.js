
 
//==========================================================TAG CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('tagsController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/tag?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.tags = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: api_url+'&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.tags = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		
		// FUNCTION FOR DELETE TAG  ============================================================== clean done
	pro.deleteTagId = function(tagId) {
		    
			$scope.tag_id = tagId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/tag/'+$scope.tag_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
			//====================FUNCTION FOR GET PARENT TAG API==========================================================================
		pro.tagSearch = function(query) {  	
            return $http.get(APP_URL + "/api/v1/tag?parents_only=true&search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 				
                return response.data.data.data;
            })
        };
  
        pro.selectedTagChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.tag_id =   data.tag_id;  
        } 
		
		
		
		
		 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		  	var auth_app_type = document.getElementById('auth_app_type').value;

		        return $http.get(APP_URL+"/api/v1/stores?auth_app_type="+auth_app_type+"&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.store_id =  values.store_id;  		
            }
			
		//========= FUNCTIONS FOR STORE A TAG=============================================================================================== 
		 pro.storeTag = function(){ 
		 	var auth_app_type = document.getElementById('auth_app_type').value;
           pro.tag_photo = $('#file_name').val(); 
		   pro.tag_title = $('#tag_title').val();
		   pro.parent_id = $('#tag_id').val(); 
            pro.store_id = $('#store_id').val();			
		   pro.tag_data = {"tag_title" : pro.tag_title, "parent_id" : pro.parent_id, "store_id" : $scope.store_id, "tag_photo" : pro.tag_photo };
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/tag?auth_app_type='+auth_app_type,
            data:  pro.tag_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
        

            if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


             location.reload();			 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A TAG FORM=============================================================================================== 
		 pro.editTag = function(values){  
		  pro.tag_id = values.tag_id;
		   pro.tag_title = values.tag_title;
		   pro.parent_tag_title = values.parent_tag_title;	 
		    pro.parent_id = values.parent_id;   
		   $('#edit').modal('show');   
		 }
		 
		 
		 	
		//========= FUNCTIONS FOR EDIT  A TAG FORM================================================
		  pro.editTagSearch = function(query) {  

		  	var auth_app_type = document.getElementById('auth_app_type').value;
            return $http.get(APP_URL + "/api/v1/tag?auth_app_type="+auth_app_type+"&parents_only=true&exclude_id="+pro.tag_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {  
                return response.data.data.data;
            })
        };
  
        pro.selectedEditTagChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
			 pro.parent_tag_id =   data.tag_id; 
               if(pro.parent_tag_id != undefined || pro.parent_tag_id != null){
				   pro.parent_id = pro.parent_tag_id;
			    }			  
        } 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A TAG=============================================================================================== 
		 pro.updateTag = function(id){ 
		 
		 pro.tag_id1 = id; 
		   pro.tag_title = $('#tag_edit_title').val();
		   pro.parent_id = $('#tag_edit_id').val();
		   pro.tag_data = {"tag_title" : pro.tag_title, "parent_id" : pro.parent_id};
		   
		     var auth_app_type = document.getElementById('auth_app_type').value;
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/tag/'+pro.tag_id1+'?auth_app_type='+auth_app_type,
            data:  pro.tag_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
           

               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
            location.reload();			 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		
   });