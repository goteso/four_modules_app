  
 app.directive('demoFileModel', function ($parse) {
        return {
            //restrict: 'A', //the directive can be used as an attribute only
 
            /*
             link is a function that defines functionality of directive
             scope: scope associated with the element
             element: element on which this directive used
             attrs: key value pair of element attributes
             */
            link: function (scope, element, attrs) {
                var model = $parse(attrs.demoFileModel),
                    modelSetter = model.assign; //define a setter for demoFileModel
 
                //Bind change event on the element
                element.bind('change', function () {
                    //Call apply on scope, it checks for value changes and reflect them on UI
                    scope.$apply(function () {
                        //set the model value
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    });
	
	app.service('fileUploadService', function ($http, $q) {
 
        this.uploadFileToUrl = function (file, uploadUrl) {
            //FormData, object of key/value pair for form fields and values
            var fileFormData = new FormData();
            fileFormData.append('file', file);
 
            var deffered = $q.defer();
            $http.post(uploadUrl, fileFormData, {
                transformRequest: angular.identity,
                headers: {'Content-Type':  ''}
 
            }).success(function (response) {
                deffered.resolve(response);
 
            }).error(function (response) {
                deffered.reject(response);
            });
 
            return deffered.promise;
        }
    });
	
	
	
//==========================================================ADD CATEGORY CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('addCategoryController', function($http, $scope , $window, $log, $q, $timeout, toastr, fileUploadService) {
 
        
     
	//========= FUNCTIONS FOR GET PARENT CATEGORY SEARCH LIST=============================================================================================== 
       /* $scope.querySearch1 = function(query) {
            return $http.get(APP_URL + "/users_search?search_text=" + query + "&user_type=customer", {
                params: {
                    q: query
                }
            })
            .then(function(response) {
                return response.data;
            })
        };

		
        $scope.selectedItemChange = function(item, field) {
            $log.info('Item changed to ' + JSON.stringify(item));
            if (field.identifier == 'customer_id') {
                 $scope.customer_id_value = field.value = item.id;
            }
            $scope.selected_values = '';
        }*/


	       
	
	 
		
    }); 
	
	
	
	
	
	
 
	
	
	 
	
//==========================================================CATEGORY CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('OrderCancelReasonsController', function($http, $scope , $window, $log, $q, $timeout, toastr, fileUploadService) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		

					console.log("URL - GET - "+APP_URL+'/api/v1/order-cancel-reasons?per_page=20&auth_app_type='+auth_app_type );
		     
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/order-cancel-reasons?per_page=20&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.order_cancel_reasons = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		


					console.log("URL - POST - "+api_url+'&per_page=20&auth_app_type='+auth_app_type );
		    
		var request = $http({
            method: "GET",
            url: api_url+'&per_page=20&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           pro.order_cancel_reasons = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		
		
		// FUNCTION FOR DELETE CATEGORY  ============================================================== clean done
	pro.deleteOrderCancelReasons = function(id) {
		    
			$scope.id = id;
			
			if (confirm("Are you sure?")) {

							console.log("URL - DELETE - "+APP_URL+'/api/v1/order-cancel-reasons/'+$scope.id );
  var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/order-cancel-reasons/'+$scope.id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                    console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) {
                console.log("ERROR RESPONSE="+JSON.stringify(data));  
	 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		
 
		
		
 
 
		
		//========= FUNCTIONS FOR STORE A CATEGORY=============================================================================================== 
		 pro.storeOrderCancelReasons = function(){ 
         
           pro.title = $('#title').val();			
		   pro.post_data = {  "title" : pro.title  };

		  var auth_app_type = document.getElementById('auth_app_type').value;
			console.log("URL - POST - "+APP_URL+'/api/v1/order-cancel-reasons' );
		    console.log("POST DATA - "+JSON.stringify( pro.post_data ));
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/order-cancel-reasons?auth_app_type='+auth_app_type,
            data:  pro.post_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {
           console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            pro.data = data;
             if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A CATEGORY FORM=============================================================================================== 
		 pro.editOrderCancelReasons = function(values){  
		 console.log(JSON.stringify(values));
		 pro.title = values.title;
           pro.id = values.id;
 
	 $('#edit_title').val(pro.title);
		     $('#editCat').modal('show');   
 }
		 
              
			
	 
		 
		 //========= FUNCTIONS FOR UPDATE A CATEGORY=============================================================================================== 
		 pro.updateOrderCancelReasons = function(id){ 
           pro.id = id; 
		   pro.title = $('#edit_title').val();

 
 
		   pro.post_data = {"title" : pro.title};

		   			console.log("URL - PUT - "+APP_URL+'/api/v1/order-cancel-reasons/'+pro.id );
		    console.log("POST DATA - "+JSON.stringify( pro.post_data ));

		     var auth_app_type = document.getElementById('auth_app_type').value;
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/order-cancel-reasons/'+pro.id+'?auth_app_type='+auth_app_type,
            data:  pro.post_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {
           console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            pro.data = data;
            if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		
   });