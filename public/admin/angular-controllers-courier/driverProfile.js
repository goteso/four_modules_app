 app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/view-grocery/driver-basicInfo.html",
        controller : "profileController"
    })
    .when("/tasks", {
        templateUrl : APP_URL+"/view-grocery/driver-task.html",
        controller : "tasksController"
    });
});
 
 
app.controller('profileController', function($http,$scope,toastr) {
 

 
   $scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.driver_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends

 
     
	   $('#loading').css('display', 'none');  

	    console.log("URL - GET - "+APP_URL+'/api/v1/users/'+$scope.driver_id+'?include_meta=true' );
		  
var auth_app_type = document.getElementById('auth_app_type').value;
	   
		 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/users/'+$scope.driver_id+'?include_meta=true&auth_app_type='+auth_app_type,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.profileData = data;
			  $('#loading').css('display', 'none');
			  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
         }).error(function (data, status, headers, config) { 
			  
			 console.log("ERROR RESPONSE="+JSON.stringify(data)); 
               
        });   
});




 


app.controller('tasksController', function($http,$scope,toastr) {
   $scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.driver_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends

 
     
 var auth_app_type = document.getElementById('auth_app_type').value;
	 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/task?driver_id='+$scope.driver_id+'&auth_app_type='+auth_app_type,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.taskData = data;
			  $('#loading').css('display', 'none');
			  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
         }).error(function (data, status, headers, config) { 
			  
			 console.log("ERROR RESPONSE="+JSON.stringify(data)); 
               
        });   
	
});





