var app = angular.module("mainApp", ['ngTagsInput', '720kb.datepicker','ngAnimate', 'toastr'])

    app.directive('clockPicker', function () {
	    return {
		    restrict: 'A',
		    link: function (scope, element, attrs) {
			    element.clockpicker();
		    }
	    }
    })


   //====================================================== ADD PRODUCT CONTROLLER=================================================================================
   //==============================================================================================================================================================
 
	
	app.controller('itemController', function ($http, $scope, $window,toastr) {

	    $('#loading').css('display', 'none');
		$scope.time = '';
	
        //1 get basic form to add product ============================================================== clean done
	     
         var form_data = JSON.parse($window.controller_data);
         $scope.addItem = form_data;
 
 
         $('#loading').css('display', 'none');
		 

		//2 store product basic info to database ====================================================== clean done
	   
	   $scope.add = function () { 
		    var photo = $('#photo').val();
            //alert(JSON.stringify($scope.addUser[0].fields[0].type));
            if($scope.addItem[0].fields[4].identifier == 'photo'){
	            $scope.addItem[0].fields[4].value = photo;
            } 
            //document.getElementById("res").value = JSON.stringify($scope.addItem);
 



 			console.log("URL - POST - "+APP_URL+'/product_add');
		    console.log("POST DATA - "+JSON.stringify($scope.addItem));

		    var auth_app_type = document.getElementById('auth_app_type').value;
            var request = $http({
			   method: "POST",
			   url: APP_URL+'/product_add?auth_app_type='+auth_app_type,
			   data: $scope.addItem,
			   headers: {
				  'Accept': 'application/json'
			   }
		    });
            request.success(function (data) {
            console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
                $scope.product_id = data["user_data"][0]["product_id"];
                $window.location.href = 'product_edit/' + $scope.product_id;
		    })
			.error(function (data, status, headers, config) {
			console.log("ERROR RESPONSE="+JSON.stringify(data));   
			     toastr.error('Error Occurs','Error!');
			     //document.getElementById("res").value = JSON.stringify(data);
			    return;
            });
	    };
    

	
	
	 
         //3  Get Categories List ======================================================
	    $scope.arrayText = [{
		   "date_filter": "firstAdded",
		   "search_text": "",
		   "parent_id": "",
		   "limit": "10"
	    }];
  
        /*get data from api**/




        			console.log("URL - POST - "+APP_URL+'/categories_list');
		    console.log("POST DATA - "+JSON.stringify( $scope.arrayText ));

		    var auth_app_type = document.getElementById('auth_app_type').value;
	    var request = $http({
		     method: "POST",
		     url: APP_URL+'/categories_list?auth_app_type='+auth_app_type,
		     data: $scope.arrayText,
		     headers: {
			     'Accept': 'application/json'
		     }
	    });

	    /* Check whether the HTTP Request is successful or not. */
	    request.success(function (data) {
	    	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
		    $scope.allCategoryList = data.data.data; 
		    // $scope.product_id = data["user_data"][0]["product_id"];/
		   //$window.location.href = 'product_edit/'+$scope.product_id;
	    })
		.error(function (data, status, headers, config) {
			console.log("ERROR RESPONSE="+JSON.stringify(data)); 
             //document.getElementById("res").value = JSON.stringify(data);
	    });

       
	   
	   
	   //4. get tags data ==============================================================
        $scope.searchPeople = function (term) {
		    var search_term = term.toUpperCase();
		    $scope.people = [];
		    angular.forEach($scope.allCategoryList, function (item) {
			
			    if (item.title.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item);
		        });
           return $scope.people;
	    };



	
	    //4. get Brands List from api====================================================
    
        $scope.brands_post_data = [{"date_filter":"firstAdded","search_text":"","parent_id":"","limit":"10"}];
     
        /*get data from api**/ 


        	console.log("URL - GET - "+APP_URL+'/get_brands_list');
		    console.log("POST DATA - "+JSON.stringify( $scope.brands_post_data));   

var auth_app_type = document.getElementById('auth_app_type').value;
        var request = $http({
            method: "GET",
            url: APP_URL+'/get_brands_list?auth_app_type='+auth_app_type,
            data:  $scope.brands_post_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
              $scope.allBrandList = data.data.data;      
              // $scope.product_id = data["user_data"][0]["product_id"];/   
              //$window.location.href = 'product_edit/'+$scope.product_id;
        })
		.error(function (data, status, headers, config) {
			console.log("ERROR RESPONSE="+JSON.stringify(data)); 
              //document.getElementById("res").value = JSON.stringify(data);
         });
	   
	    
		
		//5. Search brands data
	   	$scope.searchBrand = function (term) {
		    var search_term = term.toUpperCase();
		    $scope.people = [];
		    angular.forEach($scope.allBrandList, function (item) {
			    if (item.title.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item);
		        });
            return $scope.people;
	    };

	   
	 
	 
	   //---------- selected tabs  
	   $scope.selected = 0; 
	   
    
	
	});






	
	
	
	//========================================================== EDIT PRODUCT CONTROLLER============================================================================
	//==============================================================================================================================================================
 

    app.controller('itemEditController', function ($http, $scope, $window,toastr) {

        $('#loading').css('display', 'block');
	
        $scope.editItem = $window._yourSpecialVar;

        $('#loading').css('display', 'none');
	 
	 
	   //product update =================================================================== clean done
       
	    $scope.saveChanges = function () { 
	 
	        var photo = $('#photo').val(); 
            if($scope.editItem[0].fields[4].identifier == 'photo'){
	            $scope.editItem[0].fields[4].value = photo;
            } 
       
 
            var product_id = $scope.editItem[0]["product_id"];



            			console.log("URL - POST - "+APP_URL+'/product_update/'+product_id );
		    console.log("POST DATA - "+JSON.stringify( $scope.editItem));

		    var auth_app_type = document.getElementById('auth_app_type').value;
            var request1 = $http({
				method: "POST",
				url: APP_URL+'/product_update/'+product_id+'?auth_app_type='+auth_app_type,
				data: $scope.editItem,
				headers: {
					'Accept': 'application/json'
				}
			});
            request1.success(function (data) {
            	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 

		          if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		    }).error(function (data, status, headers, config) {
		    	console.log("ERROR RESPONSE="+JSON.stringify(data)); 
               toastr.error('Error Occurs','Error!');
		    });
	};

	
	
      //  ADD UPDATE VARIANTS (for Add Product Basic Form Autocomplete ) =====================================================================
	$scope.add = function (index) {
		var value = {};
		$scope.editItem[2].tabs[index].values.push(value); 
	}
 
	$scope.save_variant = function (index, tab_id) {
 
		$scope.price_difference = document.getElementsByName('price_difference' + index + tab_id)[0].value;
        $scope.stock_count = document.getElementsByName('stock_count' + index + tab_id)[0].value;
        $scope.price = document.getElementsByName('price' + index + tab_id)[0].value;
        $scope.product_variant_type_id = document.getElementsByName('product_variant_type_id' + index + tab_id)[0].value;
        $scope.variant_id = document.getElementsByName('id' + index + tab_id)[0].value;
        $scope.product_id = document.getElementById('product_id').value;
		$scope.title = document.getElementsByName('title' + index + tab_id)[0].value;


		$scope.variant_data = {
			"product_id": $scope.product_id,
			"product_variant_type_id": $scope.product_variant_type_id,
			"title": $scope.title,
			"price_difference": $scope.price_difference,
			"stock_count": $scope.stock_count,
			"variant_id": $scope.variant_id
		};




			console.log("URL - POST - "+APP_URL+'/add_update_variant' );
		    console.log("POST DATA - "+JSON.stringify( $scope.variant_data ));

		    var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
			method: "POST",
			url: APP_URL+'/add_update_variant?auth_app_type='+auth_app_type,
			data: $scope.variant_data,
			headers: {
				'Accept': 'application/json'
			}
		});

	 
		request.success(function (data) {
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
			
			    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            	
	         $('input[name="' + 'id' + index + tab_id + '"]').val(data.id) ;
	 
        }).error(function (data, status, headers, config) { 
        	console.log("ERROR RESPONSE="+JSON.stringify(data)); 
		toastr.error('Error Occurs','Error!');
	   }); 

	}


// API_CODE = Get Delete Product Variant (for Add Product Basic Form Autocomplete ) ================================================================
	$scope.delete_variant = function (index, tab_id ,index1) {
		
        document.getElementById("res").value = JSON.stringify($scope.editItem[2].tabs[index].values);
		$scope.editItem[2].tabs[index1].values.splice(index, 1);

		var variant_id = document.getElementsByName('id' + index + tab_id)[0].value;
		 
		  if (confirm("Are you sure?")) {

		  				console.log("URL - GET - "+APP_URL+'/delete_variant/'+variant_id );
		     
		$http.get(APP_URL+'/delete_variant/'+variant_id)
			.success(function (data, status, headers, config) {
				console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				toastr.success(data.message,'Success!');
			})
			.error(function (data, status, headers, config) {
				console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				 toastr.error('Error Occurs','Error!');
				//document.getElementById("res").value = JSON.stringify(data);
			});
		  }
   };


 
		 


// API_CODE = Get Categories List (for Add Product Basic Form Autocomplete ) =========================================================================
	$scope.loadTags = function (query) {
		return $http.get('tags.json');
	};


	$scope.arrayText = [{
		"date_filter": "firstAdded",
		"search_text": "",
		"parent_id": "",
		"limit": "1000"
	}];
 


var auth_app_type = document.getElementById('auth_app_type').value;
	/*get data from api**/
	var request = $http({
		method: "POST",
		url: APP_URL+'/categories_list?auth_app_type='+auth_app_type,
		data: $scope.arrayText,
		headers: {
			'Accept': 'application/json'
		}
	});

 
	request.success(function (data) {
		 $scope.allCategoryList = data.data.data;
     }).error(function (data, status, headers, config) {
        //document.getElementById("res").value = JSON.stringify(data);
	});

 
	$scope.externalContacts = [];
	$scope.externalContacts1 = [];


	$scope.searchPeople = function (term) {
		var search_term = term.toUpperCase();
		$scope.people = [];
		angular.forEach($scope.allCategoryList, function (item) {
			if (item.title.toUpperCase().indexOf(search_term) >= 0)
				$scope.people.push(item);

		});

		return $scope.people;
	};

	
	
	
// API_CODE = Get Categories List (for Add Product Basic Form Autocomplete ) ============================================================================
    
$scope.brands_post_data = [{"date_filter":"firstAdded","search_text":"","parent_id":"","limit":"1000"}];
 
    

    			console.log("URL - POST - "+APP_URL+'/get_brands_list' );
		    console.log("POST DATA - "+JSON.stringify( $scope.brands_post_data));

		    var auth_app_type = document.getElementById('auth_app_type').value;
        var request = $http({
            method: "GET",
            url: APP_URL+'/get_brands_list?auth_app_type='+auth_app_type,
            data:  $scope.brands_post_data,
            headers: { 'Accept':'application/json' }
        });

      
        request.success(function (data) { 
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
              $scope.allBrandList = data.data.data;      
         })
		.error(function (data, status, headers, config) {
			console.log("ERROR RESPONSE="+JSON.stringify(data)); 
              //document.getElementById("res").value = JSON.stringify(data);
         });
	   
	    
	   	$scope.searchBrand = function (term) {
		    var search_term = term.toUpperCase();
		    $scope.people = [];
		    angular.forEach($scope.allBrandList, function (item) {
			    if (item.title.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item);
		        });
            return $scope.people;
	    };
 

      $scope.selected = 0;


});