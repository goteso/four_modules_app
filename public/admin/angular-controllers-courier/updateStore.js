   
 	
//======================================================EDIT STORE CONTROLLER=================================================================================
//============================================================================================================================================================
 
	
	app.controller('updateStoreController', function ($http,$scope,$window,toastr,$location,$log, $q) {

		 	var auth_app_type = document.getElementById('auth_app_type').value;
			
			var auth_store_id = document.getElementById('auth_store_id').value;
			
		 
         //$('#loading').css('display', 'block'); 
 
		 

		 			console.log("URL - GET - "+APP_URL+'/api/v1/stores/'+auth_store_id );
 


		$http.get(APP_URL+'/api/v1/stores/'+auth_store_id+'?auth_app_type='+auth_app_type)
            .success(function(data, status, headers, config) { 
            	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
		    $scope.store = data.data[0];  
		    $scope.latitude = $scope.store.latitude; 
             $scope.vendor = $scope.store.vendor_id;
			  $scope.manager = $scope.store.manager_id; 	 
	    }) 
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
			
		//========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 

					console.log("URL - GET - "+APP_URL+'/api/v1/stores/'+auth_store_id+'/form/meta' );
		 

var auth_app_type = document.getElementById('auth_app_type').value;
         var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/stores/'+auth_store_id+'/form/meta?auth_app_type='+auth_app_type,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.storeMeta = data;    
             // $('#loading').css('display', 'none');  
			    console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            })
		    .error(function (data, status, headers, config) { 
		        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
            });   


			
			
		//========= FUNCTIONS FOR LOCATION FOR STORE====================================================================================== 
		
		function updateControls(addressComponents) {
    var city = addressComponents.city;
    $('#us5-city').val(addressComponents.city);
    $('#us5-state').val(addressComponents.stateOrProvince);
    $('#us5-zip').val(addressComponents.postalCode);
    $('#us5-country').val(addressComponents.country);
	
	 
	var address = addressComponents.city + ' ' + addressComponents.stateOrProvince +' '+ addressComponents.postalCode+' '+addressComponents.country;
                $('#address2').val(address); 
}

			  $scope.locationpickerOptions = { 
                        location: {
                            latitude:30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-edit-lat'),
                            longitudeInput: $('#us3-edit-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-edit-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true,
					   onchanged: function (currentLocation, radius, isMarkerDropped) {
                              var addressComponents = $(this).locationpicker('map').location.addressComponents;
                              updateControls(addressComponents)
					   }
                    };
					
					
					
					
					 //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.vendor_id =  values.user_id ; 				
			    if($scope.vendor_id != undefined || $scope.vendor_id != null){
				   $scope.vendor = $scope.vendor_id;
			    }
            }
			
			 $scope.searchVendorChange = function(text){ 
				   $scope.vendor = ''; 
			}
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){

		  	var auth_app_type = document.getElementById('auth_app_type').value;

		        return $http.get(APP_URL+"/api/v1/users?auth_app_type="+auth_app_type+"&user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id; 
                 if($scope.manager_id != undefined || $scope.manager_id != null){
				   $scope.manager = $scope.manager_id;
			    }				
            }
			 $scope.searchManagerChange = function(text){ 
				   $scope.manager = ''; 
			}
			
		 //========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
		$scope.updateStoreData = function(){  
		           $scope.featured = $('#featured').val();	 
		   var auth_app_type = document.getElementById('auth_app_type').value;
			$scope.store_title = $('#title').val(); 
			$scope.comission = $('#comission').val(); 
			$scope.manager_id = $('#manager_id').val(); 
			$scope.vendor_id = $('#vendor_id').val();   
			$scope.latitude = $('#us3-edit-lat').val(); 
			$scope.longitude = $('#us3-edit-lon').val();   
			$scope.address = $('#address2').val();   			
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = [{ "store_title" : $scope.store_title, "commission" : $scope.comission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address,"featured" : $scope.featured, "store_photo" : $scope.store_photo}];
 
		 
 
		  
		   $scope.updateStoreDataValue = {"basic_data" : $scope.storeData , "meta_data" : $scope.storeMeta }
		  
	 
		 
	   			console.log("URL - POST - "+APP_URL+'/api/v1/stores/'+auth_store_id+'/submit_store_edit_forms?auth_app_type='+auth_app_type );
		    console.log("POST DATA - "+JSON.stringify($scope.updateStoreDataValue));

		    
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+auth_store_id+'/submit_store_edit_forms?auth_app_type='+auth_app_type,
                 data:  $scope.updateStoreDataValue,
                 headers: { 'Accept':'application/json' }
            }); 
            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
            	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

 
             // $('#loading').css('display', 'none');  
			     
            })
		    .error(function (data, status, headers, config) { 
		     
		        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
            });       
	
		}
		
		 
 
 	});
		
 
 
 
 
 
 
 
 
 
 
 
  
	