 
	
//==========================================================TIMESLOT CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('timeslotsController', function($http, $scope , $window, $log, $q, $timeout, toastr, $filter, mdcDateTimeDialog) {
  $scope.showval = false; 
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		 


		 			console.log("URL - GET - "+APP_URL+'/api/v1/timeslots_settings');
		 

var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/timeslots_settings?auth_app_type='+auth_app_type, 
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.timeslots = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		$scope.selected = 0;
		 
		$scope.add = function (index,day) { 
		    $scope.showval = true; 
	    }
	 
		 $scope.changeTab = function(){
			  $scope.showval = false; 
		 }
		 
		 //========= FUNCTIONS FOR UPDATE A TIMESLOT=============================================================================================== 
		 pro.storeTimeslot = function(index, slot_day){ 
             pro.weekday = slot_day; 
			 pro.from_time = $('#from_time').val();  
			 pro.to_time = $('#to_time').val();  
			 pro.slotData = {  "weekday":pro.weekday,  "from_time":pro.from_time,  "to_time":pro.to_time};
			 
		 
var auth_app_type = document.getElementById('auth_app_type').value;
			  			console.log("URL - POST - "+APP_URL+'/api/v1/timeslots' );
		    console.log("POST DATA - "+JSON.stringify( pro.slotData ));


			   var request = $http({
				method: "POST",
				url: APP_URL+'/api/v1/timeslots?auth_app_type='+auth_app_type,
				data:  pro.slotData,
				headers: { 'Accept':'application/json' }
				});

				/* Check whether the HTTP Request is successful or not. */
			   request.success(function (data) {
			   console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
				pro.data = data; 
				  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
				else { toastr.error(data.message, 'Error'); return false; }


				location.reload();			 
				 
			})
			.error(function (data, status, headers, config) { 
			console.log("ERROR RESPONSE="+JSON.stringify(data));  
				 toastr.error(data.message, 'Error');
				 
			});       
		 }
		 
 
			 //========= FUNCTIONS FOR UPDATE A TIMESLOT=============================================================================================== 
		 pro.updateTimeslot = function(index,slot_day){ 
 	 
		   pro.id = document.getElementsByName('id' + index + slot_day)[0].value; 
		  pro.weekday = document.getElementsByName('weekday' + index + slot_day)[0].value; 
         pro.from_time = document.getElementsByName('from_time' + index + slot_day)[0].value; 
		 pro.to_time = document.getElementsByName('to_time' + index + slot_day)[0].value; 
		
		
		 pro.slotData = {  "weekday":pro.weekday,  "from_time":pro.from_time,  "to_time":pro.to_time};
		  
var auth_app_type = document.getElementById('auth_app_type').value;
		  			console.log("URL - PUT - "+APP_URL+'/api/v1/timeslots/'+pro.id );
		    console.log("POST DATA - "+JSON.stringify( pro.slotData));

		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/timeslots/'+pro.id+'?auth_app_type='+auth_app_type,
            data:  pro.slotData,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {
           console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            pro.data = data;
     

         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
            location.reload();			 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
			console.log("ERROR RESPONSE="+JSON.stringify(data)); 
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 //================================ FUNCTION FOR DELETE TIMELOT  ================================================================================
	 
    	 pro.deleteTimeslot = function(id) { 
			$scope.slot_id = id; 
			if (confirm("Are you sure?")) {

							console.log("URL - DELETE - "+APP_URL+'/api/v1/timeslots/'+$scope.slot_id );
		 

		    var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/timeslots/'+$scope.slot_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
					console.log("ERROR RESPONSE="+JSON.stringify(data)); 
			       
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
   });