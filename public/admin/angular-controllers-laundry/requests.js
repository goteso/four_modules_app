  app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/view-laundry/approved-requests.html",
        controller : "approvedRequestController"
    })
    .when("/pending-requests", {
        templateUrl : APP_URL+"/view-laundry/pending-requests.html",
        controller : "pendingRequestController"
    });
});
  
  
  app.controller('approvedRequestController', function($http,$scope,toastr,$log,$location) {
	  
	  var auth_app_type = document.getElementById('auth_app_type').value;


	  			console.log("URL - POST - "+APP_URL+'/api/v1/stores/log-update-request/list?auth_app_type='+auth_app_type+'&approved_status=1' );
		     
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/log-update-request/list?auth_app_type='+auth_app_type+'&approved_status=1',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.approvedRequestData  = data;
            $('#loading').css('display', 'none');  
			 
        })
		.error(function (data, status, headers, config) {
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  

		
		
		$scope.ShowData = function(data){  
	 $scope.showRequestDetail = data;
	 $('#showRequestModal').modal('show');
	 console.log(JSON.stringify(data));
	  
 }
 
 
  });
  
app.controller('pendingRequestController', function($http,$scope,toastr,$log,$location) {
	
 	var auth_app_type = document.getElementById('auth_app_type').value;



 				console.log("URL - GET - "+APP_URL+'/api/v1/stores/log-update-request/list?auth_app_type='+auth_app_type+'&approved_status=0' );
 
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/log-update-request/list?auth_app_type='+auth_app_type+'&approved_status=0',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.pendingRequestData  = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  


		
		 
 $scope.ShowData = function(data){  
	 $scope.showRequestDetail = data;
	 $('#showRequestModal').modal('show');
	 console.log(JSON.stringify(data));
	  
 }
 
 
 
  $scope.approveRequest = function(value){
	  
	 $scope.request_id = value.id;
	 $scope.store_title = value.request_data_object.store_title;
	 $scope.store_photo = value.request_data_object.store_photo;
	 $scope.latitude = value.request_data_object.latitude;
	 $scope.longitude = value.request_data_object.longitude;
	  $scope.requestDataDetail = 
	  {
    "store_title":$scope.store_title,
    "store_photo":$scope.store_photo,
    "latitude": $scope.latitude,
    "longitude": $scope.longitude 
       };



var auth_app_type = document.getElementById('auth_app_type').value;
			console.log("URL - PUT - "+APP_URL+'/api/v1/stores/approve-log-update-request/'+$scope.request_id+'?auth_app_type='+auth_app_type );
		    console.log("POST DATA - "+JSON.stringify($scope.requestDataDetail));

	  var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/stores/approve-log-update-request/'+$scope.request_id+'?auth_app_type='+auth_app_type,
            data: $scope.requestDataDetail ,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            $scope.data  = data;
           if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					location.reload();
					}
					else{toastr.error(data.message,'Error!');}
        })
		.error(function (data, status, headers, config) {
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
		
 }
 
 
 
 $scope.rejectRequest = function(value){
	  
	 $scope.request_id = value.id;
	 $scope.store_title = value.store_details[0].store_title;
	 $scope.store_photo = value.store_details[0].store_photo;
	 $scope.latitude = value.store_details[0].latitude;
	 $scope.longitude = value.store_details[0].longitude;
	  $scope.requestDataDetail1 = 
	  {
    "store_title":$scope.store_title,
    "store_photo":$scope.store_photo,
    "latitude": $scope.latitude,
    "longitude": $scope.longitude 
       };



var auth_app_type = document.getElementById('auth_app_type').value;
			console.log("URL - PUT - "+APP_URL+'/api/v1/stores/reject-log-update-request/'+$scope.request_id+'?auth_app_type='+auth_app_type );
		    console.log("POST DATA - "+JSON.stringify( $scope.requestDataDetail1 ));
		    
	  var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/stores/reject-log-update-request/'+$scope.request_id+'?auth_app_type='+auth_app_type,
            data: $scope.requestDataDetail1 ,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data;
           if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					location.reload();
					}
					else{toastr.error(data.message,'Error!');}
        })
		.error(function (data, status, headers, config) {
            console.log("ERROR RESPONSE="+JSON.stringify(data)); 
		   
        });  
		
 }
 
});


 