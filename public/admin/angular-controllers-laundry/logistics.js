app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/view-laundry/tasks.html",
        controller : "taskController"
    })
    .when("/driver", {
        templateUrl : APP_URL+"/view-laundry/driver.html",
        controller : "driverController"
    }) ;
});
	
	
//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('logisticsController',   function ($http,$scope,$window,toastr,$interval,$log , $pusher ) { 
 
 
         








    var labels = ['P', 'D'];
    var labelIndex = 0;
 var markers = [];
 

    // FUNCTION FOR GET DRIVER LIST & LOCATION  ================================================================ clean done
    console.log("URL - GET - "+APP_URL+'/api/v1/users?user_type=5' );
		  var auth_app_type = document.getElementById('auth_app_type').value;
        var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=5&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.driver = data;
            $scope.driverLocations = data.data.data;
            console.log(JSON.stringify($scope.driverLocations)); 
		
            var mapOptions = {
				 zoom: 16,
             center: new google.maps.LatLng($scope.driverLocations[0].latitude, $scope.driverLocations[0].longitude),
       
         styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}] ,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
		 mapTypeControl: false,
		 streetViewControl: false
    }


  //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds(); 
     $scope.map = new google.maps.Map(document.getElementById('dvMap'), mapOptions); 
            

    for (i = 0; i < $scope.driverLocations.length; i++) {
          var latitude = $scope.driverLocations[i].latitude;
          var longitude = $scope.driverLocations[i].longitude; 
           var id = $scope.driverLocations[i].id; 
                     
                var data = $scope.driverLocations[i];

        var position = new google.maps.LatLng(data.latitude, data.longitude);
        bounds.extend(position);
        var marker = new google.maps.Marker({
            position: position,
            id: data.user_id,
            map: $scope.map,
            icon: APP_URL+"/admin/assets/images/a.png", 
            title: data.first_name,
            draggable: true,
            animation: google.maps.Animation.DROP,
            //label:'a'+i
        });
		
		markers.push(marker);
		
        (function (marker, data) {
                      
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent("<div class='container-fluid'><div class='row'  ><div class='col-sm-3'><img style='border-radius:50px;height:50px;width:50px;margin-top:20px;' src='"+APP_URL+"/images/users/"+data.photo+"'></div><div class='col-sm-9'><h5 style='color:#6399bd'>" + data.first_name +" "+ data.last_name+ "</h5><p> "+data.email+"</p><p>"+data.phone+" </p></div></div></div>");
                        $scope.InfoWindow.open($scope.map, marker);

                        //var center = new google.maps.LatLng(data., -98.3);
                        //$scope.map.setCenter(marker.position);
                        $scope.map.panTo(marker.position);
                    });
                })(marker, data);
 
				google.maps.event.addListener($scope.map, 'click', function(event) {
        var result = [event.latLng.lat(), event.latLng.lng()];
        //$scope.transition(result, 406);
    });	
	
	
                //Plotting the Marker on the Map.
               bounds.extend(marker.position); 
                     
        set_new_marker.push(marker); // pushing markers in the array 
       lineCoordinates.push($scope.driverLocations[i]);

 

    }
 
	
	var markerCluster = new MarkerClusterer($scope.map, markers, {imagePath: APP_URL+'/admin/assets/images/images/m'});
//Load google map
google.maps.event.addDomListener(window, 'load');
$scope.map.fitBounds(bounds); 
          $scope.map.setCenter(bounds.getCenter()); 


 $scope.transition = function(result, markerId){

	var newPosition = new google.maps.LatLng(result[0],result[1]);

 
for(var j=0;j<=markers.length;j++){
 if(markerId == markers[j].id){

  //alert(result);
  markers[j].animateTo(newPosition, {  easing: "easeOutQuad",
                                 duration: 3000,
                                 complete: function() {
                                   //alert("animation complete");
                   // $scope.getDirections();
                                 }
                              }); 


 }

}

 
							  
							  
}


            
           // $('#loading').css('display', 'none');  
            document.getElementById("res").value = JSON.stringify(data);
        })
        .error(function (data, status, headers, config) { 
             document.getElementById("res").value = JSON.stringify(data);
        });  



 
var lineCoordinates = [];
    var lineCoordinates1 = [];
 
 

 ///// Direction Code Started
  // instantiate google map objects for directions
  var directionsDisplay = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#4B6C81", strokeWeight:5 }, suppressMarkers:true });
  var directionsService = new google.maps.DirectionsService();
  var geocoder = new google.maps.Geocoder();
  
  // directions object -- with defaults
  $scope.directions = {
    origin: "Chandigarh",
    destination: "Mohali",
    showList: false
  }
  
  // get directions using google maps api
  $scope.getDirections = function (slat,slong, dlat, dlong) {
    var request = {
      origin: slat+","+slong,
      destination: dlat+","+dlong,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap($scope.map);
       // directionsDisplay.setPanel(document.getElementById('directionsList'));
        $scope.directions.showList = true;
      } else {
        alert('Google route unsuccesfull!');
      }
    });
  }

     
    // setting initial source

    var set_new_marker = [];

 




$scope.getDriverLocation = function(data){

var driverId = data.driver_details[0].user_id; 

for(var j=0;j<=markers.length;j++){
 if(driverId == markers[j].id){

  
   $scope.map.panTo(markers[j].position);

 }

}


}
    


 
	
	$('.plus-icon').css('display', 'none');
	$('.container-2').css('display', 'block');
	
	// FUNCTION FOR GET DRIVER LIST & LOCATION  ================================================================ clean done
	console.log("URL - GET - "+APP_URL+'/api/v1/users?user_type=5' );
	  var auth_app_type = document.getElementById('auth_app_type').value;
	 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=5&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.driver = data;
			$scope.driverLocations = data.data.data;
		 
			$scope.getMap();
			
           // $('#loading').css('display', 'none');  
	 
        })
		.error(function (data, status, headers, config) { 
		     console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        });       
	
	
	
	// FUNCTION FOR DELETE DRIVER  ================================================================================== clean done
	  $scope.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
				console.log("URL - DELETE - "+APP_URL+'/api/v1/users/'+$scope.user_id );
	  var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
          console.log("ERROR RESPONSE="+JSON.stringify(data)); 
			        
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
	
	
	
	$scope.drivers = function(){
		$('.plus-icon').css('display', 'block');
		$('.container-2').css('display', 'none');
	}
	$scope.tasks = function(){
		$('.plus-icon').css('display', 'none');
		$('.container-2').css('display', 'block');
	}

 
 
  /**
   $scope.getMap = function(){
            //Setting the Map options.
            $scope.MapOptions = {
                center: new google.maps.LatLng($scope.driverLocations[0].latitude, $scope.driverLocations[0].longitude),
                zoom: 8, 
				 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}] ,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
 
 

            //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            $scope.Latlngbounds = new google.maps.LatLngBounds();
            $scope.Map = new google.maps.Map(document.getElementById("dvMap"), $scope.MapOptions);
            
            //Looping through the Array and adding Markers.
            for (var i = 0; i < $scope.driverLocations.length; i++) {
				var latitude = $scope.driverLocations[i].latitude;
				var longitude = $scope.driverLocations[i].longitude; 
					 
                var data = $scope.driverLocations[i];
                var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
 
                //Initializing the Marker object.
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: $scope.Map,
                    title: data.first_name,
					 icon: APP_URL+"/admin/assets/images/google-pin.png",
                });
 
                //Adding InfoWindow to the Marker.
                (function (marker, data) {
					  
	   
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.first_name +" "+ data.last_name+ "<br></div>");
                        $scope.InfoWindow.open($scope.Map, marker);
                    });
                })(marker, data);
 
                //Plotting the Marker on the Map.
                $scope.Latlngbounds.extend(marker.position);
					 
            }
 
 
            
	   
 
            //Adjusting the Map for best display.
            $scope.Map.setCenter($scope.Latlngbounds.getCenter());
            $scope.Map.fitBounds($scope.Latlngbounds);
			
	}

    **/

    var m = [];
	    $scope.getMap = function(location_task) {


 
                for ( var i = 0; i < m.length; i++ )
                {

                    
                          if ( m[i].id == 'dest' ||  m[i].id == 'pickup')
                          {
                                  m[i].setMap(null);
                                  m.splice(i, 1);
                          }
                }


                                for ( var i = 0; i < m.length; i++ )
                {

                    
                          if ( m[i].id == 'dest' ||  m[i].id == 'pickup')
                          {
                                  m[i].setMap(null);
                                  m.splice(i, 1);
                          }
                }


var dropoff_latitude = location_task.dropoff_latitude;
var dropoff_longitude = location_task.dropoff_longitude;
var pickup_latitude = location_task.pickup_latitude;
var pickup_longitude = location_task.pickup_longitude;

            var pos = new google.maps.LatLng(pickup_latitude, pickup_longitude);
            bounds.extend(pos);
            $scope.marker1 = new google.maps.Marker({
                position: pos,
                map: $scope.map,
             icon: APP_URL+"/admin/assets/images/p.png", 
                id: 'pickup',
            });
 
        $scope.map.fitBounds(bounds);

    m.push($scope.marker1);

                    var pos = new google.maps.LatLng(dropoff_latitude, dropoff_longitude);
            bounds.extend(pos);
            $scope.marker2 = new google.maps.Marker({
                position: pos,
                map: $scope.map,
             icon: APP_URL+"/admin/assets/images/d.png", 
                id: 'dest',
            });

 
       $scope.map.setCenter(bounds.getCenter()); 
        $scope.map.fitBounds(bounds);
         m.push($scope.marker2);

 
 
         $scope.getDirections(pickup_latitude, pickup_longitude, dropoff_latitude, dropoff_longitude);




    }
	
	
	
	
	




 // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
 
    var pusher = new Pusher('2e2be9c9dcbc4a3be1d0', {
      cluster: 'ap2',
      forceTLS: true
    });

    var locationChannel = pusher.subscribe('location');
    locationChannel.bind('App\\Events\\SendLocation', function(data) {

    
var data = data.location;


      
     var driverId = data.id;

     var result = [data.lat, data.long];
     //console.log(markers[0]);
     $scope.transition(result, driverId);

 


    });
  

    var taskChannel = pusher.subscribe('task_update');
    taskChannel.bind('App\\Events\\TaskUpdate', function(data) {

 
    var data = data.details;
     var driverId = data.id;

     var result = [data.lat, data.long];

     toastr.success(data.description, data.title);
     //alert("task");

    });
  
 // Pusher ENDS



	
	console.log("URL - GET - "+APP_URL+'/api/v1/task?status=0' );
		

		 var auth_app_type = document.getElementById('auth_app_type').value; 
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=0&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.unassigned = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
	 
	
	
	$scope.assignedTask = function(){

		console.log("URL - GET - "+APP_URL+'/api/v1/task?status=1,2' );
	  var auth_app_type = document.getElementById('auth_app_type').value;
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=1,2&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.assigned = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
	}
	
	$scope.completedTask = function(){
		console.log("URL - GET - "+APP_URL+'/api/v1/task?status=3' );
		  var auth_app_type = document.getElementById('auth_app_type').value;
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=3&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.completed = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
	}


	
	
	
	
	
	 /*=============================FUNCTION FOR POPUP FOR ASSIGN TASK TO DRIVER=============================================================*/		
     	$scope.getDriver = function(data){ 	
   
		    $scope.task_id = data.task_id;
			$('#driverModal').modal('show');
			
		}
		
		 

		//============================= FUNCTIONS FOR GET DRIVERS LIST=================================================================
		  $scope.driverSearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/users?user_type=5&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {  
                return response.data.data.data;
            })
        };
  
        $scope.selectedDriverChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.driver_id =   data.user_id;   
        } 
		
		 

		//============================= FUNCTIONS FOR ASSIGN DRIVER TO A TASK================================================================
		$scope.assignDriver = function(){ 
	     	$scope.driver = {"driver_id": $scope.driver_id}; 

	     	console.log("URL - PUT - "+APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id);
		console.log("POST DATA - "+JSON.stringify( $scope.driver));
 var auth_app_type = document.getElementById('auth_app_type').value;
		     var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id+'?auth_app_type='+auth_app_type,
				 data: $scope.driver,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			    request.success(function (data) { 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				$scope.statusData =  data ;  
				$('#loading').css('display', 'none');
       
				if( data.status_text == 'Success'  )
				{
                   toastr.success(data.message, 'Success');
                   location.reload();		
				}
				else
				{
					toastr.error(data.message, 'Error');
				}
			 
				 
				 
			})
			.error(function (data, status, header, config) {            
				console.log("ERROR RESPONSE="+JSON.stringify(data));         
			}); 
		
		 }
        
		$scope.locationpickerOptions = { 
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
                        origin:'30.6554,76.3444',
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					
					
					
					
			/*		 var markers = [];
              for (var i=0; i<8 ; i++) {
      markers[i] = new google.maps.Marker({
        title: "Hi marker " + i
      })
    }
    $scope.GenerateMapMarkers = function() {
        $scope.date = Date(); // Just to show that we are updating
        
        var numMarkers = Math.floor(Math.random() * 4) + 4;  // betwween 4 & 8 of them
        for (i = 0; i < numMarkers; i++) {
            var lat =   1.280095 + (Math.random()/100);
            var lng = 103.850949 + (Math.random()/100);
            // You need to set markers according to google api instruction
            // you don't need to learn ngMap, but you need to learn google map api v3
            // https://developers.google.com/maps/documentation/javascript/markers
            var latlng = new google.maps.LatLng(lat, lng);
            markers[i].setPosition(latlng);
            markers[i].setMap($scope.map);
        }      
    };  
    
    $interval( $scope.GenerateMapMarkers, 2000);
	*/
	
 	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('taskController', function ($http,$scope,$window,toastr, $log) {

		console.log("URL - GET - "+APP_URL+'/api/v1/task?status=0');
		  var auth_app_type = document.getElementById('auth_app_type').value;
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=0&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.unassigned = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
	 
	
	
	$scope.assignedTask = function(){

		console.log("URL - GET - "+APP_URL+'/api/v1/task?status=1,2' );
		 

 var auth_app_type = document.getElementById('auth_app_type').value;
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=1,2&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.assigned = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
	}
	
	$scope.completedTask = function(){

		console.log("URL - GET - "+APP_URL+'/api/v1/task?status=3' );
		 

 var auth_app_type = document.getElementById('auth_app_type').value;
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=3&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.completed = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
	}
	
	
	
	
	 /*=============================FUNCTION FOR POPUP FOR ASSIGN TASK TO DRIVER=============================================================*/		
     	$scope.getDriver = function(data){ 	 
		    $scope.task_id = data.task_id;
			$('#driverModal').modal('show');
			
		}
		
		 

		//============================= FUNCTIONS FOR GET DRIVERS LIST=================================================================
		  $scope.driverSearch = function(query) {  
		  	 var auth_app_type = document.getElementById('auth_app_type').value;
            return $http.get(APP_URL + "/api/v1/users?auth_app_type="+auth_app_type+"&user_type=5&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {  
                return response.data.data.data;
            })
        };
  
        $scope.selectedDriverChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.driver_id =   data.user_id;  
				 
        } 
		
		 

		//============================= FUNCTIONS FOR ASSIGN DRIVER TO A TASK================================================================
		$scope.assignDriver = function(){ 
	     	$scope.driver = {"driver_id": $scope.driver_id}; 

	     	console.log("URL - PUT - "+APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id );
		console.log("POST DATA - "+JSON.stringify( $scope.driver));
 var auth_app_type = document.getElementById('auth_app_type').value;
		     var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id+'&auth_app_type='+auth_app_type,
				 data: $scope.driver,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			    request.success(function (data) { 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				$scope.statusData =  data ;  
				$('#loading').css('display', 'none');
       
				if( data.status_text == 'Success'  )
				{
                   toastr.success(data.message, 'Success');
                   location.reload();		
				}
				else
				{
					toastr.error(data.message, 'Error');
				}
			 
				 
				 
			})
			.error(function (data, status, header, config) {            
				console.log("ERROR RESPONSE="+JSON.stringify(data));        
			}); 
		
		 }
		 
	
	});
	
	
	
	
	
	// =====================================================DRIVER CONTROLLER STARTS HERE===================================================================
	//========================================================================================================================================================
	
	
	
	app.controller('driverController', function ($http,$scope,$window,toastr) { 
	
	console.log("URL - GET - "+APP_URL+'/api/v1/users?user_type=5' );
		 
	   	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=5&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.driver = data;
           // $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		  console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	  $scope.deleteUser = function(userId) { 


			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {

				console.log("URL - DELETE - "+APP_URL+'/api/v1/users/'+$scope.user_id );
		 


		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
	});
	
	
	
