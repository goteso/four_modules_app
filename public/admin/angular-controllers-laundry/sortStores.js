
//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('sortStoreController', function ($http,$scope,$window,toastr,$log) { 

        //$('#loading').css('display', 'block'); 
		


					console.log("URL - GET - "+APP_URL+'/api/v1/stores-sorted' );
	 
var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores-sorted?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.stores = data; 

            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
			  $scope.storesData = data.data;
			 $scope.storesDataList = {
		stores : $scope.storesData
	};
	
	$scope.storesData1 = { 
        d : {"d1": $scope.storesDataList.stores}
    };
	
	
 
	 
        })
		.error(function (data, status, headers, config) { 
			console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
	
	
	 
	
	

		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		


					console.log("URL - GET - "+api_url+'&?exclude_busy_store=false' );
		 

var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: api_url+'&?exclude_busy_store=false&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.stores = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
	
	
		//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
				$scope.storeData = values;
                 $log.info('Item changed to ' + JSON.stringify($scope.storeData)); 
                $scope.store_id =  values.store_id  ;  
            
            }
			
	      $scope.searchStoreChange = function(text,data){  
				   $scope.store = ''; 
			}
			
			
	
	
	
	
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
			$scope.addSelectedStore = function() {  
		
		      var alreadyInArray = '';
                var sortIndex = 0;

                if ($scope.storesDataList.stores.length == 0) { 
                    $scope.storesDataList.stores.push($scope.storeData); 
					$scope.sortArray();
                    return;
				}

                for (var i = 0; i < $scope.storesDataList.stores.length; i++) {
                    if ($scope.storesDataList.stores[i].store_id == $scope.storeData.store_id) {
                        toastr.error('Store already Selected','Error!');
						  alreadyInArray = 'YES';
                        sortArray = i;
                    }
                }

				
                if (alreadyInArray == 'YES') {
                    toastr.error('Store already Selected','Error!');
                    return;
                } 
				
				else { 
				 $scope.storesData1 = { 
						selected: null,
						d : {"d1": []}
					};	
                    $scope.storesDataList.stores.push($scope.storeData); 
					$scope.sortArray(); 
                    console.log(JSON.stringify($scope.storesDataList.stores)); 
				   
                } 
	}
	
	//========= FUNCTIONS FOR GET STORE=====================================================================================
	
 	
		
	
		
 	// FUNCTION FOR SORT OR ADD TABLE ARRAY  ============================================================== clean done
  $scope.sortArray = function(){
	for(var j = 0; j<= $scope.storesDataList.stores.length; ++j) {
        $scope.storesData1.d.d1.push($scope.storesDataList.stores[j]);  
		
    }
 
    // Model to JSON for demo purpose
    $scope.$watch('storesData1', function(model) {  
        $scope.modelAsJson = angular.toJson(model, true);
		console.log("iiii"+JSON.stringify(model.d.d1)); 
		 
    }, true);
  }

 
  // FUNCTION FOR DELETE STORE  ============================================================== clean done
	$scope.deleteStore = function(storeId,index) {
		    
			$scope.store_id = storeId; 
			if (confirm("Are you sure?")) {
		   $scope.storesData1.d.d1.splice(index, 1)
			
		    }
		
        };
		
 
 
 
   // FUNCTION FOR UPDATE SORT STORE LIST  ============================================================== clean done
 
 $scope.addStore = function(){
			   
	 

					console.log("URL - PUT - "+APP_URL+'/api/v1/stores-sorted');
		    console.log("POST DATA - "+JSON.stringify( $scope.storesData1 ));


		    var request = $http({
                 method: "PUT",
                 url: APP_URL+'/api/v1/stores-sorted?auth_app_type='+auth_app_type,
                 data:  $scope.storesData1,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success');  
			  }
            else { toastr.error(data.message, 'Error'); return false; }


				
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
		
	
	
	
	
 	});
	
	
	
	