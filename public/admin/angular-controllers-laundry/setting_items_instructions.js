
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('settingItemsInstructionsController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		

		console.log("URL - GET - "+APP_URL+'/api/v1/setting-items_instructions' );
		       


		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/setting-items-instructions?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.setting_items_instructions = data;
            $('#loading').css('display', 'none');  
			
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.faqs = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		
		
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		 pro.storeSettingItemsInstructions = function(){ 
           var auth_app_type = document.getElementById('auth_app_type').value;
		   pro.title = $('#title').val();
		  
		   pro.setting_items_instructions_data = {title : pro.title  };

		   		console.log("URL - POST - "+APP_URL+'/api/v1/setting-items-instructions' );
		        console.log("POST DATA - "+JSON.stringify(pro.faq_data));
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/setting-items-instructions?auth_app_type='+auth_app_type,
            data:  pro.setting_items_instructions_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  
           	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
			if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}
			 
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		  $scope.AppendText = function() {
     var myEl = angular.element( document.querySelector( '#divID' ) );
     myEl.append('Hi<br/>');     
    }
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.editSettingItemsInstructions = function(id,title){  
         
		    pro.title = title;
		    pro.instruction_id = id;
          		
          	 
		   $('#edit').modal('show');   
		 }
		 
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A FAQ=============================================================================================== 
		 pro.updateSettingItemsInstruction = function(id){ 
 
		 
		 	var auth_app_type = document.getElementById('auth_app_type').value;
           pro.instruction_id = id; 
		   pro.title = $('#edit_title').val();
		   
		   pro.instruction_data = {title : pro.title };

		   console.log("URL - PUT - "+APP_URL+'/api/v1/setting-items-instructions/'+pro.instruction_id );
		        console.log("POST DATA - "+JSON.stringify(pro.instruction_data));


		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/setting-items-instructions/'+pro.instruction_id+'?auth_app_type='+auth_app_type,
            data:  pro.instruction_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
           	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
             if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}	 
			 
        })
		.error(function (data, status, headers, config) { 
		console.log("ERROR RESPONSE="+JSON.stringify(data));  
             toastr.error(data.message, 'Error');
		     
        });       
		 }
		 
		 
		 
		 
		 // FUNCTION FOR DELETE STORE  ============================================================== clean done
	pro.deleteFaq = function(faqId) {
		    var auth_app_type = document.getElementById('auth_app_type').value;
			pro.instructions_id = faqId;

			  console.log("URL - DELETE - "+APP_URL+'/api/v1/setting-items-instructions/'+pro.instructions_id );
	 


			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/setting-items-instructions/'+pro.instructions_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
			        toastr.success(data.message,'Success!');

                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
					console.log("ERROR RESPONSE="+JSON.stringify(data)); 			         
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
   });