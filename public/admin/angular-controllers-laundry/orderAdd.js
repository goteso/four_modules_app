

app.factory('cartStorage', function() {
    var _cart = {
        items: [],
    };
    var service = {
        get cart() {
            return _cart;
        }
    }
    return service;
});




//==========================================================ADD ORDER CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('orderController', function($http,$scope,$window,$log, toastr, cartStorage) {
 
      
        var _this = this;
       
	    _this.cartStorage = cartStorage.cart;
        _this.total = 0;

        $('.order-content').css('display', 'none');
        $('#loading').css('display', 'block'); 
        
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API==================================================================
					console.log("URL - GET - "+APP_URL+'/api/v1/orders/add/form/');
  var auth_app_type = document.getElementById('auth_app_type').value;
		 var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/?auth_app_type='+auth_app_type,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	console.log("ERROR RESPONSE="+JSON.stringify(data));       
	    }); 
		
		
		 
		/* _this.orderCalculate = function() {
			alert('223');
			$scope.order_items = _this.cartStorage.items;
				    $scope.order_items.data = {"order_items": $scope.order_items};
				 
				    var request = $http({
                        method: "POST",
                        url: APP_URL + '/api/v1/orders/calculate',
                        data: $scope.order_items.data,
                        headers: {
                           'Accept': 'application/json'
                        }
                    });

                     Check whether the HTTP Request is successful or not. *
                    request.success(function(data) { 
                        _this.paymentData = data;  
					    $(".main-cart").show();	
					    $(".cart").hide();	
				        //document.getElementById("res8").value = JSON.stringify(_this.paymentData);
                    }).error(function(data, status, headers, config) { 
                        document.getElementById("res").value = JSON.stringify(data);
                    });
		}; */
		
	//========= FUNCTIONS FOR ADD PRODUCTS TO CART ====================================================================================
	//=================================================================================================================================
	
	
        _this.addToCart = function(item) {
            $scope.item = item;
            $scope.id = item.item_id;
            $scope.item_id = {"item_id": $scope.id }
			
			//CHECK VARIANTS EXIST OR NOT=====================================================================
			
			//IF DOES NOT EXIT-----------------------------------------------------------------
            if (item.variant_exist == '0') {
                var alreadyInCart = '';
                var cartIndex = 0;

                if (_this.cartStorage.items.length == 0) {
                    item.discounted_price = item.item_price;
                    item.quantity = 1;
				    item.variants = [];
                    _this.cartStorage.items.push(item);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.items);
				 
                    return;
				}

                for (var i = 0; i < _this.cartStorage.items.length; i++) {
                    if (_this.cartStorage.items[i].item_id == item.item_id) {
                        alreadyInCart = 'YES';
                        cartIndex = i;
                    }
                }

				
                if (alreadyInCart == 'YES') {
                    _this.cartStorage.items[cartIndex].quantity = _this.cartStorage.items[cartIndex].quantity + 1;
				
                    return;
                } 
				
				else {
                    item.quantity = 1;
				    item.variants = [];
                    item.discounted_price = item.item_price;
                    _this.cartStorage.items.push(item); 
                    document.getElementById("res").value = JSON.stringify(_this.cartStorage.items);
				
				   
                }
            }
 
			//IF VARIANTS EXIT------------------------------------------------------------------
			else { 
				//CALL API FOR GET PRODUCT VARIANTS===========================

							console.log("URL - GET - "+APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id );
		    

 var auth_app_type = document.getElementById('auth_app_type').value;
				var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id+'&auth_app_type='+auth_app_type,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.variantsdata = data.data;
            console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $('#loading').css('display', 'none');
			 $("#variantsModal").modal('show');
                     $scope.variantsArray = [];
        })
		.error(function (data, status, header, config) {            
        	console.log("ERROR RESPONSE="+JSON.stringify(data));       
	    }); 
			
				 
            }

        };

		
		  $scope.variantsSelectedTitle = [];
		
		
		//FUNCTION FOR SHOW PRODUCT VARIANTS AS CHECKBOXES=======================================================================
        $scope.getVariantValue = function(variants) {
		 
            $scope.variantsSelectedTitle = []; 
            var alreadyInVariants = '';
            var variantsIndex = 0;
            $scope.variant = variants;
            if ($scope.variantsArray.length == 0) {
			 
				
                $scope.variantsArray.push(variants);
				 
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) {
                if ($scope.variantsArray[i].item_variant_value_id == variants.item_variant_value_id) {
				 
					 
                    alreadyInVariants = 'YES';
                    variantsIndex = i;
                }
            }

            if (alreadyInVariants == 'YES') {
                $scope.variantsArray.splice(variantsIndex, 1);
				 
				 
                return;
            } 
			else {
                $scope.variantsArray.push(variants);
				 
                return;
            }
		}

		
		
		
		
		
	//FUNCTION FOR ADD PRODUCT VARIANTS TO CART============================================================================ 
        _this.addVariant = function() {
            // alert(JSON.stringify($scope.product)); 
            //$scope.value =  $('#v').val();
            //alert($scope.value);
            if ($scope.variantsArray == '') {
                alert('Please choose atleast one variant');
            } 
		    else {
			    $scope.item.variants = $scope.variantsArray;
                $scope.item.quantity = 1;
                var price = $scope.item.item_price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price) {
                        price = $scope.variantsArray[j].price;
                    }
                }

                var productToAdd = angular.copy($scope.item);
                productToAdd.item_price = price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].price_difference) {
                        productToAdd.item_price = parseInt(productToAdd.item_price) + parseInt($scope.variantsArray[j].price_difference);
                    }
                }
                productToAdd.discounted_price = productToAdd.item_price  ;
         
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.item.id == $scope.variantsArray[j].product_id) {
                        $scope.variantsSelectedTitle.push($scope.variantsArray[j].title);
                    }
                    $scope.variantsSelectedTitleData = $scope.variantsSelectedTitle.toString();
                }
			
			        
		   
                _this.cartStorage.items.push(productToAdd);
                $("#variantsModal").modal('hide');
			
			    
            }
        }
	
		 
	//========= FUNCTIONS FOR GET DATA SEARCH LIST VIA API===============================================================================================
    //===========================================================================================================================================
        _this.querySearch = function(query, field) {  
            return $http.get(APP_URL + "/" + field.api, {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };

	 
	
        _this.selectedValueChange = function(data,field ) { 
            $log.info('Data changed to ' + JSON.stringify(data));
            if (field.identifier == 'customer_id') {
                 field.value = data.value; 
            } 
            $scope.selected_values = '';
        }
		
		
	  

    });



//==========================================================CART CONTROLLER==========================================================================================================================
//========================================================================================================================================================================================================
    app.controller('cartController', function($http, cartStorage,$scope,$window, $log, $q, $timeout, toastr) {
        var _this = this;
        _this.cartStorage = cartStorage.cart;

	    //FUNCTION FOR INCREMENT item QUANTITY============================================================
        _this.increaseItemAmount = function(item) { 
            item.price = item.base_price;
            var quantity = item.quantity++;
        
		    
			document.getElementById('quantity').value = quantity;
            product.totalPrice = product.price * product.quantity;
		
        }

		
		//FUNCTION FOR DECREMENT PRODUCT QUANTITY IN CART============================================================
        _this.decreaseItemAmount = function(product) {
            product.quantity--;
		    $scope.order_products = _this.cartStorage.products;
			$scope.order_products.data = {"order_products": $scope.order_products};
				 


				console.log("URL - POST - "+APP_URL + '/place_order_calculate' );
		       	console.log("POST DATA - "+JSON.stringify($scope.order_products.data));
		    

		     var auth_app_type = document.getElementById('auth_app_type').value;
			var request = $http({
                method: "POST",
                url: APP_URL + '/place_order_calculate?auth_app_type='+auth_app_type,
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();					 
				console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            })
			.error(function(data, status, headers, config) { 
			   console.log("ERROR RESPONSE="+JSON.stringify(data)); 
            });
				
            if (product.quantity <= 0) {
			    product.quantity = 0;
                product.addedToCart = false;
                product.showAddToCart = false;
                var itemIndex = _this.cartStorage.products.indexOf(product);
                
				if (itemIndex > -1) {
				    _this.cartStorage.products.splice(itemIndex, 1);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.products);
                    _this.enableMe = true;
				}
            }
        }

		
		
		//FUNCTION FOR REMOVE PRODUCT FROM CART============================================================
        _this.removeFromCart = function(product) {
            var itemIndex = _this.cartStorage.products.indexOf(product);
            _this.cartStorage.products.splice(itemIndex, 1); 
		   
		    $scope.order_products = _this.cartStorage.products;
			$scope.order_products.data = {"order_products": $scope.order_products}; 


			console.log("URL - POST - "+APP_URL + '/place_order_calculate' );
		       	console.log("POST DATA - "+JSON.stringify($scope.order_products.data));
				
			var request = $http({
                method: "POST",
                url: APP_URL + '/place_order_calculate?auth_app_type='+auth_app_type,
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
             /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) { 
                _this.paymentData = data; 
                $(".main-cart").hide();		
                $(".cart").show();				
				console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            })
			.error(function(data, status, headers, config) { 
			    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
            });

            /*	   product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
			alert(itemIndex);
            if (itemIndex > -1) {
                var a =  _this.cartStorage.products.splice(itemIndex, 1);
				alert(JSON.stringify(a));
				alert(JSON.stringify(_this.cartStorage.products)); */
                document.getElementById('res1').value = JSON.stringify(_this.cartStorage.products);
            //$scope.enableMe = true;
            // }
        }
		
		
		
    });

function getPickupTime() {

    var x = document.getElementById("pickupTimePicker").value;

}