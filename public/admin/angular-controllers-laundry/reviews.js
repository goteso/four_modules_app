 
	
 //====================================================== REVIEW CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('reviewsController', function ($http, $scope, $window,toastr) {
 
	     // get basic form to add product ============================================================== clean done
	     
 var auth_app_type = document.getElementById('auth_app_type').value;
         $('#loading').css('display', 'block');
		  


		  			console.log("URL - POST - "+APP_URL+'/api/v1/order-review');
		 
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/order-review?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.reviews  = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  



		
		 //================================ FUNCTION FOR DELETE TIMELOT  ================================================================================
	 
    	 $scope.deleteReview = function(id) { 
			$scope.review_id = id; 
			if (confirm("Are you sure?")) {



							console.log("URL - DELETE - "+APP_URL+'/api/v1/order-review/'+$scope.review_id );
 
var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/order-review/'+$scope.review_id+'&auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		

			 

		    var auth_app_type = document.getElementById('auth_app_type').value;
		var request = $http({
            method: "GET",
            url: api_url+'&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.reviews = data;
            $('#loading').css('display', 'none');  
			 
        })
		.error(function (data, status, headers, config) {
		console.log("ERROR RESPONSE="+JSON.stringify(data));  
	 
        });       
		
		} 
		}
		
		
		
		
	});
		
		  
		
		
		 