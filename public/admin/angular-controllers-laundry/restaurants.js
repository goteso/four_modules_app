
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('faqController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		


        var auth_app_type = document.getElementById('auth_app_type').value;
		console.log("URL - POST - "+APP_URL+'/api/v1/faqs' );
		    
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/faqs?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.faqs = data;
            $('#loading').css('display', 'none');  
			
			  
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		
		
		
		
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		 pro.storeFaq = function(){ 
           var auth_app_type = document.getElementById('auth_app_type').value;
		   pro.question = $('#question').val();
		   pro.answer = $('#answer').val();
		   pro.faq_data = {question : pro.question , answer : pro.answer};


		   console.log("URL - POST - "+APP_URL+'/api/v1/faqs' );
		   console.log("POST DATA - "+JSON.stringify( pro.faq_data ));

		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/faqs?auth_app_type='+auth_app_type,
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
		  console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
             toastr.success(data.message, 'Success');
             location.reload();			 
			 
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		  $scope.AppendText = function() {
     var myEl = angular.element( document.querySelector( '#divID' ) );
     myEl.append('Hi<br/>');     
    }
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.editFaq = function(id,question,answer){  
           pro.faq_id = id;
		    pro.question = question;
            pro.answer = answer;			
		   $('#edit').modal('show');   
		 }
		 
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A FAQ=============================================================================================== 
		 pro.updateFaq = function(id){ 
		 	var auth_app_type = document.getElementById('auth_app_type').value;
           pro.faq_id = id; 
		   pro.question = $('#edit_question').val();
		   pro.answer = $('#edit_answer').val();  
		   pro.faq_data = {question : pro.question , answer : pro.answer};


		   			console.log("URL - PUT - "+APP_URL+'/api/v1/faqs/'+pro.faq_id );
		    console.log("POST DATA - "+JSON.stringify( pro.faq_data));

		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/faqs/'+pro.faq_id+'?auth_app_type='+auth_app_type,
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
           	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
             toastr.success(data.message, 'Success');
            location.reload();			 
			 
        })
		.error(function (data, status, headers, config) { 
		console.log("ERROR RESPONSE="+JSON.stringify(data));  
             toastr.error(data.message, 'Error');
		 
        });       
		 }
		 
		 
		 
		 
		 // FUNCTION FOR DELETE STORE  ============================================================== clean done
	pro.deleteFaq = function(faqId) {
		    
			pro.faq_id = faqId;
			
			if (confirm("Are you sure?")) {




							console.log("URL - DELETE - "+APP_URL+'/api/v1/faqs/'+pro.faq_id );
	 
		    var auth_app_type = document.getElementById('auth_app_type').value;
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/faqs/'+pro.faq_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
					console.log("ERROR RESPONSE="+JSON.stringify(data)); 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
   });