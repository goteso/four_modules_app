  app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/view-laundry/account-profile.html",
        controller : "profileController"
    })
    .when("/basic-settings", {
        templateUrl : APP_URL+"/view-laundry/account-basic-settings.html",
        controller : "settingsController"
    })
   .when("/change-password", {
        templateUrl : APP_URL+"/view-laundry/account-change-password.html",
        controller : "passwordController"
    })	;
});
 
app.controller('profileController', function($http,$scope,toastr) {
 	var auth_app_type = document.getElementById('auth_app_type').value;
    var auth_user_id = document.getElementById('auth_user_id').value;

           console.log('URL - GET -'+APP_URL+'/v1/form/user?'+auth_app_type)
  var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE ="+JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     console.log("ERROR RESPONSE ="+JSON.stringify(data));
        });  


		
		 //==========================function for update user======================================
		   $scope.updateUser = function(){
			 
			    var photo = $('#item_photo').val();
				 
				 for(var i=0;i<$scope.editUser[0].fields.length;i++){
				 if($scope.editUser[0].fields[i].identifier == 'photo'){
					 $scope.editUser[0].fields[i].value = photo; 
				 } 
				}
				
			console.log('URL - PUT -'+APP_URL+'/api/v1/form/user/'+auth_app_type);
            console.log('POST DATA -'+JSON.stringify($scope.editUser));
							
			var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+auth_app_type,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
           
            if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }
             
            

            $('#loading').css('display', 'none');    
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE ="+JSON.stringify(data));
        });  
 
		   };			   
 
});


 app.controller('settingsController', function($http,$scope,toastr,$location,$log) {
 var auth_app_type = document.getElementById('auth_app_type').value;
  console.log('URL - GET -'+APP_URL+'/api/v1/setting?exclude_type=image')
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/setting?exclude_type=image&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.settingsData  = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
 
 $scope.updateSettings = function(id,value){
	 var auth_app_type = document.getElementById('auth_app_type').value;
	 $scope.key_id = id;
	 $scope.key_value = value;
	 $scope.key_data = {"key_value":$scope.key_value};
	 console.log('URL - PUT -'+APP_URL+'/api/v1/setting/'+$scope.key_id+'&auth_app_type='+auth_app_type )
	 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/setting/'+$scope.key_id,
            data:  $scope.key_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;

			     if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }

            $('#loading').css('display', 'none');  
			location.reload();
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
	 
 }
 
});


app.controller('passwordController', function($http,$scope,toastr) {
	
	var auth_app_type = document.getElementById('auth_app_type').value;
	$scope.old_password = $('#old_password').val();
	$scope.new_password = $('#new_password').val();
	$scope.confirm_new_password = $('#confirm_new_password').val();
	
	$scope.changePassword = function(){
		
	if($scope.new_password != $scope.confirm_new_password){
		 toastr.error('Password does not match', 'Error');
		 return false;
	}
	
	else{

        console.log('URL - POST -'+APP_URL+'/api/v1/users/change-password' )
	$scope.passwordData = { "user_id":auth_app_type,   "new_password":$scope.new_password,   "old_password":$scope.old_password};
	 var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/change-password?auth_app_type='+auth_app_type,
            data:  $scope.passwordData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			
			if(data.status_code == 1){

				toastr.error('Password does not match', 'Error');
			   toastr.success(data.message, 'Success');
               
            $('#loading').css('display', 'none');  
			location.reload();
			}
			else{
				toastr.error(data.message, 'Error');
			}
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
	}
	}
});