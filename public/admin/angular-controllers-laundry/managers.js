 
	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr) {
 
	     // get basic form to add product ============================================================== clean done
	     
 
         $('#loading').css('display', 'block');
		  

		console.log("URL - GET - "+APP_URL+'/v1/form/user?user_type=4');
		 
 var auth_app_type = document.getElementById('auth_app_type').value;
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=4&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 


			//alert(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  




		
		  
		
		
		
		   // FUNCTION FOR STORE USER  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 
		   
		    for(var i=0;i<$scope.addUser[0].fields.length;i++)
		    { 
			     if($scope.addUser[0].fields[i].identifier == 'user_type')
			     {  $scope.addUser[0].fields[i].value = '4'; }
            }
			  
			  
			   var photo = $('#photo').val();
				 
				 for(var i=0;i<$scope.addUser[0].fields.length;i++){
				 if($scope.addUser[0].fields[i].identifier == 'photo'){
					 $scope.addUser[0].fields[i].value = photo; 
				 } 
				}
				

			console.log("URL - POST - "+APP_URL+'/api/v1/form/user' );
		    console.log("POST DATA - "+JSON.stringify( $scope.addUser ));
 var auth_app_type = document.getElementById('auth_app_type').value;
			var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_app_type='+auth_app_type,
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));   
            $scope.data  = data;
            $('#loading').css('display', 'none');
         
			       if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			 
			$scope.customer_id = data.data.user_id
			  $window.location.href = 'manager-laundry/'+$scope.customer_id;
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
         
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
		   };			   
		  
	});



 
	

	
//=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userEditController', function ($http, $scope, $window,toastr) {
 
  $scope.customer_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
	 
	        $('#loading').css('display', 'block');
 

 			console.log("URL - GET - "+APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=4" );
		  var auth_app_type = document.getElementById('auth_app_type').value;
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=4&auth_app_type="+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));  
            $scope.editUser  = data;
            $('#loading').css('display', 'none'); 

 for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
	 }			
		 
        })
		.error(function (data, status, headers, config) {
		    console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  


		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){
			    
				
				 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
			  			console.log("URL - PUT - "+APP_URL+'/api/v1/form/user/'+$scope.customer_id );
		    console.log("POST DATA - "+JSON.stringify( $scope.editUser ));

		     var auth_app_type = document.getElementById('auth_app_type').value;
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_app_type='+auth_app_type,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
        console.log("SUCCESS RESPONSE="+JSON.stringify(data));   
            $scope.data  = data; 
          

              if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


            $('#loading').css('display', 'none');    
			 
        })
		.error(function (data, status, headers, config) { 
		console.log("ERROR RESPONSE="+JSON.stringify(data));  	
		
            
                if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }
 
        });  
 
		   };		


//==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };


							console.log("URL - PUT - "+APP_URL+'/api/v1/users/update-password/'+$scope.customer_id );
		    console.log("POST DATA - "+JSON.stringify( $scope.passwordValue));

		     var auth_app_type = document.getElementById('auth_app_type').value;
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.customer_id+'?auth_app_type='+auth_app_type,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
		   
			}
           };		   
	});



