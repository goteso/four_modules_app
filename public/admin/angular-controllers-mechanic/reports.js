 app.directive('hcChart', function() {
     return {
         restrict: 'E',
         template: '<div id="container" style="margin: 0 auto">not working</div>',
         scope: {
             options: '='
         },
         link: function(scope, element) {
             var chart = new Highcharts.chart(element[0], scope.options);
             $(window).resize(function() {
                 chart.reflow();
             });
         }
     };
 })

 
 app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	

 

 
 //====================================================== REPORTS CONTROLLER=================================================================================
//==============================================================================================================================================================
 
 
 app.controller('reportsViewController', function($http, $scope, $window, $filter) {

  $('#loading').css('display', 'block');
  


  			console.log("URL - GET - "+APP_URL+'/api/v1/reports' );
	 var auth_app_type = document.getElementById('auth_app_type').value;
     var request = $http({
         method: "GET",
         url: APP_URL+'/api/v1/reports?auth_app_type='+auth_app_type,
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) { 
     	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
         $scope.reports = data; 
     $('#loading').css('display', 'none');
         //$scope.product_id = data["user_data"][0]["product_id"]; 
         //$window.location.href = 'product_edit/'+$scope.product_id;
     });






     $scope.revenueReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: ' {value}',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[0]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Revenue',
                 type: 'spline',
                 data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#4583EB', '#44a5f4', '#FF8080'],

     };




     $scope.orderReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: '{value}',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[0]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Orders',
                 type: 'spline',
                 data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#44a4f3', '#44a5f4', '#FF8080'],

     };




     $scope.userReport = {
         title: {
             text: ''
         },
         xAxis: {
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
             labels: {
                 rotation: -45,
                 style: {
                     fontSize: '11px',
                     fontFamily: 'Open Sans, sans-serif'
                 }
             }
         },
         yAxis: [{ // Primary yAxis
             labels: {
                 format: ' {value}',
                 style: {
                     color: Highcharts.getOptions().colors[1]
                 }
             },
             title: {
                 text: '',
                 style: {
                     color: Highcharts.getOptions().colors[1]
                 }
             }
         }],
         plotOptions: {
             series: {
                 label: {
                     connectorAllowed: false
                 }
             }
         },

         series: {
             showInNavigator: true
         },
         credits: {
             enabled: false
         },
         legend: {
             enabled: true,
             align: 'right',
             verticalAlign: 'top',
             layout: 'vertical',
             x: 0,
             y: 100
         },
         exporting: {
             enabled: false
         },

         // legend: { },
         options: {
             chart: {
                 color: Highcharts.getOptions().colors[1]
             }
         },
         rangeSelector: {
             floating: true,
             y: -65,
             verticalAlign: 'bottom'
         },

         navigator: {
             margin: 60
         },

         series: [{
                 name: 'Users',
                 type: 'spline',
                 data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 95.6, 54.4]
             }

             /**, 
					{ name:'Users Joined',
						type:'spline',
                        data: $scope.revenueReport[0].users_y_array
					},{ name:'Revenues',
						type:'spline',
                        data: $scope.revenueReport[0].revenues_y_array
					}**/

         ],

         colors: ['#4583EB', '#44a5f4', '#FF8080'],

     };




 });
 
 
 
 
 
 
 //====================================================== REPORTS DATA CONTROLLER=================================================================================
//==============================================================================================================================================================
 

 app.controller('reportDataController', function($http, $scope, $window) {

     $scope.determinateValue = 0;
     $scope.isLoading = true;
     $scope.isDisabled = false;


     			console.log("URL - GET - "+APP_URL + '/api/v1/reports' );
		var auth_app_type = document.getElementById('auth_app_type').value;     
     var request = $http({
         method: "GET",
         url: APP_URL + '/api/v1/reports?auth_app_type='+auth_app_type,
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) {
     	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 

         $scope.determinateValue = 50;

         setTimeout(function() {
             $scope.isLoading = false;
             $scope.isDisabled = true;
             //alert($scope.isLoading);
         }, 20);

         $scope.determinateValue = 100;
         $scope.isDisabled = true;


         $scope.reports = data;

         //$scope.product_id = data["user_data"][0]["product_id"];

         //$window.location.href = 'product_edit/'+$scope.product_id;
     });




 });