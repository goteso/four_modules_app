var app = angular.module("mainApp", [])


app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	
 
app.controller('reportController',  function($http,$scope,Excel,$timeout,$location, $filter) {
	  $('#loading').css('display', 'none');
	var url = $location.absUrl();
  
var n = url.lastIndexOf('/'); 
var result = url.substring(n + 1);
 

 
			 
//$scope.firstday = '';
//$scope.lastday = '';
$scope.title= 'This Week';
var curr = new Date; // get current date 
var day = curr.getDay();
//var first = curr.getDate(); // First day is the day of the month - the day of the week
//var last = first + 6; // last day is the first day + 6
var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day );
var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day+6 );
$scope.firstday = $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");



$scope.thisWeek = function(){ 
$scope.title= 'This Week';
var curr = new Date; // get current date 
var day = curr.getDay();
//var first = curr.getDate(); // First day is the day of the month - the day of the week
//var last = first + 6; // last day is the first day + 6
var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day );
var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day+6 );
$scope.firstday = $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
}

$scope.lastWeek = function(){ 
$scope.title= 'Last Week';
var curr = new Date; // get current date 
var day = curr.getDay(); 
var first = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day - 7);
var last = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() + (day == 0?-6:1)-day - 1 ); 
$scope.firstday = $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
 
}

$scope.thisMonth = function(){
$scope.title= 'This Month';	
var curr = new Date; // get current date 
var first = new Date(curr.getFullYear(), curr.getMonth(), 1);
var last = new Date(curr.getFullYear(), curr.getMonth() + 1, 0);
$scope.firstday =  $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
}

$scope.lastMonth = function(){ 
$scope.title= 'Last Month';
var curr = new Date; // get current date 
var first = new Date(curr.getFullYear(), curr.getMonth() - 1, 1);
var last = new Date(curr.getFullYear(), curr.getMonth() , 0);
$scope.firstday =  $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
}

$scope.thisQuarter = function(){ 
$scope.title= 'This Quarter';
var curr = new Date();
var quarter = Math.floor((curr.getMonth() / 3));
var first= new Date(curr.getFullYear(), quarter * 3, 1);
var last = new Date(first.getFullYear(), first.getMonth() + 3, 0); 
$scope.firstday = $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
}

$scope.lastQuarter = function(){
$scope.title= 'Last Quarter';	
var curr = new Date();
var quarter = Math.floor((curr.getMonth() / 3)); 
var first = new Date(curr.getFullYear(), quarter * 3 - 3, 1);
var last = new Date(first.getFullYear(), first.getMonth() + 3, 0); 
$scope.firstday =  $filter('date')(first, "yyyy-MM-dd");
$scope.lastday = $filter('date')(last, "yyyy-MM-dd");
}

//$scope.user_id = $('#userId').val();

 	 
 			console.log("URL - GET - "+APP_URL+'/api/v1/reports/'+result+'&date_from='+$scope.firstday+'&date_to='+$scope.lastday );
		    

		    var auth_app_type = document.getElementById('auth_app_type').value;
	      	   var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/reports/'+result+'&date_from='+$scope.firstday+'&date_to='+$scope.lastday+'&auth_app_type='+auth_app_type,
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
			console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
			 	 $scope.reports =  data; 
               $('#loading').css('display', 'none');
            });  
			
			
 
       $scope.IsVisible = true;
       $scope.IsHidden = false;
       $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsHidden =  true;
			$scope.IsVisible = false;
        };
			
	
	
	
	
	$scope.generateReport = function(){ 
       $('#loading').css('display', 'block');


       			console.log("URL - GET - "+APP_URL+'/api/v1/reports/'+result+'date_from='+$scope.firstday+'&date_to='+$scope.lastday );
		  
	      	   var request = $http({
                method: "GET",
                 url: APP_URL+'/api/v1/reports/'+result+'date_from='+$scope.firstday+'&date_to='+$scope.lastday+'&auth_app_type='+auth_app_type,
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
				 $scope.reports =  data; 
				 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
               $('#loading').css('display', 'none');
            });  
			
			
	}
	
      /**Get notes values from json**/
       angular.forEach($scope.reports, function(data) {
         if(data.notes == '') { 		   
	   $scope.notes = 'There are no any notes';
		 }
		 else{
	      $scope.notes = data.notes; 
		 }
       });		

      /** Submit notes form data **/ 
		$scope.submit = function() { 
         if(this.notes) {   
           $scope.notes = this.notes;
		   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		   //$scope.notes = '';
         }
		 if(this.notes == ''){
			  $scope.notes = 'There are no any notes';
			   $scope.IsHidden =  false;
		   $scope.IsVisible = true;
		 }
		 
		};
		
		
		
		$scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'Report');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        }
		
/*  $scope.refresh = function(){
    $http.get('/api/users')
          .success(function(data){
               $scope.users = data;
          });
} */ 





    $scope.printToCart = function(tableToExport) {
        var innerContents = document.getElementById('tableToExport').innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }
	
	
	
   $scope.export = function(){
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
        });
   };
  
});

