
	
	
	//======================================================LOGO UPLOAD CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('bannerController', function ($http,$scope,$window,toastr,$log) {
		
		 
	var auth_app_type = document.getElementById('auth_app_type').value;
	console.log('URL - GET -'+APP_URL+'/api/v1/banners');
		   
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/banners?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.banners = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		
		
		 // FUNCTION FOR DELETE  COUPON  ============================================================== clean done
	$scope.deleteBanner = function(bannerId) {
		    console.log('URL - GET -'+APP_URL+'/api/v1/banners/'+$scope.banner_id1);
			$scope.banner_id1 = bannerId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/banners/'+$scope.banner_id1+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
			   

			       if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

             console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			         console.log("ERROR RESPONSE="+JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values)); 
 			
	            $scope.linked_store_id =  values.store_id;  		
            }
			
			$scope.searchStoreChange = function(text){  
				   $scope.linked_store_id = ''; 
			};
			
			
			
 
    $scope.store_banner = function(id,value){
	 $scope.banner_image = $('#item_photo').val(); 
	 $scope.type = $('#banner_type').val();
	 $scope.linked_id = $('#linked_store_id').val();
	 $scope.banner_data = {"photo":$scope.banner_image, "type":$scope.type,  "linked_id":$scope.linked_id};
	 


	 console.log('URL - POST -'+APP_URL+'/api/v1/banners');
	 console.log('POST DATA -'+APP_URL+'/api/v1/banners/'+$scope.banner_id1);
	  
	 var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/banners?auth_app_type='+auth_app_type,
            data:  $scope.banner_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;

			  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }
   	             console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 


			 location.reload();
			 
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
			  	  console.log("ERROR RESPONSE="+JSON.stringify(data)); 
		     
        });  
 
	 
 }
 
 
 
 
 
  $scope.editBanner = function(values){ 
 $scope.store_title = values.store_title; 	  
$scope.banner_id = values.id;  
		 $scope.item_photo1 = values.photo;
           $scope.banner_edit_type = values.type;
		   $scope.linked_edit_store_id = values.linked_id;  
		     $('#editBan').modal('show');    
		
 	};
	
	  //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeEditSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedEditStoreChange = function(values) {
              $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.store_id =   values.store_id; 
               if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.linked_edit_store_id = $scope.store_id;
			    }				 
        }; 			
              
			
			$scope.searchEditStoreChange = function(text){  
				   $scope.linked_edit_store_id = ''; 
			};
	
	
	
	
	
	
	 $scope.update_banner = function(id){
	 	var auth_app_type = document.getElementById('auth_app_type').value;
		 $scope.banner_id = $scope.banner_id;
	 $scope.banner_image1 = $('#item_photo1').val(); 
	 $scope.edit_type = $('#banner_edit_type').val();
	 $scope.linked_edit_store_id = $('#linked_edit_store_id').val();
	 $scope.banner_data1 = {"photo":$scope.banner_image1, "type":$scope.edit_type,  "linked_id":$scope.linked_edit_store_id};
	 

	 console.log('URL - PUT -'+APP_URL+'/api/v1/banners/'+$scope.banner_id);
	 console.log('POST DATA -'+JSON.stringify($scope.banner_data1));
	  
	 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/banners/'+$scope.banner_id+'?auth_app_type='+auth_app_type,
            data:  $scope.banner_data1,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;



			  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 	 location.reload(); }
            else { toastr.error(data.message, 'Error');
 
             }
   
		
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });  
 
	 
 }
 
 
	});
