
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('faqController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 var auth_app_type = document.getElementById('auth_app_type').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		

		console.log("URL - GET - "+APP_URL+'/api/v1/faqs' );
		       


		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/faqs?auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.faqs = data;
            $('#loading').css('display', 'none');  
			
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		      console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_app_type='+auth_app_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.faqs = data;
            $('#loading').css('display', 'none');  
			 console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) { 
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		
		} 
		}
		
		
		
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		 pro.storeFaq = function(){ 
 var auth_app_type = document.getElementById('auth_app_type').value;
		   pro.question = $('#question').val();
		   pro.answer = $('#answer').val();
		   pro.faq_data = {question : pro.question , answer : pro.answer};

		   		console.log("URL - POST - "+APP_URL+'/api/v1/faqs' );
		        console.log("POST DATA - "+JSON.stringify(pro.faq_data));
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/faqs?auth_app_type='+auth_app_type,
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  
           	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
			if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}
			 
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     console.log("ERROR RESPONSE="+JSON.stringify(data)); 
        });       
		 }
		 
		 
		  $scope.AppendText = function() {
     var myEl = angular.element( document.querySelector( '#divID' ) );
     myEl.append('Hi<br/>');     
    }
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.editFaq = function(id,question,answer){  
           pro.faq_id = id;
		    pro.question = question;
            pro.answer = answer;			
		   $('#edit').modal('show');   
		 }
		 
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A FAQ=============================================================================================== 
		 pro.updateFaq = function(id){ 
		 	var auth_app_type = document.getElementById('auth_app_type').value;
           pro.faq_id = id; 
		   pro.question = $('#edit_question').val();
		   pro.answer = $('#edit_answer').val();  
		   pro.faq_data = {question : pro.question , answer : pro.answer};

		   console.log("URL - PUT - "+APP_URL+'/api/v1/faqs/'+pro.faq_id );
		        console.log("POST DATA - "+JSON.stringify(pro.faq_data));


		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/faqs/'+pro.faq_id+'?auth_app_type='+auth_app_type,
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
           	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
            pro.data = data;
             if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}	 
			 
        })
		.error(function (data, status, headers, config) { 
		console.log("ERROR RESPONSE="+JSON.stringify(data));  
             toastr.error(data.message, 'Error');
		     
        });       
		 }
		 
		 
		 
		 
		 // FUNCTION FOR DELETE STORE  ============================================================== clean done
	pro.deleteFaq = function(faqId) {
		    var auth_app_type = document.getElementById('auth_app_type').value;
			pro.faq_id = faqId;

			  console.log("URL - DELETE - "+APP_URL+'/api/v1/faqs/'+pro.faq_id );
	 


			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/faqs/'+pro.faq_id+'?auth_app_type='+auth_app_type,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	console.log("SUCCESS RESPONSE="+JSON.stringify(data)); 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
					console.log("ERROR RESPONSE="+JSON.stringify(data)); 			         
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
   });