-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2018 at 09:02 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `g_commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_meta_type`
--

CREATE TABLE `user_meta_type` (
  `user_meta_type_id` int(50) NOT NULL,
  `title` varchar(300) NOT NULL DEFAULT '',
  `identifier` varchar(100) NOT NULL DEFAULT '',
  `count_limit` int(50) NOT NULL DEFAULT '1',
  `type` varchar(50) NOT NULL DEFAULT '',
  `field_options` varchar(500) NOT NULL DEFAULT '',
  `user_type` varchar(300) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_meta_type`
--

INSERT INTO `user_meta_type` (`user_meta_type_id`, `title`, `identifier`, `count_limit`, `type`, `field_options`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Date of Birth', 'dob', 1, 'datePicker', '', '2', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(2, 'Gender', 'gender', 1, 'radio', '[{\"title\":\"Male\",\"value\":\"male\"},{\"title\":\"Female\",\"value\":\"female\"}]', '2', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(3, 'Driving Liscence', 'driving_liscence', 1, 'text', '', '5', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(4, 'Age', 'age', 1, 'text', '', '3', '2018-04-05 05:46:48', '0000-00-00 00:00:00'),
(5, 'Brand Name', 'brand_name', 1, 'text', '', '4', '2018-04-05 05:46:48', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_meta_type`
--
ALTER TABLE `user_meta_type`
  ADD UNIQUE KEY `id` (`user_meta_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_meta_type`
--
ALTER TABLE `user_meta_type`
  MODIFY `user_meta_type_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
