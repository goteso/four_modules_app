@extends('admin-grocery.layout.auth')
@section('title', 'Settings')
 <link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
 
@section('content')
@section('header')
@include('admin-grocery.includes.header')
@show

 <?php $auth_user_type = Auth::user()->user_type;   
$auth_user_id =   Auth::id();
App::setLocale('settings');   
?>

<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('admin-grocery.includes.sidebar')
      @show
      <!-- Modal -->
 
 <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none ;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						
				 
                  <div class="tab-content" >
				   <div class="container-fluid" >
				      <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Settings</h2>
                                 </div>
                                 <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    @include('admin-grocery.includes.realtime-search')
                                 </div>
                              </div>
				  

 

				  <div  class="row">
				     <div class="col-sm-12">
					   <div class="panel settings"> 
					      <div class="row">

<?php if( trans('permission'.$auth_user_type.'.view_account_settings') == '1' || trans('permission'.$auth_user_type.'.view_business_logo_settings') == '1' || trans('permission'.$auth_user_type.'.view_faqs_settings') == '1') {?>
						     <div class="col-sm-12"> 
							   <h5 class="text-uppercase">General Settings</h5>
							   <ul class="list-inline">

							   	 @if(trans('permission'.$auth_user_type.'.view_account_settings') == '1')
							     <li><a href="{{URL::to('v1/account-grocery')}}"><p class="gSetting"><i class="fa fa-user"></i></p><p class="text-center">Account</p></a></li>
							     @endif

							     @if(trans('permission'.$auth_user_type.'.view_business_logo_settings') == '1')
								 <li><a href="{{URL::to('v1/business-logo-grocery')}}"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Business Logo</p></a></li>
								 @endif 
								 
								  <li style="display:none"><a href="{{URL::to('v1/banners-grocery')}}"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Banners</p></a></li> 

								  @if(trans('permission'.$auth_user_type.'.view_faqs_settings') == '1')
								 <li><a href="{{URL::to('v1/faqs-grocery')}}"><p class="gSetting"><i class="fa fa-question-circle"></i></p><p class="text-center">FAQs</p></a></li>
								 @endif
 
								 <li style="display:none"><a href="{{URL::to('v1/update_store-grocery')}}"><p class="gSetting"><i class="fa fa-building"></i></p><p class="text-center">Update Store</p></a></li>
								 


							   </ul>
							 </div>
 <?php } ?>


<?php if( trans('permission'.$auth_user_type.'.view_timeslots_settings') == '1' || trans('permission'.$auth_user_type.'.view_tax_settings') == '1' ) {?>							 
							 <div class="col-sm-12"> 
							   <h5 class="text-uppercase">Admin Settings</h5>
							   <ul class="list-inline">

							   	 @if(trans('permission'.$auth_user_type.'.view_timeslots_settings') == '1')
							     <li><a href="{{URL::to('v1/timeslots-grocery')}}"><p class="aSetting text-center"><i class="fa fa-calendar-o"></i></p><p class="text-center">Timeslots</p></a></li>
							     @endif

							      @if(trans('permission'.$auth_user_type.'.view_tax_settings') == '1')
								 <li><a href="{{URL::to('v1/tax-grocery')}}"><p class="aSetting text-center"><i class="fa fa-percent"></i></p><p class="text-center">Tax</p></a></li>
								 @endif
								 
								  
								   <li style="display:none"><a href="{{URL::to('v1/stores/sort')}}"><p class="gSetting"><i class="fa fa-building"></i></p><p class="text-center">Sort Stores</p></a></li>
								   
							   </ul>
							 </div>
<?php } ?>							 
						



<?php if( trans('permission'.$auth_user_type.'.view_locations_settings') == '1' || trans('permission'.$auth_user_type.'.view_areas_settings') == '1' || trans('permission'.$auth_user_type.'.view_plugins_settings') == '1') {?>	 
							 <div class="col-sm-12"> 
							   <h5 class="text-uppercase">Logistics Settings</h5>
							   <ul class="list-inline">

							   	 @if(trans('permission'.$auth_user_type.'.view_locations_settings') == '1')
							     <li style="display:none"><a href="{{URL::to('v1/locations-grocery')}}"><p class="lSetting text-center"><i class="fa fa-location-arrow"></i></p><p class="text-center">Locations</p></a></li>
							     @endif

							     @if(trans('permission'.$auth_user_type.'.view_areas_settings') == '1')
							     <li><a href="{{URL::to('v1/areas-grocery')}}"><p class="lSetting text-center"><i class="fa fa-map-marker"></i></p><p class="text-center">Areas</p></a></li> 
							     @endif  

							      @if(trans('permission'.$auth_user_type.'.view_plugins_settings') == '1')
							     <li style="display:none"><a href="{{URL::to('v1/plugins-grocery')}}"><p class="lSetting text-center"><i class="fa fa-plug"></i></p><p class="text-center">Plugins</p></a></li> 
							     @endif

							   </ul>
							 </div>
<?php } ?>							 
						

 <?php if( trans('permission'.$auth_user_type.'.view_categories_settings') == '1' || trans('permission'.$auth_user_type.'.view_tags_settings') == '1' ) {?>	 	 
							 <div class="col-sm-12"> 
							   <h5 class="text-uppercase">Category Settings</h5>
							   <ul class="list-inline">

							   	 @if(trans('permission'.$auth_user_type.'.view_categories_settings') == '1')
							     <li><a href="{{URL::to('v1/categories-grocery')}}"><p class="nSetting text-center"><i class="fa fa-list-ul " aria-hidden="true"></i></p><p class="text-center">Categories</p></a></li>
							     @endif

							      @if(trans('permission'.$auth_user_type.'.view_tags_settings') == '1')
								 <li style="display:none"> <a href="{{URL::to('v1/tags-grocery')}}"><p class="nSetting text-center"><i class="fa fa-tags"></i></p><p class="text-center">Tags</p></a></li>
								 @endif


							   </ul>
							 </div>
<?php } ?>							 
							 
							 <!---<div class="col-sm-12"> 
							   <h5 class="text-uppercase">Notification Settings</h5>
							   <ul class="list-inline">
							     <li><p class="nSetting text-center"><i class="fa fa-envelope" aria-hidden="true"></i></p><p class="text-center">Email</p></li>
								 <li><p class="nSetting text-center"><i class="fa fa-"></i></p><p class="text-center">Push Notifications</p></li>  
							     <li><p class="nSetting text-center"><i class="fa fa-comment"></i></p><p class="text-center">SMS</p></li> 
							   </ul>
							 </div>-->
							 
						  </div>
                       </div>					   
					 </div>
				  </div>
		</div>
	  </div>
	  </div> 
	     </div>
   </div>
    
   </section>
</div>

</div>

</div>

  </div>

		
		 
 

<!----manage modal ends here--->
@endsection