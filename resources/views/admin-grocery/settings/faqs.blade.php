@extends('admin-grocery.layout.auth')
@section('title', 'FAQs' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
@section('content')
@section('header')
@include('admin-grocery.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-grocery.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
            @include('admin-grocery.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="faqController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display:  none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">FAQs</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>QUESTION</th>
									    <th>ANSWER</th>
                                       <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.faqs.data.data  ">
                                       <td>#@{{values.id}}</td>
                                       <td>@{{values.question}}</td>
									   <td>@{{values.answer}}</td>
                                       <td>@{{values.created_at_formatted}}</td>
                                       <td>
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editFaq(values.id, values.question, values.answer, $index)" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteFaq(values.id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div> 
						   
						    <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.faqs.data.first_page_url != null"  ng-click="ctrl.pagination(ctrl.faqs.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.faqs.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.faqs.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.faqs.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.faqs.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.faqs.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.faqs.data.last_page_url != null"  ng-click="ctrl.pagination(ctrl.faqs.data.last_page_url);">Last</button> 
                                       </div>
									   
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Faq</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-9"> 
                     <md-input-container  class="md-block">
                        <label>Question</label>
                        <input type="text" id="question" name="question"/>
                     </md-input-container>
                     <br>
					    <md-input-container  class="md-block">
                        <label>Answer</label>
                        <input type="text" id="answer" name="answer"/>
                     </md-input-container>
                     </div>
					 <div class="col-sm-3">
                     <md-button ng-click="ctrl.storeFaq()" class="md-raised bg-color md-submit">Add Faq</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
	
	
	
	
	
	 <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="edit" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Faq</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-9">
                       <md-input-container  class="md-block">
                        <label>Question</label>
                        <input type="text" ng-model="ctrl.question" id="edit_question" name="edit_question"/>
                     </md-input-container>
                     <br>
					    <md-input-container  class="md-block">
                        <label>Answer</label>
                        <input type="text" ng-model="ctrl.answer" id="edit_answer" name="edit_answer"/>
                     </md-input-container>
                      </div>
					 <div class="col-sm-3">
                     <md-button ng-click="ctrl.updateFaq(ctrl.faq_id)" class="md-raised bg-color md-submit">Update Faq</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-grocery/faq.js')}}"></script> 
@endsection