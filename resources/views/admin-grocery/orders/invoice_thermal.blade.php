<!DOCTYPE html>
<html>
<head>
	<!-- Define Charset -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Responsive Meta Tag -->
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

	<title>Invoice</title>
	
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	 
</head>
<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<body ng-app="mainApp">
<div  ng-controller="invoiceController" ng-cloak>
 
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="" >
  
  <tr >
     <td >
	  <table  width="100%" id="exportthis">
			 <tr>
				 <td>
		 <table  width="100%"  >
			 <tr>
				 <td>
				     <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="container590 "  >
								
								<tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
								
								<tr>
								 <td>
								 <table border="0"  width="100%"  >
								<tr >
								<td style="text-align:-webkit-center"><img width="156" border="0" style="display: block; width: 150px;"  src="{{ URL::asset('logo')}}/@{{invoice.app_logo}}" alt="logo" /> </td>
								</tr>
								<tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
								
								<tr>
								<td>
								<table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
								<tr>
								<td><img width="156" ng-show="invoice.barcode"  border="0" style= "width: 156px;" ng-src="{{ URL::asset('orders')}}/@{{invoice.order_id}}.png" alt="barcode" /></td>
								</tr>
								<tr>
								<td style="font-family:'Open Sans',sans-serif;font-size:20px"> INVOICE #@{{invoice.order_id}} </td>
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								 <td style="font-family:'Open Sans',sans-serif;" align="right"> 
								   <p style="line-height:20px;font-size:16px;margin:8px 0;">Date : @{{invoice.date}}
								  </p>
								</td> 
								</tr>
								</table>
								</td>
								</tr>
								
								 
								<tr>
								 <td>
								 
								  <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
								  
								<tr><td style="font-family:'Open Sans',sans-serif" >Customer Details : </td></tr>
								<tr>
								<td > 
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{invoice.customer_details.data[0].name}}</p>
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{invoice.customer_details.data[0].email}}</p>
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{invoice.customer_details.data[0].mobile}}</p> 
								</td>
								</tr>
								<tr >
								<td  style="font-family:'Open Sans',sans-serif;" > 
								 <span ng-show="invoice.customer_details.data[0].address.address_line1">@{{invoice.customer_details.data[0].address.address_line1}} </span><br>
								   <span ng-show="invoice.customer_details.data[0].address.address_line2">@{{invoice.customer_details.data[0].address.address_line2}}</span><br>
								   <span ng-show="invoice.customer_details.data[0].address.city">@{{invoice.customer_details.data[0].address.city}}</span><br>
								   <span ng-show="invoice.customer_details.data[0].address.state"> @{{invoice.customer_details.data[0].address.state}}</span><br>
									<span ng-show="invoice.customer_details.data[0].address.country"> @{{invoice.customer_details.data[0].address.country}}</span><br>
								</td>
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								<td align="right"  > 
								 <pre style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{invoice.vendor_details}}</pre>
								    
								</td>
								</tr>
								  <tr>
								<td align="right"  > 
								 <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;">GST NO. : <?php echo env('GST_NUMBER');?></p>
								    
								</td>
								</tr>
								 
								</table> 
									
									
									
								 </td>
								</tr>  
								</table>
								
								 
								
								</td>
								</tr>
								
								 
					</table>
				 </td>
			 </tr>
		 </table>
		 
		 <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0"  border="0"   >
		  <tr>
		    <td>
			   <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0"   style="border-top:1px solid #c5c5c5" >
								
					<tr><td height="5" style="font-size: 5px; line-height: 5px;">&nbsp;</td></tr>
								
					<tr>
						<td style="font-family:'Open Sans',sans-serif;">
						  <h4 style="margin:10px 0;">   @{{invoice.products.title}}</h4>
						</td>
					</tr>
					
					<tr>
					   <td>
					    <table border="0" width="100%" align="0" cellpadding="0" cellspacing="0"  style="border-bottom:1px solid #c5c5c5">
											
											<tr   >
												<td  align="left" style="  font-size: 14px; font-family: 'Open Sans', sans-serif; line-height: 26px;" >
													Description
												</td> 
												<td  align="right" style="  font-size: 14px; font-family: 'Open Sans', sans-serif; line-height: 26px;">
													Total
												</td> 
											</tr>
											<tr ng-repeat="value in invoice.products.data" >
												<td align="left" style="  font-size: 14px; font-family: 'Open Sans', sans-serif;  line-height: 26px;border-top: solid 1px #f1f1f1;">
													@{{value.title}}
													<pre ng-hide="!value.variants_desc" style="margin: 0px; font-size: 12px; line-height: 18px;font-family: 'Open Sans', sans-serif;">@{{value.variants_desc}}</pre>
												</td> 
												<td align="right" style="  font-size: 14px; font-family: 'Open Sans', sans-serif; line-height: 26px;border-top: solid 1px #f1f1f1;">
													<?php echo env("CURRENCY_SYMBOL", "");?>@{{value.discounted_price * value.quantity}}
												</td> 
											</tr>
										 <tr ><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
										</table>
					</td>
					</tr>
					
					<tr ><td height="10" style="font-size: 20px; line-height: 10px;">&nbsp;</td></tr>
								
								<tr >
									<td >
										<table border="0"   align="0" width="100%" cellpadding="0" cellspacing="0" >
										<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Subtotal
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													<?php echo @$currency_symbol;?>@{{invoice.order_details[0].sub_total}}
												</td>
											</tr>
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Discount
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													 @{{invoice.order_details[0].coupon_discount}}
												</td>
											</tr> 
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													Total
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													 <?php echo @$currency_symbol;?>@{{invoice.total1 }}
												</td>
											</tr> 
											
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Shipping
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													 @{{invoice.order_details[0].shipping}}
												</td>
											</tr> 
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													Express Delivery
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													 @{{invoice.order_details[0].express_shipping}}
												</td>
											</tr> 
											
											<tr>
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													Total
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													 <?php echo @$currency_symbol;?>@{{invoice.total2 }}
												</td>
											</tr> 
											
											<tr ng-repeat="data in invoice.order_tax_transactions">
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
												@{{data.tax_title}}
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;">
													@{{data.amount}}
												</td>
											</tr> 
											<tr  >
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													Total
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													<?php echo @$currency_symbol;?>@{{invoice.total3}}
												</td>
											</tr>
										</table>
									</td>
								</tr>
								
								
								
						 
					
					
								
				</table>
				 
				
			</td>
		  </tr> 
		 </table>
		  </td>
			 </tr>
		 </table>
		 
	  </td>
  </tr>
</table>

<div id="editor"></div>
<!--<button id="pdf-button" type="button" value="Download PDF" ng-click="export()">Download as PDF</button>-->

</div>
 
 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!----assets for pdf download--------->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> 
<script>
var app = angular.module("mainApp", []);
app.controller('invoiceController', function($http,$scope) {
  

$scope.invoice = <?php echo json_encode($invoice_data);?>;
  
 
		/*download as pdf**/
   $scope.export = function(){
   
    
        html2canvas(document.getElementById('exportthis'), {
		
		//var doc = new jsPDF("p", "mm", "a4"); 
       //var width = doc.internal.pageSize.width;    
       //var height = doc.internal.pageSize.height;

            onrendered: function (canvas) {
                var data2 = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data2,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("order.pdf");
            }
        });
   };
 
});
</script>
 <script src="https://docraptor.com/docraptor-1.0.0.js"></script>
  <script>
    var downloadPDF = function() {
      DocRaptor.createAndDownloadDoc("YOUR_API_KEY_HERE", {
        test: true, // test documents are free, but watermarked
        type: "pdf",
        document_content: document.querySelector('html').innerHTML, // use this page's HTML
        // document_content: "<h1>Hello world!</h1>",               // or supply HTML directly
        // document_url: "http://example.com/your-page",            // or use a URL
        // javascript: true,                                        // enable JavaScript processing
        // prince_options: {
        //   media: "screen",                                       // use screen styles instead of print styles
        // }
      })
    }
  </script>
</body>
</html>