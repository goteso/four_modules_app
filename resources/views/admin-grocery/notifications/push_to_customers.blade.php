@extends('admin-grocery.layout.auth')
@section('title', 'Notifications' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
@section('content')
@section('header')
@include('admin-grocery.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-grocery.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin-grocery.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="notificationController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Send Push Notifications to Customers</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12 panel" >
                        <div class="panel-body  category-add" >
						
						 <div class="row" >
                     <div class="col-sm-7" >
                            <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"     id="title" name="title" name="title"/>
                     </md-input-container>
                      
					  <md-input-container  class="md-block text-left">
                        <label>Sub Title</label>
                        <textarea id="sub_title" name="sub_title" rows="5" max-rows="5"  md-no-autogrow></textarea>
                     </md-input-container>
					 </div>
					 <div class="col-sm-5" ><br><br><br>
                     <md-button ng-click="sendNotification()" class="md-raised bg-color md-submit">Send</md-button>
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   
	
	 
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-grocery/notifications.js')}}"></script> 
@endsection