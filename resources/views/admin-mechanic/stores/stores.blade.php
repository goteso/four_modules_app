@extends('admin-grocery.layout.auth')
@section('title', 'Store Management' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">


<div ng-app="mainApp">
@section('content')
@section('header')
@include('admin-grocery.includes.header')
@show
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-grocery.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				       <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                           @include('admin-grocery.includes.realtime-search')
                       </div>
                      <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              
							  <div ng-view></div>
							  
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>



<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-grocery/stores.js')}}"></script> 
<!------>
@endsection