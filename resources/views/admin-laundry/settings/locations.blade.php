@extends('admin-laundry.layout.auth')
@section('title', 'Locations' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
@section('content')
@section('header')
@include('admin-laundry.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-laundry.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin-laundry.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="locationController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Locations</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           <button type="button" class="btn text-right md-raised bg-color md-add md-button md-ink-ripple"  data-toggle="modal" data-target="#add">Add New</button>  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>Id</th>
                                       <th>TITLE</th>
                                       <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.locations.data.data  ">
                                       <td>#@{{values.location_id}}</td>
                                       <td>@{{values.title}}<br>
									   <span ng-hide="!parent_location_title">Parent Title : @{{values.parent_location_title}}</span></td>
                                       <td>@{{values.created_at_formatted}}</td>
                                       <td>
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editLocation(values.location_id, values.title, values.parent_id, values.parent_location_title, $index)" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteLocationId(values.location_id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
						    <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.locations.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.locations.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="ctrl.locations.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.locations.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.locations.data.current_page}}</span>
                                          <button class="btn" ng-show="ctrl.locations.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.locations.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.locations.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.locations.data.last_page_url);">Last</button> 
                                       </div>
					 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Location</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-7">
                     <h5>Parent Location </h5> 
                      <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.selectedItem" md-search-text-change="ctrl.searchLocationChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedLocationChange(values)" md-items="values in ctrl.locationSearch(searchText)" md-item-text="values.title" md-min-length="0" placeholder="Select Parent Location"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{values.title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
						<input type="hidden" id="location_id" name="location_id" ng-model="location_id" value="@{{location_id}}" />
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"     id="location_title" name="location_title"/>
                     </md-input-container> 
					 </div>
					  <div class="col-sm-5">
                     <md-button ng-click="ctrl.storeLocation()" class="md-raised bg-color md-submit">Add Location</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
	
	
	
	
	
	 <!-----------------------------------------------LOCATION EDIT MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="edit" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Location</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-7">
                     <h5>Parent Location </h5>
                     <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.parent_location_title" md-search-text-change="ctrl.searchEditLocationChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedEditLocationChange(data)" md-items="data in ctrl.editLocationSearch(searchText)" md-item-text="data.title" md-min-length="0" placeholder="Select Parent Location"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{data.title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>

						<input type="text" id="location_edit_id" name="location_edit_id" ng-model="ctrl.parent_id" value="@{{location_edit_id}}" style="display:none;"/>

						<input type="text" id="location_edit_id" name="location_edit_id" ng-model="ctrl.parent_id" value="@{{ctrl.parent_id}}"style="display:none;" />

                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"  ng-model="ctrl.location_title"  id="location_edit_title" name="location_edit_title"/>
                     </md-input-container>
                    </div>
					  <div class="col-sm-5">
                     <md-button ng-click="ctrl.updateLocation(ctrl.location_id)" class="md-raised bg-color md-submit">Update Location</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------LOCATION EDIT MODAL ENDS HERE-------------------------------------------------------------------->
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-laundry/locations.js')}}"></script> 
@endsection