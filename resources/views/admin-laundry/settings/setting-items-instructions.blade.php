@extends('admin-laundry.layout.auth')
@section('title', 'Order Items Instructions' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
@section('content')
@section('header')
@include('admin-laundry.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-laundry.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
            @include('admin-laundry.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="settingItemsInstructionsController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display:  none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Setting Items Instructions</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>TITLE</th>
									            <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.setting_items_instructions.data.data  ">
                                       <td>#@{{values.instruction_id}}</td>
                                       <td>@{{values.title}}</td>
									            <td>@{{values.created_at_formatted}}</td>
                                       <td>
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editSettingItemsInstructions(values.instruction_id, values.title, $index)" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteFaq(values.instruction_id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div> 
						   
						    <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.setting_items_instructions.data.first_page_url != null"  ng-click="ctrl.pagination(ctrl.setting_items_instructions.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.setting_items_instructions.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.setting_items_instructions.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.setting_items_instructions.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.setting_items_instructions.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.setting_items_instructions.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.setting_items_instructions.data.last_page_url != null"  ng-click="ctrl.pagination(ctrl.setting_items_instructions.data.last_page_url);">Last</button> 
                                       </div>
									   
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Items Instructions</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-9"> 
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text" id="title" name="title"/>
                     </md-input-container>
                     <br>
					 
                     </div>
					 <div class="col-sm-3">
                     <md-button ng-click="ctrl.storeSettingItemsInstructions()" class="md-raised bg-color md-submit">Add Instruction</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
	
	
	
	
	
	 <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="edit" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Items Instructions</b></h4>
            </div>
            <div class="modal-body " style="margin-bottom:5%"  >
               <div class="row"   > 
                  <div class="col-sm-9">
                       <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text" ng-model="ctrl.title" id="edit_title" name="edit_title"/>
                     </md-input-container>
                     <br>
				 
                      </div>
					 <div class="col-sm-3">
                     <md-button ng-click="ctrl.updateSettingItemsInstruction(ctrl.instruction_id)" class="md-raised bg-color md-submit">Update</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-laundry/setting_items_instructions.js')}}"></script> 
@endsection