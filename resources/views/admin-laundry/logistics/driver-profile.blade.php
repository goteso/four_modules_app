@extends('admin-laundry.layout.auth')
@section('title', 'Driver Profile' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
<style>
   .ui-autocomplete {
   z-index: 999999 !important;} 
</style>
@section('content')
@section('header')
@include('admin-laundry.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin-laundry.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                        @include('admin-laundry.includes.realtime-search')
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none;" ></textarea>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid"  >
                              <div class="row" ng-controller="profileController" ng-cloak >
                                 <div class="col-sm-8 col-md-9 user-profile-data" >
                                    <div class="row"  >
                                       <div class="col-sm-12 col-md-6 col-lg-5"   >
                                          <div class="card info"  ng-repeat="values in profileData.data" >
                                             <img src="{{URL::asset('admin/assets/images/boy.png')}}" style="display:block;margin-right:0">
                                             <div>
                                                <h4>@{{values.first_name}} @{{values.last_name}} <a href="{{ URL::to('v1/driver-laundry') }}/@{{values.user_id}}"><i class="fa fa-edit"></i></a></h4>
                                                <h5>@{{values.email}}</h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <input type="hidden" ng-model="id" id="driver_id" value="@{{id}}">
                              </div>
                           </div>
                           <div ng-view></div>
                        </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<!------>  
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-laundry/driverProfile.js')}}"></script> 
@endsection