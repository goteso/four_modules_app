@extends('admin-laundry.layout.auth')
@section('title', 'Dashboard' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
<style>
   @media(min-width:1200px){
   .card{height:300px;overflow-y:auto} 
   }
   @media (min-width: 992px){
   .col-md-3.top-data {
   width: 20%;
   }}
   @media(min-width:2200px){
   .top-value{display:none;}
   .bottom-value{display:block;}	
   }
   @media(max-width:2199px) {
   .top-value{display:block;}
   .bottom-value{display:none;}	
   }
   @media(min-width:766px) and (max-width:1199px){
   .card{height:330px;overflow-y:auto} 
   } 
   hc-chart {
   padding-top:5px;
   width: 100%; 
   display: block;
   } 
</style>
@section('content')
@section('header')
@include('admin-laundry.includes.header')
@show

<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value; ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper">
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('admin-laundry.includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                     @include('admin-laundry.includes.realtime-search')
                  </div>
				  
				   <div class="col-sm-7 text-right " style="display:inline-block;" ng-controller="dashboardController">

                       <?php          $auth_user_type = Auth::user()->user_type;   
                                      $auth_user_id =   Auth::id();  
                        ?> 

                        
                    @if(trans('permission'.$auth_user_type.'.switch_store_availability') == '1')
                    <label>Store Availability</label>:  <md-switch class="text-center md-primary" ng-change="switchStoreStatus(busy);" ng-model="busy" value="@{{busy}}"  ng-true-value="1" ng-false-value="0"   style="display:inline-block;margin:0;padding:10px;"> </md-switch>
                    @endif
               </div>
				  
				   <textarea id="res" style="display: none " ></textarea>
						<textarea id="res1" style="display: none " ></textarea>
						<textarea id="res2" style="display:none  " ></textarea>
                        <div id="loading" class="loading" >
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">			 
                           <p >Calling all data...</p>
                        </div>
						
           
    <!-----------------------------------------------STORE BUSY ON OFF MODAL ENDS HERE-------------------------------------------------------------------->
	
	
                           </div>
						   
						   
						   
						   
               
               
						   
                <!--------------------------- APP NAVIGATION STARTS HERE --------------------->
						             <div class="row">
                         
                          <div class="col-lg-6 col-sm-6">
                               <a href="{{ URL::to('/admin/dashboard-grocery') }}">
                                 <img src="{{ URL::to('admin/assets/images/categories.png')}}" class="img-responsive center-block">
                                 <p class="text-center ">GROCERY</p>
                              </a>
                          </div>

                          <div class="col-lg-6 col-sm-6">
                              <a href="{{ URL::to('/admin/dashboard-laundry')}}">
                                <a href="{{ URL::to('/admin/dashboard-laundry')}}">
 
                                 <img src="{{ URL::to('admin/assets/images/categories.png')}}" class="img-responsive center-block">
                                 <p class="text-center">LAUNDRY</p>
                              </a>
                          </div>

                         </div>              


                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                               <a href="{{ URL::to('/admin/dashboard-mechanic') }}">
                                 <img src="{{ URL::to('admin/assets/images/categories.png')}}" class="img-responsive center-block">
                                 <p class="text-center">MECHANIC</p>
                              </a>
                            </div>

                            <div class="col-lg-6 col-sm-6">
                               <a href="{{ URL::to('/admin/dashboard-courier')}}">
                                 <a href="{{ URL::to('/admin/dashboard-courier')}}">
                                    <img src="{{ URL::to('admin/assets/images/categories.png')}}" class="img-responsive center-block">
                                 <p class="text-center">COURIER</p>
                              </a>
                            </div>
                        </div>
						   <!--------------------------- APP NAVIGATION ENDS HERE --------------------->
						   
						   
						   
						  
	
	
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
						
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-laundry/dashboard.js')}}"></script> 
<!----assets for pdf download--------->
<!------>
@endsection