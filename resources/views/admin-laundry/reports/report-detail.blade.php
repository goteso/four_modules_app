@extends('admin-laundry.layout.auth')
@section('title', 'Report Details' )
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}"> 
 <style>
 .caret:before{top:10px!important;}
 </style>
@section('content')
@section('header')
@include('admin-laundry.includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('admin-laundry.includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
	  
 

      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12"> 
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
					 <div id="loading" class="loading" >
                    <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
      <div ng-app="mainApp" ng-controller="reportController" ng-cloak>
         <div class="container-fluid" >
            <div class="row" >
               <div class="col-sm-12" >
			    
				
				 <div class="row">
					   <div class="col-sm-4" style="display:flex;">
					     <button class="btn btn-info edit" ng-click="ShowHide()" style="display:none">Edit Notes <i class="fa fa-pencil"></i></button>
						 <h5> Filter By : &nbsp;</h5>
						  <div class="dropdown">
						  <button class="btn  dropdown-toggle" type="button" data-toggle="dropdown" style="background:transparent;border:1px solid #ccc;"> Select Filter Type
						  <span class="caret"  ></span></button>
						  <ul class="dropdown-menu">
							<li><a href="#" ng-attr-title="This week" ng-click="thisWeek();">This Week</a></li>
							<li><a href="#" ng-click="lastWeek();">Last Week</a></li>
							<li><a href="#" ng-click="thisMonth();">This Month</a></li>
							<li><a href="#" ng-click="lastMonth();">Last Month</a></li>
							<li><a href="#" ng-click="thisQuarter();">This Quarter</a></li>
							<li><a href="#" ng-click="lastQuarter();">Last Quarter</a></li>
						  </ul>
						</div>
						<h5 style="padding-left:10px;font-family:'Open Sans Semibold',sans-serif">@{{title}}</h5>
					   </div> 
					   <div class="col-sm-6 form-inline">
					     <div class="form-group">
						  <label>From Date </label>
						  <input type="text" id="from_date" ng-model="firstday" date='dd-MM-yyyy' pattern="dd/MM/YYYY" value="@{{firstday}}" class="form-control">
						 </div>
						 <div class="form-group">
						  <label> - To Date</label>
						  <input type="text" id="to_date" ng-model="lastday"  date='dd-MM-yyyy' class="form-control">
						 </div>
					   </div>
					    <div class="col-sm-2">
						  <button class="btn btn-success" ng-click="generateReport();">Generate</button>
					   </div>
					   </div>
					   <br>
                  <div class="panel reports-panel">
                     <div class="panel-header">
					 <div class="row">
					   <div class="col-sm-6">
					     <button class="btn btn-info edit" ng-click="ShowHide()" style="display:none">Edit Notes <i class="fa fa-pencil"></i></button> 
						 </div>
					   <div class="col-sm-6 text-right">
					     <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
					     <i class="fa fa-file-excel-o" ng-click="exportToExcel('#tableToExport')" style="display: " title="Export Excel"></i> 
					     <i class="fa fa-cog" style="display:none"></i>
						 <i class="fa fa-print" onclick="myFunction()" style="display:none"></i>
					   </div>
					   </div>
                     </div>
                     <div class="panel-body"   ng-repeat="data in reports.data">
                        <h2 class="text-center title" >@{{data.header}}</h2>
                        <h4 class="text-center text-uppercase subtitle"> @{{data.subTitle}} </h4>
                        <h4 class="text-center date"> @{{data.dateRange}}</h4>
						<div id="tableToExport" class="table-responsive"> 
                        <table class="table" id="exportthis">
                           <tr>
                              <th ng-repeat="c in data.columns  track by $index"  > @{{c}}</th>
                           </tr>
                           <tbody >
                              <tr  ng-repeat="value in data.data">
                               <td  ng-repeat="values in value track by $index" >  @{{values}} </td> 
                              </tr>
                           </tbody>
                        </table>
						</div>
						
						<div class="row" > 
						 <div class="col-sm-12" ng-show= "IsVisible"> 
						  <p >@{{notes}}</p>
						  </div>
						 <form ng-show = "IsHidden" ng-submit="submit()"> 
						  <div class="col-sm-8 col-md-9 col-lg-10"> 
						      <textarea class="form-control" ng-model="notes" id="notes" name="notes">@{{data.notes}}</textarea>
						  </div>
						   <div class="col-sm-4 col-md-3 col-lg-2">
						     <button type="submit" class="btn btn-success">Save</button>
						   </div>
						 </form>
						</div>
						
                     </div>
                     <div class="panel-footer">
                        <p class="text-center">@{{data.footer}}</p>
                     </div>
                  </div> 
               </div>
            </div>
         </div>
      </div>
	  
	                      
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------>


		<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers-laundry/reportDetail.js')}}"></script> 
@endsection

