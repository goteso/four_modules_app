<?php
namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 

class DashboardVendorController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line.
 
   
   


 // Route- ============================================================== To get dashboard Data =========================================> 
 
   public function index(Request  $request)
   {
 		
      $dashboard_main_array = array();
      $dashboard_blocks_array = array();
      $dashboard_counts_array = array();

      if(isset($_GET['auth_user_id']) && $_GET['auth_user_id'] != '')
      {
        $auth_user_id = $_GET['auth_user_id'];
      }


      //blocks starts
      $dashboard_blocks_array[] = $this->get_recent_orders($request);
      $dashboard_blocks_array[] = $this->get_stores($request);
      //$dashboard_blocks_array[] = $this->get_today_pickup_orders($request);
      //$dashboard_blocks_array[] = $this->get_today_delivery_orders($request);
      $dashboard_blocks_array[] = $this->get_orders_graph_data($request);

      //count starts
      $store_id_array = @\App\Store::where('vendor_id',$auth_user_id)->pluck('store_id');
      $count_data['total_store'] = sizeof($store_id_array);
      $count_data['total_order'] = @\App\Order::whereIn('store_id', $store_id_array)->count();
      $count_data['total_tasks'] = @\App\Task::whereIn('store_id', $store_id_array)->count();

      $dashboard_counts_array[] = $count_data;

      $d['blocks'] = $dashboard_blocks_array;
      $d['counts'] = $dashboard_counts_array;

      return $d;

   }


 






// Fuction o give different types of data for dashboard page

   public function get_stores(Request $request)
   {
     $main = array();
     $d["type"] = 'stores'; 
     $d["title"] = 'Stores'; 
     $top_users = array();
 

    if(isset($_GET['auth_user_id']) && $_GET['auth_user_id'] != '')
     {
        $auth_user_id = $_GET['auth_user_id'];
     }


     $stores = @\App\Store::where('vendor_id',$auth_user_id)->get();
 
     $d["data"] = $stores;
     $main[] = $d;
     return $main;

   }



   public function get_recent_users()
   {

     $main = array();
     $d["type"] = 'recent_users'; 
     $d["title"] = 'Recent Users'; 
     $recent_users = \App\User::orderBy('created_at','DESC')->take(5)->get(['user_id','first_name','last_name','email','phone','photo','created_at']);
     $d["data"] = $recent_users;
     $main[] = $d;
     return $main;

       }

 
    public function get_recent_orders(Request $request)
   {
   	 $main = array();
     $d['type'] ='recent_orders';
     $d['title'] ='Recent Orders';

 if(isset($_GET['auth_user_id']) && $_GET['auth_user_id'] != '')
     {
        $auth_user_id = $_GET['auth_user_id'];
     }

     $store_id_array = @\App\Store::where('vendor_id',$auth_user_id)->pluck('store_id');
 

	   $orders = \App\Order::whereIn('store_id', $store_id_array)->orderBy('created_at','DESC')->get()->take(5);
     $d['data'] = $orders;
     $main[] = $d;
     return $main;
   }


    public function get_today_pickup_orders(Request $request)
   {
     //getting list of todays pickup order
      $main = array();
      $d["type"] = 'today_pickup_orders'; 
      $d["title"] = 'Todays Pickup Orders'; 
  
      $todays_orders_pickup_id = array();
      $todays_pickup_orders = array();

      $setting_order_meta_type_id = $this->get_setting_order_meta_type_id( $request , 'pickup_time');
      $present_date = \Carbon\Carbon::now()->format('Y-m-d')."";
      $todays_orders_pickup_id = @\App\OrderMetaValue::where('setting_order_meta_type_id',$setting_order_meta_type_id)->where('order_meta_value_text', 'like', "%".$present_date."%")->pluck('order_id');
  
     if(sizeof($todays_orders_pickup_id) > 0)
     {
       for($po=0;$po<sizeof($todays_orders_pickup_id);$po++)
        {
             $order = @\App\Order::where('order_id',$todays_orders_pickup_id[$po])->first();
             $todays_pickup_orders[] =$order;
        }
     }
 
      $d["data"] = $todays_pickup_orders;
      $main[] = $d;
      return $main;
  
   	
   }


   public function get_today_delivery_orders(Request $request)
   {
   	     //getting list of todays pickup order
      $main = array();
      $d["type"] = 'today_delivery_orders'; 
      $d["title"] = 'Todays Delivery Orders'; 
  
      $todays_orders_delivery_id = array();
      $todays_delivery_orders = array();

      $setting_order_meta_type_id = $this->get_setting_order_meta_type_id( $request , 'delivery_time');
      $present_date = \Carbon\Carbon::now()->format('Y-m-d')."";
      $todays_orders_delivery_id = @\App\OrderMetaValue::where('setting_order_meta_type_id',$setting_order_meta_type_id)->where('order_meta_value_text', 'like', "%".$present_date."%")->pluck('order_id');
  
     if(sizeof($todays_orders_delivery_id) > 0)
     {
       for($po=0;$po<sizeof($todays_orders_delivery_id);$po++)
        {
             $order = @\App\Order::where('order_id',$todays_orders_delivery_id[$po])->first();
             $todays_delivery_orders[] =$order;
        }
     }
 
      $d["data"] = $todays_delivery_orders;
      $main[] = $d;
      return $main;
   }




   public function get_orders_graph_data(Request $request)
   {
     //graph 3 starts here
      $data = array();
      $data_object["type"] = 'orders'; 
      $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates3[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
      $total3 = 0;

      foreach(array_reverse($dates3)  as $date)
      {
        $ud3['label'] = \Carbon\Carbon::parse($date)->format('M');
        $ud3['date'] = $date;
 
      if(isset($_GET['auth_user_id']) && $_GET['auth_user_id'] != '')
     {
        $auth_user_id = $_GET['auth_user_id'];
     }
        $store_id_array = @\App\Store::where('vendor_id',$auth_user_id)->pluck('store_id');
        $ud3['y'] = \App\Order::whereIn('store_id', $store_id_array)->whereDate('created_at', 'like', "%".$date."%")->count();
        $data[] = $ud3;
      }

      $data_object["total"] = '2000';
      $data_object["desc"] = "Total Order of Last 12 Months";
      $data_object["data"] = $data;

      $main_array[] = $data_object;
      return $main_array;
    }






















///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

  
   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'active'; }
    return $status;
}


   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	 
 
 


}