<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ItemsController extends Controller 
{
	

use one_signal;  
use bitcoin_price;  
use trait_functions;  
   
 
//Route-1.7 ================= Submit Add Item Form======================================
      public function submit_add_form(Request $request)
   {

               $basic_form_fields = @$request[0]['fields'];
               $meta_form_fields = @$request[1]['fields'];

               if(sizeof($basic_form_fields) > 0)
                {
                	 for($i=0;$i <sizeof($basic_form_fields);$i++)
                	 {
                	      $data[$basic_form_fields[$i]['identifier']] = @$basic_form_fields[$i]['value'];
                     }
                  
                	  
                }

                for($i=0;$i <sizeof($meta_form_fields);$i++)
                	 {
                	      $meta_data[$meta_form_fields[$i]['identifier']] = @$meta_form_fields[$i]['value'];
                     }
 
                $data['meta'] = $meta_data;
                 return $this->store( $request , $data);
  
  }



//Route-1.8 ================= Submit Edit Item Form======================================
      public function submit_edit_form(Request $request , $id)
   {



   	    //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

                $basic_form_fields = $request[0]['fields'];
                $meta_form_fields = $request[1]['fields'];

               if(sizeof($basic_form_fields) > 0)
                {
                	 for($i=0;$i <sizeof($basic_form_fields);$i++)
                	 {
                	      $data[$basic_form_fields[$i]['identifier']] = @$basic_form_fields[$i]['value'];


                     }
                  
                	  
                }

                for($i=0;$i <sizeof($meta_form_fields);$i++)
                	 {
                	      $meta_data[$meta_form_fields[$i]['identifier']] = $meta_form_fields[$i]['value'];
                     }
 
                $data['meta'] = $meta_data;

  
 
                return $this->update( $request , $id ,$data);
  
  }



 
 // Route-1.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request , $create_item_request = '')
   {
                
              if($create_item_request != '')
                {
                	$request = $create_item_request;
                }

              if($request['item_title'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Title Required';
                    return $data;	
               }

               if($request['item_price'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Price Required';
                    return $data;	
               }

    $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];  
          return $data;		  
       }


                 $item_active_status = $this->get_variable_item_active_status($request);
            
					       $item = new \App\Items;
					       $item->item_title = @$request['item_title'];
					       $item->item_price =  @$request['item_price'];
					       $item->item_discount = $this->validate_integer(@$request['item_discount']);
					       $item->item_discount_expiry_date =$this->validate_datetime(@$request['item_discount_expiry_date']);
					       $item->item_stock_count =$this->validate_integer(@$request['item_stock_count']);
					       $item->item_stock_count_type = $this->validate_string(@$request['item_stock_count_type']);
					       $item->item_photo = $this->validate_string(@$request['item_photo']);
                 $item->app_type = $this->validate_string(@$auth_app_type);

					       $item->item_thumb_photo = $this->validate_string(@$request['item_photo']);

                 if($item->item_photo != '')
                 {
                   $item->item_thumb_photo = 'thumb-'.$item->item_photo;
                 }
					       $item->item_tags = $this->validate_string(@$request['item_tags']);


                 //categories starts
                 $item_categories = @$request['item_categories'];
                 $item_categories_array_final = array();
                 foreach($item_categories as $item_category)
                 {
                    $item_categories_array_final[] = $item_category['value'];
                 }
                 $item->item_categories = $this->validate_string(@implode (",", @$item_categories_array_final ));

                        //categories starts
                 $item_tags = @$request['item_tags'];
                 $item_tags_array_final = array();
                 foreach($item_tags as $item_tag)
                 {
                    $item_tags_array_final[] = $item_tag['value'];
                 }
 
                 $item->item_tags = $this->validate_string(@implode (",", @$item_tags_array_final ));


                 
                 



					       $item->vendor_id = $this->validate_integer(@$request['vendor_id']);
					       $item->store_id = $this->validate_integer(@$request['store_id']);

 


                 $item->item_active_status = $item_active_status;
                 @$item->save();
 
        					//store meta value
        				 $item_meta = @$request['meta'];
        				 $this->store_meta_values($item->id , $item_meta);
					
				    if($item != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Product Added Successfully';
                          $data['data']      =   $item;  

				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Product';
                          $data['data']      =   [];  
					}
				   return $data;
				 
  }
  

   

 // Route-1.2 ============================================================== Get Item Details =========================================> 
   public function show($id)
   {
	    
                  //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}		

 
    $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];   
		  return $data;
       }




					
					$details = @\App\Items::where('app_type',$auth_app_type)->where('item_id',$id)->get();
					
					$meta_include_needed = $this->get_variable_include_meta();
					if($meta_include_needed == 'true' or $meta_include_needed == true)
					{
						$details[0]['item_meta_value'] = @\App\ItemMetaValue::where('item_id',$id)->get();
					}

          $details[0]['items_images'] = @\App\ItemsImages::where('item_id',$id)->get();
					
			 			
				    if($details != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Fetched Successfully';
                          $data['data']      =   $details;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Fetch Product';
                          $data['data']      =   [];  
					}
				   return $data;
				 
  }   
  
  // Route-1.3 ============================================================== Get Items List =========================================> 
   public function get_list()
   {
    $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];  
          return $data;		  
       }

    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$category = $this->get_variable_category();
		$tag = $this->get_variable_tag();
		$status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
		
		$model = new \App\Items;
		$model = $model::where('item_id' ,'<>', '0');  
    $model = $model->where('app_type' ,  $auth_app_type);  

   //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();

    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }

    if($user_type == '3') //vendor
    {   
        $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================
	 
		if($status != '' && $status != null)
		{   $model = $model->where('item_active_status' , $status);  }	

      if($store_id != '' && $store_id != null)
    {   $model = $model->where('store_id' , $store_id);  } 
	   
	    if($search != '' && $search != null)
		{   $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
	    if($category != '' && $category != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  }	
	
		if($tag != '' && $tag != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

        $model = $model->orderBy($orderby,$order);	
        $result = $model->paginate($per_page);
		
		//include meta values , default is false======
		if(isset($_GET['include_meta']) && $_GET['include_meta'] != '' && $_GET['include_meta'] == 'true')
		{
		    foreach($result as $item)
			  {
			    $item['item_meta_value'] = @\App\ItemMetaValue::where('item_id',$item->item_id)->get();
		    }
		}
					 
		      if(sizeof($result) > 0)
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item List Fetched Successfully';
                          $data['data']           =   $result;  
                          $data['tags']           =   @\App\SettingTags::get(['tag_id' , 'tag_title' , 'tag_photo']);  
				  }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Items Found';
                          $data['data']           =   [];  
                          $data['tags']           =   @\App\SettingTags::get(['tag_id' , 'tag_title' , 'tag_photo']);  
					}
				   return $data;
   }  



  // Route-1.4 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id , $create_item_request = '')
   {
	          if($create_item_request != '')
                {
                	$request = $create_item_request;
                }

              if($request['item_title'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Title Required';
                    return $data;	
               }

               if($request['item_price'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Price Required';
                    return $data;	
               }
			  
	                  //check existance of item with ID in items table
				          	$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
				          	}
				            $item_active_status = $this->get_variable_item_active_status($request);



	           
               if( $request['item_photo'] != '' &&  $request['item_photo'] != null )
                {
                  $item_thumb_photo = 'thumb-'.$request['item_photo'];
                }
                else
                {
                  $item_thumb_photo  = $this->validate_string(@$request['item_thumb_photo']);
                }

 
                //categories starts
                 $item_categories = @$request['item_categories'];
                 $item_categories_array_final = array();
                 foreach($item_categories as $item_category)
                 {
                    $item_categories_array_final[] = $item_category['value'];
                 }
                // 
 
                //tags starts
                 $item_tags = @$request['item_tags'];
                 $item_tags_array_final = array();
                 foreach($item_tags as $item_tag)
                 {
                    $item_tags_array_final[] = $item_tag['value'];
                 }
                // 
         
        	        $item =  \App\Items::where('item_id',$id)->update([
        					'item_title' => $request['item_title'],
        					'item_price' =>  @$request['item_price'],
        					'item_discount' => $this->validate_integer(@$request['item_discount']),
        					'item_discount_expiry_date' => $this->validate_datetime(@$request['item_discount_expiry_date']),
        					'item_stock_count' => $this->validate_integer(@$request['item_stock_count']),
        					'item_stock_count_type' => $this->validate_string(@$request['item_stock_count_type']),
        					'item_photo' => $this->validate_string(@$request['item_photo']),
                  'item_thumb_photo' => $this->validate_string(@$item_thumb_photo),
                  'item_tags' => $this->validate_string(@implode (",", @$item_tags_array_final )),
                  'item_categories' => $this->validate_string(@implode (",", @$item_categories_array_final )),
        					'vendor_id' => $this->validate_integer(@$request['vendor_id']),
        					'store_id' => $this->validate_integer(@$request['store_id']),
        					'item_active_status' => $item_active_status ]);
        		 
        					//update meta value
        				 	$item_meta = $request['meta'];
                 
        			  		$this->update_meta_values($id , $item_meta);
        	               
        				  $result = @\App\Items::where('item_id',$id)->get();
        			 			
        	        if(sizeof($result) > 0)
        					{
        						              $data['status_code']    =   1;
                                  $data['status_text']    =   'Success';             
                                  $data['message']        =   'Item Updated Successfully';
                                  $data['data']      =   $result;  
        				  }
        					else
        					{
        						              $data['status_code']    =   0;
                                  $data['status_text']    =   'Failed';             
                                  $data['message']        =   'Unable to Update';
                                  $data['data']      =   [];  
        					}
				          return $data;
   }  


   


  // Route-1.5 ============================================================== Items Delete =========================================> 

   public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

   	 $this->remove_id_from_coupons($id);
   	 @\App\Items::where('item_id',$id)->delete();
   	 @\App\ItemVariantValue::where('item_id',$id)->delete();
   	 @\App\ItemMetaValue::where('item_id',$id)->delete();

   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }





 // Route-1.6 ============================================================== item_active_status update =========================================> 

public function item_active_status_update(Request $request , $id = '')
{

  $validator = Validator::make($request->all(), [
          //'title' => 'required|unique:posts|max:255',
          'item_active_status' => 'required', //active , inactive , any
         
         
            ]);
     
        if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;         
                }


         $item_active_status = $request->item_active_status;
         $item =  \App\Items::where('item_id',$id)->update([ 'item_active_status' => $item_active_status ]);
         
         $result = @\App\Items::where('item_id',$id)->get();
            
        if(sizeof($result) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Updated Successfully';
                          $data['data']      =   $result;  
          }
        else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
          }
        return $data;

} 






public function remove_id_from_coupons($id)
{
     $coupons = @\App\Coupons::get();
     foreach($coupons as $coupon)
     {
     	  $new_included_items_id = array();
     	 $items_included = $coupon->items_included;
     	 $items_included_array = explode("," , $items_included);
     	  
     	 for($i=0;$i<sizeof($items_included_array);$i++)
     	 {
     	 	if($items_included_array[$i] != $id)
     	 	{
     	 		if($items_included_array[$i] != '' && $items_included_array[$i] != null)
     	 		{
     	 			$new_included_items_id[] = $items_included_array[$i];
     	 		}
     	 		
     	 	}
          }
     	 $new_string = implode("," , $new_included_items_id);
     	 App\Coupons::where('coupon_id', $coupon['coupon_id'])->update(['items_included' => $new_string]);
     }
}






  
   
   
   
   
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
   public function store_meta_values($item_id , $item_meta)
  {
	  
	      $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];  
          return 0;	  
       }

	   
	   
	  $item_meta_type = @\App\ItemMetaType::where('app_type',$auth_app_type)->get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
	    
		$item_meta_value = new \App\ItemMetaValue;
		$item_meta_value->item_meta_type_id = @$item_meta_type_id;
		$item_meta_value->item_id = @$item_id;
		$item_meta_value->value = $this->validate_string(@$value);
		$item_meta_value->save();
	  }
	  return 1;
  }
  
     public function update_meta_values($item_id , $item_meta)
  {
	       $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];  
          return 0;	  
       }

	   
	   
	    $item_meta_type = @\App\ItemMetaType::where('app_type',$auth_app_type)->get();
 
	  foreach($item_meta_type as $imt)
	  {
  		   $identifier = $imt->identifier;  
  	   	 $value = $this->validate_string($item_meta[$identifier]);
  	     $item_meta_type_id = $this->get_item_meta_type_id($identifier);

         $meta_value_exist = App\ItemMetaValue::where('item_id', $item_id)->where('item_meta_type_id', $item_meta_type_id)->count();
         if($meta_value_exist > 0 )
         {

                 App\ItemMetaValue::where('item_id', $item_id)->where('item_meta_type_id', $item_meta_type_id)->update(['value' => $value ]);
         }
         else
         {
                         $flight = new \App\ItemMetaValue;
                         $flight->item_id = $item_id;
                         $flight->item_meta_type_id = $item_meta_type_id;
                         $flight->value = $value;
                         $flight->save();
         }
 
	  }
	  return  1;
  }
  
  public function get_item_meta_type_id($identifer)
  {
	  
	        $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];  
          return 0;	  
       }
 
	  
	  $item_meta_type_id = @\App\ItemMetaType::where('app_type',$auth_app_type)->where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

  
   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'active'; }
    return $status;
}



   public function get_variable_item_active_status($request)
{
	 if(isset($request['item_active_status']) && $request['item_active_status'] != null && $request['item_active_status'] != '')
					{ $item_active_status = $request['item_active_status']; }
					else 
					{ $item_active_status = 'active'; }
    return $item_active_status;
}


   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	

   public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
} 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}