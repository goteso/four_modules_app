<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ItemVariantsController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
    
   
 // Route-4.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request)
   {

 
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'item_variant_type_id' => 'required',
					'item_variant_value_title' => 'required',
					'item_id' => 'required',
                 ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
				else
				{
					 
					$item_variant = new \App\ItemVariantValue;
					$item_variant->item_variant_type_id = $this->validate_string($request->item_variant_type_id);
					$item_variant->item_variant_value_title = $this->validate_string($request->item_variant_value_title);
					$item_variant->item_id =$this->validate_string($request->item_id);
					$item_variant->item_variant_price =$this->validate_integer($request->item_variant_price);
					$item_variant->item_variant_price_difference = $this->validate_string($request->item_variant_price_difference);
					$item_variant->item_variant_photo = $this->validate_integer($request->item_variant_photo);
					$item_variant->item_variant_stock_count = $this->validate_integer($request->item_variant_stock_count);
					$item_variant->save();
					 
		  
				    if($item_variant != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Variant Added Successfully';
                          $data['data']      =   $item_variant;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Item Variant';
                          $data['data']      =   [];  
					}
				   return $data;
				}
  }
  

 
  // Route-4.2 ============================================================== Get Items List =========================================> 
   public function get_list()
   {

 
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
 		$item = $this->get_variable_item();
		$type = $this->get_variable_type();
		 
		
		
		          $auth_user_id = $this->get_auth_user_id();
          $auth_user_type = $this->get_auth_user_type();
          $auth_app_type = $this->get_auth_app_type();
          if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
          else {  
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'App Type Required';
                $data['data']      =   [];  
                return $data;				
             }
			 
			 

		 if($type != '' && $type != null)
		{
		   $item_variant_type = @\App\ItemVariantType::where('app_type' , $auth_app_type)->where('item_variant_type_id',$type)->get(["item_variant_type_id","item_variant_type_title","selection_type"]); 
		}
		else
		{
  			 $item_variant_type = @\App\ItemVariantType::where('app_type' , $auth_app_type)->get();
		}

    
$item_variant_type_array = array();

 

       foreach($item_variant_type as $type)
       {
       		$model = new \App\ItemVariantValue;
			$model = $model::where('item_variant_value_id' ,'<>', '0');  
         
		if($item != '' && $item != null)
		{   
            $model = $model->where('item_id' , $item);  
		}	




            $model = $model->where('item_variant_type_id' , $type->item_variant_type_id );  	
        
            $model = $model->orderBy($orderby,$order);
	          $type->item_variant_values = $model->get();

	        if(count($type->item_variant_values) > 0)
	        {
	        	  $item_variant_type_array[] = $type;
	        }
          
       


	      
	    }
		
	 				 
	              if(sizeof($item_variant_type_array) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Variant Fetched Successfully';
                          $data['data']      =   $item_variant_type_array;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Items Variant Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-4.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	          $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'item_variant_type_id' => 'required',
					'item_variant_value_title' => 'required',
					'item_id' => 'required',
				]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			  
	               //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				 
	                App\ItemVariantValue::where('item_variant_value_id', $id)->update(
	                	[
	                		 'item_variant_type_id' => $request->item_variant_type_id ,
	                		 'item_variant_value_title' => $request->item_variant_value_title ,
	                		 'item_id' => $request->item_id ,
	                		 'item_variant_price' => $this->validate_integer($request->item_variant_price) ,
	                		 'item_variant_price_difference' => $this->validate_string($request->item_variant_price_difference) ,
	                		 'item_variant_photo' =>$this->validate_integer($request->item_variant_photo) ,
	                		 'item_variant_stock_count' => $this->validate_integer($request->item_variant_stock_count)

	                	]);
					 
				    $result = @\App\ItemVariantValue::where('item_variant_value_id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Variant Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  




// Route-4.4 ============= Delete Item Variant Here =============================================
   public function  destroy($id)
   {

   	 $exist = @\App\ItemVariantValue::where('item_variant_value_id',$id)->count();
   	 if($exist < 1)
   	 {
   	 		              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item Variant Id Not Found';
                          $data['data']      =   [];  
                          return $data;
   	 }


	                      $result = @\App\ItemVariantValue::where('item_variant_value_id',$id)->delete();
			 			
	             
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Variant Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
			  

   }


   







  
   
   
   
   
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\ItemVariantValue::where('item_variant_value_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	
 
   
  


///================================ function to check GET variable's and Defaults ====================================================//
 

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_item()
{
	 if(isset($_GET['item']) && $_GET['item'] != null && $_GET['item'] != '')
					{ $item = $_GET['item']; }
					else 
					{ $item = ''; }
    return $item;
}	

 
      
   
   public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}	

 
  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
 
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Harvinder Singh
     * set user language by default
     */
 
 
 


}