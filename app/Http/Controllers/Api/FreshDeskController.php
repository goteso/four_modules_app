<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
 
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
 
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class FreshDeskController extends Controller 
{
	
  
   
   
 

   
  
  // Route-27.1 ============================================================== Get Setting List =========================================> 
public function store(Request $request)
{
 

 $user_id = $request->user_id;
 $description = $request->description;

 $user_details = @\App\User::where('user_id',$user_id)->get();
 $email = 'harvindersingh@goteso.com';
 
 $main['description'] = $description;
 $main['user_details'] = $user_details;
 


    \Mail::send('emails.enquiry', $main, function($message) use (  $user_details , $email , $description) 
                            {  
                                $message->to( $email, env('APP_NAME'))->subject( env('APP_NAME').': Enquiry' );
                                $message->from('harvindersingh@goteso.com', env('APP_NAME'));
                            });




/**
 $this->store_freshdesk_enquiries($user_id , $description);

 
$api_key = "eWy7TFKzpdKBpSFHtnz1";
$password = "x";
 
$ticket_data = json_encode(array(
  "name" => $user_details[0]['first_name'],
  "email" => $user_details[0]['email'],
 // "facebook_id" => $request->facebook_id,
  //"unique_external_id" => $request->unique_external_id,
  "subject" => 'enquiry',
  "phone" => $user_details[0]['email'],
  "description" => $request->description,
  "priority" => 1,
  "status" => 2,
  "cc_emails" => array("harvindersingh@goteso.com", "yugalkishoregoyal@gmail.com")
));
$url = "https://goteso.freshdesk.com/api/v2/tickets";
$ch = curl_init($url);
$header[] = "Content-type: application/json";
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
curl_setopt($ch, CURLOPT_POSTFIELDS, $ticket_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$server_output = curl_exec($ch);
$info = curl_getinfo($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$headers = substr($server_output, 0, $header_size);
$response = substr($server_output, $header_size);
if($info['http_code'] == 201) {
 // echo "Ticket created successfully, the response is given below \n";
 // echo "Response Headers are \n";
 // echo $headers."\n";
//  echo "Response Body \n";
 
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =  'Server Error : EndPoints are wrong';
                    $data['data'] = json_decode($response);
                    return $data; 

} else {
  if($info['http_code'] == 404) {
                   
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =  'Server Error : EndPoints are wrong';
                    return $data; 
  } else {
   // echo "Error, HTTP Status Code : " . $info['http_code'] . "\n";
   // echo "Headers are ".$headers;

   **/
     
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =  'Enquiry Submitted Successfully';
                    $data['data'] = [];
                    return $data; 

 

}  


 


public function store_freshdesk_enquiries($user_id , $description)
{
        $flight = new \App\FreshdeskEnquiries;
        $flight->user_id = $user_id;
        $flight->description = $description;
        $flight->save();
        return 1;
}
 
 
 
 
 
   public function get_list()
   {
     $per_page = $this->get_variable_per_page(); 
     $model = new \App\FreshdeskEnquiries;
     $result = $model->paginate($per_page);


     
          if(sizeof($result) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Enquiries List Fetched Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Enquiries Found';
                          $data['data']      =   [];  
          }
           return $data;
   }  














///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}




public function get_variable_per_page()
{
   if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
          { $type = $_GET['per_page']; }
          else 
          { $type = '50'; }
    return $type;
}






public function get_variable_keys()
{
   if(isset($_GET['keys']) && $_GET['keys'] != null && $_GET['keys'] != '')
          { $keys = $_GET['keys']; }
          else 
          { $keys = ''; }
    return $keys;
}



 

 public function get_variable_exclude_type()
{
   if(isset($_GET['exclude_type']) && $_GET['exclude_type'] != null && $_GET['exclude_type'] != '')
          { $exclude_type = $_GET['exclude_type']; }
          else 
          { $exclude_type = ''; }
    return $exclude_type;
}
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 


}