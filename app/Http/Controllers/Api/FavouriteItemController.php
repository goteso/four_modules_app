<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class FavouriteItemController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 
 
 





   
 // Route-14.1 ============================================================== Add Favourite Item =========================================> 
   public function store(Request $request , $create_item_request = '')
   {
                
              if($create_item_request != '')
                {
                	$request = $create_item_request;
                }

              if($request['item_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Id Required';
                    return $data;	
               }

               if($request['user_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User Id Required';
                    return $data;	
               }
               $item_id = $request['item_id'];
               $user_id = $request['user_id'];


               $count = @\App\FavouriteItem::where('user_id',$user_id)->where('item_id',$item_id)->count();
               if($count > 0)
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Already in your favourite list ';
                    return $data; 
               }

                
				        $favourite_item = new \App\FavouriteItem;
				      	$favourite_item->user_id = @$user_id;
					      $favourite_item->item_id =  @$item_id;
                $favourite_item->save();
 
					
				    if($favourite_item != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Added to Favourite List';
                          $data['data']      =   $favourite_item;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add';
                          $data['data']      =   [];  
					}
				   return $data;
				 
  }
  

     // Route-14.2 ============================================================== Get Items List =========================================> 
   public function get_list()
   {
       
    $model = new \App\FavouriteItem;
    $model = $model::where('id' ,'<>', '0');  
    $result = $model->get();
     if(sizeof($result) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Favourite List Fetched Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Items Found';
                          $data['data']      =   [];  
          }
           return $data;
   }  

 

  // Route-14.3 ============================================================== Items Delete =========================================> 

   public function destroy($id)
   {
   	 
                          @\App\FavouriteItem::where('id',$id)->delete();

   	 	                    $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Removed from favourites';
                          $data['data']      =   [];  
                          return $data;
   }






 















 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
   public function store_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
	    
		$item_meta_value = new \App\ItemMetaValue;
		$item_meta_value->item_meta_type_id = @$item_meta_type_id;
		$item_meta_value->item_id = @$item_id;
		$item_meta_value->value = $this->validate_string(@$value);
		$item_meta_value->save();
	  }
	  return 1;
  }
  
     public function update_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
		App\ItemMetaValue::where('item_id', $item_id)->where('item_meta_type_id', $item_meta_type_id)->update(['value' => $value ]);
	  }
	  return 1;
  }
  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

  
   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'active'; }
    return $status;
}



   public function get_variable_item_active_status($request)
{
	 if(isset($request['item_active_status']) && $request['item_active_status'] != null && $request['item_active_status'] != '')
					{ $item_active_status = $request['item_active_status']; }
					else 
					{ $item_active_status = 'active'; }
    return $item_active_status;
}


   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}