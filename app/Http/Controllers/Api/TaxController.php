<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class TaxController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
 
 


   
  
  // Route-26.1 ============================================================== Get Users List =========================================> 
   public function get_list()
   {
	   
	          $auth_user_id = $this->get_auth_user_id();
          $auth_user_type = $this->get_auth_user_type();
          $auth_app_type = $this->get_auth_app_type();
          if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
          else {  
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'App Type Required';
                $data['data']      =   [];  
                return $data;				
             }
	   
    $model = new \App\SettingTax;
	$model = $model->where('app_type' , $auth_app_type);
    $model = $model->orderBy('setting_tax_id','DESC');	
    $result = $model->get(['setting_tax_id','title','percentage']); 
	    
 
	      if(sizeof($result) > 0)
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tax List Fetched Successfully';
                          $data['data']      =   $result;  
				  }
				else
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'No Tax Found';
                          $data['data']      =   [];  
					}
				  
          return $data;
   }  





  // Route-26.2 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id )
   {
     
 
             if($request['title'] == '' || $request['title'] == null)
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Title equired';
                    return $data; 
               }

               if($request['percentage'] == '' || $request['percentage'] == null)
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Percentage Required';
                    return $data; 
               }

  
              $tax =  @\App\SettingTax::where('setting_tax_id',$id)->update(['title' => $request['title'] , 'percentage'=>$request['percentage'] ]);
         
          
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tax Updated Successfully';
                          $data['data']      =   [];  
       
               return $data;
   }  
 



  
 
 


}