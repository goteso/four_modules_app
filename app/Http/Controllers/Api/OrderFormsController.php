<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class OrderFormsController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   




  // 1 ==================================================== show Order Form =====================================================================
  
  public function get_category_products($category_id,$path,$parent_category_title,$category_title)
  {

    $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];   
		  return $data;
       }


         $sub_categories = \App\SettingCategories::where('app_type', $auth_app_type )->where('parent_id',$category_id)->get();
    
       foreach($sub_categories as $sub_category)
       {
             $category_title = @\App\SettingCategories::where('app_type', $auth_app_type )->where('category_id',$sub_category['category_id'])->first(['category_title'])->category_title; 
             $s_count = \App\SettingCategories::where('app_type', $auth_app_type )->where('parent_id',$sub_category['category_id'])->count();
             $c["items"] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$sub_category['category_id'].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories']);
         
         
         
         if(intval($s_count) < 1)
         {
             $path .= "/".$category_title;
             $s['category_id'] = $sub_category['category_id'];
             $s['category_title'] = $category_title;
             $s['path'] = $path;
             $s['items'] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$sub_category['category_id'].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories']);
             $sub_categories_array[]= $s;
     
         }
         else
         {
             $path .= "/".$category_title;
             $s2['sub'] =$this->get_category_products($sub_category['category_id'],$path,$parent_category_title,$category_title);
             $s2['category_id'] = $sub_category['category_id'];
             $s2['category_title'] = $category_title;
             $s2['items'] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$sub_category['category_id'].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories']);
             $sub_categories_array[]= $s2;
         }
       }
       
       return $sub_categories_array;
  }
  
  
  public function get_add_form(Request $request)
  {

        $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];   
		  return $data;
       }
    
    $main = array();
    $categories = array();
    $root_categories = \App\SettingCategories::where('app_type', $auth_app_type )->where('parent_id','')->orWhere('parent_id',0)->pluck('category_id');
    
 
 
    
   
    for($i=0;$i<sizeof($root_categories);$i++)
    {
          $path ='';
          $path2 ='';
      
          $parent_category_title = @\App\SettingCategories::where('app_type', $auth_app_type )->where('category_id',$root_categories[$i])->first(['category_title'])->category_title;
          $path = $parent_category_title;
          $path2 = $parent_category_title;
          $sub_categories = \App\SettingCategories::where('app_type', $auth_app_type )->where('parent_id',$root_categories[$i])->get();

          $c["items"] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$root_categories[$i].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories']);
           
          $sub_categories_array = array();
        

       foreach($sub_categories as $sub_category)
       {
               
           $category_title = $sub_category['category_title'];
           $s_count = \App\SettingCategories::where('app_type', $auth_app_type )->where('parent_id',$sub_category['category_id'])->count();
         
        if(intval($s_count) < 1)
         {
            $path2 .= "/".$category_title;
            $s['category_id'] = $sub_category['category_id'];
            $s['category_title'] = $category_title;
            $s['path'] = $path2;
            $s['items'] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$sub_category['category_id'].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories']);
            $sub_categories_array[]= $s;
     
         }
         else
         {
            $path .= "/".$category_title;
            $s2['sub_categories'] =$this->get_category_products($sub_category['category_id'],$path,$parent_category_title,$category_title);
            $s2['category_id'] = $sub_category['category_id'];
            $s2['category_title'] = $category_title;
            $s2['path'] = $path;
           
            $s2['items'] = \App\Items::where('app_type', $auth_app_type )->whereRaw("FIND_IN_SET(".$sub_category['category_id'].",items.item_categories)")->get(['item_id','item_title','item_price','item_photo','item_categories' ]);
            $sub_categories_array[]= $s2;
          }
       }




       
       $c['sub_categories'] = $sub_categories_array; 
       $c['category_id'] = $root_categories[$i]; 
       $c['category_title'] = $parent_category_title; 
       $categories[] = $c;
    }
    
   return $this->order_meta_form();
    $data['categories_products'] = $categories;
    $data['order_meta_fields'] = $this->order_meta_form();
    $main = $data;
    return $main;
  }








  
  public function order_meta_form()
{
        $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];   
          return $data;
       }

      $setting_orders_meta_types = \App\SettingOrderMetaType::where('app_type', $auth_app_type )->get(['setting_order_meta_type_id','setting_order_meta_type_title','important','required_or_not','field_options','identifier','parent_identifier','input_type','show_bool','display_show_rule','api']);

    $fields_array2 = array();
    foreach($setting_orders_meta_types as $umt)
    {     
      
           if($umt->field_options_model != '' && $umt->type != 'api')
            {
              $modelName = $umt->field_options_model;  
              $model = new $modelName();
              $columns_array = explode(",",$umt->field_options_model_columns);
              $columns_data = array();
              $columns_data[] = $columns_array[0];
              $columns_data[] = $columns_array[1];
              $col0 =  $columns_array[0];
              $col1 =  $columns_array[1];
              $data_field_options = $model::get($columns_data);
        
              $t = array();
              foreach($data_field_options as $fo)
               {
                     $d['title'] = $fo->$col0;
                     $d['value'] = $fo->$col1;
                    $t[] = $d;
                } 

              $umt->field_options =$t;
              $user_meta_type_id = $umt->id;  
              $umt->value  = '';
            }


            else if($umt->field_options != '')
            {
                  $user_meta_type_id = $umt->id;
                  $umt->field_options = json_decode($umt->field_options);
                  $umt->value  = '';
            }
 
           else
           {
                   $user_meta_type_id = $umt->id;
                   $umt->value  = '';
           }


         if($umt->field_options == '')
            {
                  $user_meta_type_id = $umt->id;
                  $umt->field_options =[];
                  $umt->value  = '';
            }
     } 

    
    $parent_array = array();
    foreach( $setting_orders_meta_types as $mt)
      {
            if($mt->parent_identifier == '' or $mt->parent_identifier == ' ' or $mt->parent_identifier == null )
            {
               $parent_array[] = $mt;
            }
      }
    
 
    $final_array = array();
    $child_array = array();
    foreach($parent_array as $pa)
    {
       $c =array();
       $c[] = $pa;
           foreach( $setting_orders_meta_types as $mt)
               {
 
                  if($pa->identifier == $mt->parent_identifier)
                   {              
                        $c[] = $mt;
                   }
               }
       $final_array[] = $c; 
    }
  
    //return $orders_meta_types;  //old
    $data_array2["title"] = "Additional Information";
    $data_array2["fields"] = $final_array;
    return $data_array2;
}

















































 
 

public function apply_setting_tax_to_order($order_total)
{
     $setting_tax = @\App\SettingTax::get();
     $total_tax_amount = 0;
     foreach($setting_tax as $tax)
     {
      $tax_amount = floatval($tax->percentage) / 100 * $order_total;
      $tax['order_transaction_tax_amount'] = $tax_amount;
      $total_tax_amount = $total_tax_amount + $tax_amount;
     }
      $order_total = $order_total + $total_tax_amount;

      $d['setting_tax'] = $setting_tax;
      $d['total_tax_amount'] = $total_tax_amount;
      $d['order_total'] = $order_total;
      return $d;
}




 
 

  






     // Route-60 ============================================================== Get Orders List =========================================> 
   public function get_list()
   {
    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $store_id = $this->get_variable_store_id();
    $customer_id = $this->get_variable_customer_id();
 
    $status = $this->get_variable_status(); //default Pending
    

        $auth_user_id = $this->get_auth_user_id();
    $auth_user_type = $this->get_auth_user_type();
    $auth_app_type = $this->get_auth_app_type();
    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
    else {  
          $data['status_code']    =   0;
          $data['status_text']    =   'Failed';             
          $data['message']        =   'App Type Required';
          $data['data']      =   [];   
       }



    $model = new \App\Order;
    $model = $model::where('order_id' ,'<>', '0');  
     $model = $model->where('app_type' , $auth_app_type);  
    
         
   
    if($status != '' && $status != null)
    {  $model = $model->where('order_status' , $status);  } 

      if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 

      if($customer_id != '' && $customer_id != null)
    {  $model = $model->where('customer_id' , $customer_id);  } 
     
 
  
        $model = $model->orderBy($orderby,$order);  
        $result = $model->paginate($per_page);
    
 
       
     
                if(sizeof($result) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item List Fetched Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Items Found';
                          $data['data']      =   [];  
          }
           return $data;
   }  




 


 
 




public function remove_id_from_coupons($id)
{
     $coupons = @\App\Coupons::get();
     foreach($coupons as $coupon)
     {
     	  $new_included_items_id = array();
     	 $items_included = $coupon->items_included;
     	 $items_included_array = explode("," , $items_included);
     	  
     	 for($i=0;$i<sizeof($items_included_array);$i++)
     	 {
     	 	if($items_included_array[$i] != $id)
     	 	{
     	 		if($items_included_array[$i] != '' && $items_included_array[$i] != null)
     	 		{
     	 			$new_included_items_id[] = $items_included_array[$i];
     	 		}
     	 		
     	 	}
          }
     	 $new_string = implode("," , $new_included_items_id);
     	 App\Coupons::where('coupon_id', $coupon['coupon_id'])->update(['items_included' => $new_string]);
     }
}






  
   
   
   
   
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
   public function store_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
	    
		$item_meta_value = new \App\ItemMetaValue;
		$item_meta_value->item_meta_type_id = @$item_meta_type_id;
		$item_meta_value->item_id = @$item_id;
		$item_meta_value->value = $this->validate_string(@$value);
		$item_meta_value->save();
	  }
	  return 1;
  }
  
     public function update_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
		App\ItemMetaValue::where('item_id', $item_id)->where('item_meta_type_id', $item_meta_type_id)->update(['value' => $value ]);
	  }
	  return 1;
  }
  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
 

  
   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'pending'; }
    return $status;
}

   public function get_variable_customer_id()
{
   if(isset($_GET['customer_id']) && $_GET['customer_id'] != null && $_GET['customer_id'] != '')
          { $customer_id = $_GET['customer_id']; }
          else 
          { $customer_id = ''; }
    return $customer_id;
}
   public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
}



 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}