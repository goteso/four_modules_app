<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class FilterOptionsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route-31.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request)
   {

  
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'identifier' => 'required|unique:store_filter_options',
					'value' => 'required',
					'type' => 'required',
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
                

					$store_filter_options = new \App\FilterOptions;
				    $store_filter_options->identifier = $this->validate_integer($request->identifier);
				    $store_filter_options->value = $this->validate_integer($request->value);
				    $store_filter_options->type = $this->validate_integer($request->type);
                    $store_filter_options->save();
					
				    if($store_filter_options != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Filter Added Successfully';
                          $data['data']      =   $store_filter_options;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Filter';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route-31.2 ============================================================== Get Categories List =========================================> 
   public function get_list()
   {
	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$type = $this->get_variable_type();
 
        $model = new \App\FilterOptions;
		$model = $model::where('id' ,'<>', '0');  
        $model = $model->orderBy($orderby,$order);

        if( $type != ''  )
        {
     
        	$model = $model->where('type',$type);
        }
        
	    if($search != '' && $search != null)
		{   $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(identifier,' ',type,' ',value)"),'like', '%'.$search.'%'); });  }
      
	    $result = $model->paginate($per_page);
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Filters Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Filters Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  





     // Route-31.5 ============================================================== Get Categories List =========================================> 
   public function get_filters()
   {

   	  
	    $main = array();
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
 		$filter_category = $this->get_variable_filter_category();

        $model = new \App\FilterOptions;
		$model = $model::where('id' ,'<>', '0');  
     

        if($filter_category == 'store')
        {
        	$selectable_types = array("Meat Types");
        }

        $distinct_types = @\App\FilterOptions::whereIn('type',$selectable_types)->distinct('type')->pluck('type');
        $distinct_types = $distinct_types->toArray();
 
      
      if(sizeof($distinct_types) > 0)
      { 
        for($t=0;$t<sizeof($distinct_types);$t++)
        {
          $d['type'] = $distinct_types[$t];
          $d['filters'] = @\App\FilterOptions::where('type',$distinct_types[$t])->get();
          $main[] = $d;
        }
    }

        
 
	   
	              if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Filters Fetched Successfully';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Filters Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  





  // Route-31.4 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
						//'title' => 'required|unique:posts|max:255',
						'identifier' => 'required',
					'value' => 'required',
					'type' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Categories with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
					 
	                $identifier = $this->validate_integer($request->identifier);
	                $type = $this->validate_integer($request->type);
	                $value = $this->validate_integer($request->value);
				 
	                @\App\FilterOptions::where('id', $id)->update(['identifier' => $identifier ,'type' => $type ,'value' => $value  ]);
	               
				    $result = @\App\FilterOptions::where('id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Filter Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
 
 
 
    // Route-31.3 ========================================================To delete Location =========================================> 
 
  
   public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
						 	$exist = $this->model_exist($id);	
		                    if($exist == 0 or $exist == '0')
		                    {
								  $data['status_code']    =   0;
		                          $data['status_text']    =   'Failed';             
		                          $data['message']        =   'Filter with this ID does not exist';
		                          $data['data']      =   [];
		                          return $data;						  
							}

	   	 
	   	 					  @\App\FilterOptions::where('id',$id)->delete();
	    
	                          $data['status_code']    =   1;
	                          $data['status_text']    =   'Success';             
	                          $data['message']        =   'Filter Deleted Successfully';
	                          $data['data']      =   [];  
	                          return $data;
   }





 
 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\FilterOptions::where('id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_parent_id()
{
	 if(isset($_GET['parent_id']) && $_GET['parent_id'] != null && $_GET['parent_id'] != '')
					{ $parent_id = $_GET['parent_id']; }
					else 
					{ $parent_id = ''; }
    return $parent_id;
}	

   public function get_variable_parents_only()
{
	 if(isset($_GET['parents_only']) && $_GET['parents_only'] != null && $_GET['parents_only'] != '')
					{ $parents_only = $_GET['parents_only']; }
					else 
					{ $parents_only = 'false'; }
    return $parents_only;
}	

public function get_variable_filter_category()
{
	 if(isset($_GET['filter_category']) && $_GET['filter_category'] != null && $_GET['filter_category'] != '')
					{ $filter_category = $_GET['filter_category']; }
					else 
					{ $filter_category = ''; }
    return $filter_category;
}	



public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}	

  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}