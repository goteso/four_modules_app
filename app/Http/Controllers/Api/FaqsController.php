<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class FaqsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 
   
 // Route-7.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request)
   {

			   	$auth_user_id = $this->get_auth_user_id();
			    $auth_user_type = $this->get_auth_user_type();
			    $auth_app_type = $this->get_auth_app_type();
			    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
			    else {  
			          $data['status_code']    =   0;
			          $data['status_text']    =   'Failed';             
			          $data['message']        =   'App Type Required';
			          $data['data']      =   []; 
						return $data;					  
			       }



                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'question' => 'required',
					'answer' => 'required',
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
					$category = new \App\Faqs;
					$category->app_type = @$auth_app_type;
					$category->question = @$request->question;
					$category->answer = $this->validate_string($request->answer);
		            $category->save();
					
				    if($category != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Faq Added Successfully';
                          $data['data']      =   $category;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Faq';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route-7.2 ============================================================== Get Categories List =========================================> 
   public function get_list()
   {

			   	$auth_user_id = $this->get_auth_user_id();
			    $auth_user_type = $this->get_auth_user_type();
			    $auth_app_type = $this->get_auth_app_type();
			    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
			    else {  
			          $data['status_code']    =   0;
			          $data['status_text']    =   'Failed';             
			          $data['message']        =   'App Type Required';
			          $data['data']      =   [];   
					  return $data;
			       }


	   $per_page = $this->get_variable_per_page(); 
		$model = new \App\Faqs;
	    $result = $model->where('app_type' , $auth_app_type )->paginate($per_page);
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Faqs List Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Category Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-7.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	   
	   			   	$auth_user_id = $this->get_auth_user_id();
			    $auth_user_type = $this->get_auth_user_type();
			    $auth_app_type = $this->get_auth_app_type();
			    if($auth_app_type == 'laundry' || $auth_app_type == 'grocery' || $auth_app_type == 'courier' || $auth_app_type == 'mechanic') {} 
			    else {  
			          $data['status_code']    =   0;
			          $data['status_text']    =   'Failed';             
			          $data['message']        =   'App Type Required';
			          $data['data']      =   [];   
					  return $data;
			       }

			       
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
							'question' => 'required',
					        'answer' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Faq with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
				 
					$question = $this->validate_string($request->question);
					$answer = $this->validate_string($request->answer);
				 
	                App\Faqs::where('id', $id)->update(['question' => $question ,'answer' => $answer  ]);
	               
				    $result = @\App\Faqs::where('app_type' , $auth_app_type )->where('id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Faq Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
 
 
 
 
  // Route-7.4 ============================= delete a TimeSlots================================
   public function destroy(Request $request, $id)
   {
 
     
             //check existance of item with ID in items table
                $exist = $this->model_exist($id); 
                    if($exist == 0 or $exist == '0')
                    {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Faq with this ID does not exist';
                          $data['data']      =   [];
                          return $data;             
                    }
 
                      @\App\Faqs::where('id',$id)->delete();
 
 

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Faq Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
    
   }
 
 
 
 

















 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\Faqs::where('id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}