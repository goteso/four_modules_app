<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMetaValue extends Model
{
	
protected $fillable = ['item_meta_type_id','item_id','value','app_type'];
protected $table = 'item_meta_value';

		
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }



     public function getItemMetaTypeIdentifierAttribute($value) {
         return  @\App\ItemMetaType::where('item_meta_type_id',$this->item_meta_type_id)->first(['identifier'])->identifier;
    }

       public function getItemMetaTypeTitleAttribute($value) {
         return  @\App\ItemMetaType::where('item_meta_type_id',$this->item_meta_type_id)->first(['item_meta_type_title'])->item_meta_type_title;
    }
	
	

    
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
 
}
