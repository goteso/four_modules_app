<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsImages extends Model
{
        protected $fillable = [ 'item_id', 'photo' , 'app_type' ];
		protected $table = 'items_images';
		
		
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
 
protected $casts = [ 'item_id'=>'int'  ];


 
 
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}