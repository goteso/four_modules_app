<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingTax extends Model
{
        protected $fillable = [
        'title', 'percentage','app_type'
    ];

    protected $table ='setting_tax';
 



  public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
 
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }



    
}
