<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingOrderMetaType extends Model
{
        protected $fillable = [ 'setting_order_meta_type_title' , 'important','show_bool','required_or_not' , 'field_options' , 'identifier' , 'parent_identifier' , 'time_slot_days_difference' , 'input_type' , 'display_show_rule' , 'api', 'app_type' ];
		protected $table = 'setting_order_meta_type';
		
 






 
 
protected $casts = [ 'show_bool'=>'int' ];




	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}