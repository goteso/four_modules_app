<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class SettingOrdersStatus extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','color','label_colors','identifier','type','customer_notify', 'app_type'
    ];
	protected $table = 'setting_order_status';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
 
	
}
