<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMetaType extends Model
{
	
protected $fillable = ['item_meta_type_title','identifier','limit','type','app_type'];
protected $table = 'item_meta_type';
 
}
