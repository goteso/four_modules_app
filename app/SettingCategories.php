<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingCategories extends Model
{
        protected $fillable = [
        'title','parent_id','photo','store_id','status','app_type'
    ];
    protected $table = 'setting_categories';
 
protected $casts = [ 'status' => 'int' , 'store_id' => 'int'  ];


public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }



public function getParentCategoryTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$parent_id = @$this->parent_id;
 
        if($parent_id != 'null' && $parent_id != '' &&  $parent_id != '0' &&  $parent_id != 0)
        {
            $parent_category_title = @\App\SettingCategories::where('category_id',$parent_id)->first(['category_title'])->category_title;
        }
        else
        {
            $parent_category_title ='';
        }
        return $parent_category_title;
    }



    public function getStoreTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$store_id = @$this->store_id;
 
        if($store_id != 'null' && $store_id != '' &&  $store_id != '0' &&  $store_id != 0)
        {
            $store_title = @\App\Store::where('store_id',$store_id)->first(['store_title'])->store_title;
        }
        else
        {
            $store_title ='';
        }
        return $store_title;
    }




	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }



    
}
