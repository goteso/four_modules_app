<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreshdeskEnquiries extends Model
{
        protected $fillable = [ 'user_id', 'description'];
		protected $table = 'freshdesk_enquiries';
		


	protected $casts = [ 'id'=>'int' ];

	
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }



 public function getUserDetailsAttribute($value) {
         return  @\App\User::where('user_id',$this->user_id)->get(['user_id','first_name','last_name','email','phone']);
    }

 

 
 


 
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}