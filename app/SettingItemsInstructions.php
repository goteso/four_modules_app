<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingItemsInstructions extends Model
{
        protected $fillable = [
        'title','app_type'
    ];
 

protected $table = 'setting_items_instructions';


  public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
 public function getParentTagTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$parent_id = @$this->parent_id;
 
        if($parent_id != 'null' && $parent_id != '' &&  $parent_id != '0' &&  $parent_id != 0)
        {
            $tag_title = @\App\SettingTags::where('tag_id',$parent_id)->first(['tag_title'])->tag_title;
        }
        else
        {
            $tag_title ='';
        }
        return $tag_title;
    }
	



        public function getStoreTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$store_id = @$this->store_id;
 
        if($store_id != 'null' && $store_id != '' &&  $store_id != '0' &&  $store_id != 0)
        {
            $store_title = @\App\Store::where('store_id',$store_id)->first(['store_title'])->store_title;
        }
        else
        {
            $store_title ='';
        }
        return $store_title;
    }





 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }



    
}
