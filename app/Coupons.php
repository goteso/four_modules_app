<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
        protected $fillable = [ 'coupon_id', 'coupon_title', 'coupon_desc', 'coupon_image', 'coupon_code', 'discount','valid_from'
		,'expiry','max_discount','items_included','limit_total','limit_user','minimum_order_amount','maximum_order_amount','active_status','vendor_id','store_id','type', 'app_type'];
		protected $table = 'coupons';
      
		
		
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }


 
 
     public function getStoreTitleAttribute($value) {
         return  @\App\Store::where('store_id',$this->store_id)->first(['store_title'])->store_title;
    }
    
     public function getVendorNameAttribute($value) {
               return  @\App\User::where('user_id',$this->vendor_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$this->vendor_id)->first(['last_name'])->last_name;
    }
    
	
	protected $casts = [ 'discount' => 'float' , 'coupon_id' => 'float', 'max_discount' => 'float', 'limit_total' => 'float', 'limit_user' => 'float', 'minimum_order_amount' => 'float', 'maximum_order_amount' => 'float', 'vendor_id' => 'float', 'store_id' => 'float'  ];
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}