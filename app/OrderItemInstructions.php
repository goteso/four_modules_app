<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItemInstructions extends Model
{
    protected $fillable = [ 'order_items_instructions_id' , 'instruction_id', 'value' ];
		protected $table = 'order_item_instructions';
		
 
	
	  public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }



        public function getInstructionTitleAttribute($value) {
         return  \App\SettingItemsInstructions::where('instruction_id' , $this->instruction_id)->first(['title'])->title;
    }

  
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}