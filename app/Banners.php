<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
        protected $fillable = [ 'photo', 'type' , 'linked_id' , 'app_type'];
		protected $table = 'banners';
		
       protected $casts = [ 'linked_id'=>'int'    ];
				
				
                
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	
 
    public function getStoreTitleAttribute($value) {


  if($this->type == 'store')
  {
    $store_id = $this->linked_id;
  }

         return  @\App\Stores::where('store_id', $store_id)->first(['store_title'])->store_title;
    }


 
 public function getStoreDetailsAttribute($value) {

  if($this->type == 'store')
  {
    $store_id = $this->linked_id;
  }

  return  @\App\Stores::where('store_id', @$store_id)->get();

 }





 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}