<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsEmail extends Model
{
  protected $fillable = [ 'notification_type' , 'email_subject' , 'email_body' , 'user_type' , 'app_type'];
  protected $table = 'notifications_email';
}
