<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMetaValue extends Model
{
        protected $fillable = [ 'user_meta_value_id' , 'user_meta_type_id' , 'user_id', 'value' ];
		protected $table = 'user_meta_value';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) 
     {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
     }
	



    public function getUserMetaTypeTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$user_meta_type_id = @$this->user_meta_type_id;
 
        if($user_meta_type_id != 'null' && $user_meta_type_id != '' &&  $user_meta_type_id != '0' &&  $user_meta_type_id != 0)
        {
            $user_meta_type_title = @\App\UserMetaType::where('user_meta_type_id',$user_meta_type_id)->first(['title'])->title;
        }
        else
        {
            $user_meta_type_title ='';
        }
        return $user_meta_type_title;
    }


	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}