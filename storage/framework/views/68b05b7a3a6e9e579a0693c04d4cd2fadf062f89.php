<?php $__env->startSection('title', 'Tags' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin-laundry.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin-laundry.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            <?php echo $__env->make('admin-laundry.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         </div>
		   <?php          $auth_user_type = Auth::user()->user_type;   
                     $auth_user_id =   Auth::id();  
 
                 ?>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
			 <input type="file" id="file" style="display:none "/>
            <div class="tab-content"  ng-controller="tagsController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Tags</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>TAG NAME</th>
									   <th>IMAGE</th>
                                       <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.tags.data.data  ">
                                       <td>#{{values.tag_id}}</td>
                                       <td>{{values.tag_title}}</td>
									    <td><img ng-hide="!values.tag_photo" src="<?php echo e(URL::asset('/images/tags')); ?>/{{values.tag_photo}}" />
									     <img ng-show="!values.tag_photo" src="<?php echo e(URL::asset('/admin/assets/images/placeholder.jpg')); ?>" /></td>
                                       <td>{{values.created_at_formatted}}</td>
                                       <td>
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editTag(values, $index)" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteTagId(values.tag_id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
						    <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-show="ctrl.tags.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.tags.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="ctrl.tags.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.tags.data.prev_page_url);">Previous</button> 
                                          <span>{{ctrl.tags.data.current_page}}</span>
                                          <button class="btn" ng-show="ctrl.tags.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.tags.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.tags.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.tags.data.last_page_url);">Last</button> 
                                       </div>
									 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Tag</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
                       <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
						
						  <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name" style="display:none "/>
                           <br><br>			 
                           <div style="text-align: center;position: relative" id="image">
                              <img width="300px"  id="preview_image" src="<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>"/>
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Tag</h5>
                      <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.selectedItem" md-search-text-change="ctrl.searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedTagChange(data)" md-items="data in ctrl.tagSearch(searchText)" md-item-text="data.tag_title" md-min-length="0" placeholder="Select Parent Tag"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> {{data.tag_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
						<input type="hidden" id="tag_id" name="tag_id" ng-model="tag_id" value="{{tag_id}}" />
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"     id="tag_title" name="tag_title"/>
                     </md-input-container>
                      
					     <?php
                                          if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                            $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
                                           
										   <div class="row"   >
											  <div class="col-sm-12" ng-hide="<?php echo e($auth_user_type); ?>=='4'">
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="searchStoreChange(searchStore)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values, field)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													  <input type="hidden" id="store_id" name="store_id" ng-model="store_id"  value="<?php echo @$value;?>" />
                                                      <br> 
                                                   </div>
												  </div> 
                                       <br>
 
                                          <?php                                 
                                            }
                                        ?>
                     <md-button ng-click="ctrl.storeTag()" class="md-raised bg-color md-submit" style="bottom:0px">Add Tag</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
	
	
	
	
	
	 <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="edit" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Tag</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
                     <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
						
						  <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name1" style="display:none "/>
                           <br><br>			 
                           <div style="text-align: center;position: relative" id="image">
                              <img    class="img-responsive center-block"  id="preview_image1" ng-show="!ctrl.tag_photo" src="<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>"/>
							    <img    class="img-responsive center-block" id="preview_image2" ng-show="ctrl.tag_photo"  src="<?php echo e(asset('images/tags')); ?>/{{ctrl.tag_photo}}" style="padding:0px 20px;"/> 
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Tag</h5>
                     <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.parent_tag_title" md-search-text-change="ctrl.searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedEditTagChange(data)" md-items="data in ctrl.editTagSearch(searchText)" md-item-text="data.tag_title" md-min-length="0" placeholder="Select Parent Tag"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> {{data.tag_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
						<input type="hidden" id="tag_edit_id" name="tag_edit_id"  ng-model="ctrl.parent_id" value="{{ctrl.parent_id}}" />
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"  ng-model="ctrl.tag_title"  id="tag_edit_title" name="tag_edit_title"/>
                     </md-input-container>
                     <br>
                     <md-button ng-click="ctrl.updateTag(ctrl.tag_id)" class="md-raised bg-color md-submit">Update Tag</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers-laundry/tags.js')); ?>"></script> 

<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () { 
        if ($(this).val() != '') {
            upload(this); 
			alert(this);
        }
    });
    function upload(img) {
		alert('1');
		alert(img);
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-tags')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
					alert('s');
                    $('#preview_image').attr('src', '<?php echo e(asset('admin/assets/images/img-placeholder.png')); ?>');
                    alert(data.errors['file']);
                }
                else {
					 alert(data);
					 var data = data.filename;
					  alert(data);
                     $('#file_name').val(data);					 
                    $('#file_name1').val(data);
                    //$('#photo').val(data);
					$('#preview_image').attr('src', '<?php echo e(asset('images/tags')); ?>/'+data);
                    $('#preview_image2').attr('src', '<?php echo e(asset('images/tags')); ?>/'+data);
					$('#preview_image1').attr('src', '<?php echo e(asset('images/tags')); ?>/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/tags/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-tags/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>

<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-tags')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/tags/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#file_name').val(data);
                    $('#photo').val(data);
                    $('#preview_image').attr('src', '<?php echo e(asset('images/tags')); ?>/'+data);
					$('#preview_image1').attr('src', '<?php echo e(asset('images/tags')); ?>/'+data);

                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/tags/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-tags/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-laundry.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>