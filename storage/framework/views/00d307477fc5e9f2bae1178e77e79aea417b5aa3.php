<?php $__env->startSection('title', 'Logistics' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
<style>
   md-tabs, .nav-tabs.nav-tabs1>li>a{font-family:'Open Sans',sans-serif}
   .nav-tabs.nav-tabs1>li>a>i{color:#fff;padding-top:5px;}
   .nav-tabs.nav-tabs1>li.icon:hover>a, .nav-tabs.nav-tabs1>li.icon:active>a, .nav-tabs.nav-tabs1>li.icon.active>a, .nav-tabs.nav-tabs1>li.icon:focus>a {
   color: #fff!important; 
   border-bottom: 0px solid #fff!important;
   }
   .nav-tabs{background:#488de4;}
   .nav-tabs>li:hover>a, .nav-tabs>li:active>a, .nav-tabs>li.active>a, .nav-tabs>li:focus>a {
   color: #fff!important; 
   border-bottom: 2px solid #fff!important;
   }
   md-tabs .md-tab.md-active{color:#000!important;}
   .table>tbody>tr>td  {
   padding: 15px 5px 10px!important;
   font-size:15px !important;
   }
   md-tabs md-tabs-wrapper{background:#e9ecf0}
   .nav-tabs.nav-tabs1>li.icon>a{color:#fff;opacity:1;}
   .nav-tabs>li>a{color:#fff;opacity:0.6;}
   .nav-tabs>li:hover>a,.nav-tabs>li.active:hover>a, .nav-tabs>li.active>a{opacity:1;}
   .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
   } 
   .StepProgress {
   position: relative;
   padding-left: 20px;
   list-style: none;
   }
   .StepProgress::before {
   display: inline-block;
   content: '';
   position: absolute;
   top: 2px;
   left: 0px;
   width: 10px;
   height: 50%;
   border-left: 1px solid #CCC;
   }
   .StepProgress-item {
   position: relative;
   counter-increment: list;
   padding-left: 10px;
   }
   .StepProgress-item:not(:last-child) {
   padding-bottom: 10px;
   }
   .StepProgress-item-first::before {
   display: inline-block;
   content: '';
   position: absolute;
   left: -20px;
   height: 100%;
   width: 10px;
   top: 2px;
   }
   .StepProgress-item::after {
   content: '';
   display: inline-block;
   position: absolute;
   top: 0px;
   padding-top: 6px;
   left: -34px;
   width: 30px;
   height: 30px;
   border: 0px solid #CCC;
   border-radius: 50%;
   background-color: #D8E1ED;
   }
   .StepProgress-item.is-done::before {
   border-left: 2px dotted #D8E1ED;
   }
   .StepProgress-item.is-done::after {
   content: "P";
   padding-top: 6px;
   font-size: 12px;
   color: #FFF;
   text-align: center;
   border: 0px solid #488de4;
   background-color: #D8E1ED;
   }
   .unassigned .StepProgress-item.StepProgress-item-first.is-done::after {
   /*content: counter(list);*/
   content: "P";
   padding-top: 6px;
   width: 30px;
   height: 30px;
   top: 0px;
   left: -34px;
   font-size: 12px;
   text-align: center;
   color: #fff; 
   background-color: #D8E1ED; 
   border: 0px solid #488de4;
   }
   .StepProgress-item.current::before {
   border-left: 2px dotted #D8E1ED;
   }
   .StepProgress-item.current::after {
   /*content: counter(list);*/
   content: "D";
   padding-top: 6px;
   width: 30px;
   height: 30px;
   top: 2px;
   left: -35px;
   font-size: 12px;
   text-align: center;
   color: #fff;
   border: 0px solid #488de4;
   background-color: D8E1ED;
   }
   .completed .StepProgress-item.StepProgress-item-last.is-done::after { 
   content: "D"!important;
   font-size: 12px;
   color: #FFF;
   text-align: center;
   border: 0px solid #488de4;
   background-color: #D8E1ED;
   }
   strong {
   display: block;
   }
   md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
   .container-2{
   width: auto;
   vertical-align: middle;
   white-space: nowrap;
   position: absolute!important;
   right:0;
   }
   .container-2 input#search{
   margin-top:5px;
   width: 50px;
   height: 30px;
   background: #fff;
   border: none;
   font-size: 10pt;
   float: right;
   color: #262626;
   padding-right: 35px;
   -webkit-border-radius: 5px;
   -moz-border-radius: 5px;
   border-radius: 0px;
   color: #fff;
   opacity:0;
   -webkit-transition: width .55s ease;
   -moz-transition: width .55s ease;
   -ms-transition: width .55s ease;
   -o-transition: width .55s ease;
   transition: width .55s ease;
   }
   .container-2 input#search::-webkit-input-placeholder {
   color: #65737e;
   }
   .container-2 input#search:-moz-placeholder { /* Firefox 18- */
   color: #65737e;  
   }
   .container-2 input#search::-moz-placeholder {  /* Firefox 19+ */
   color: #65737e;  
   }
   .container-2 input#search:-ms-input-placeholder {  
   color: #65737e;  
   }
   .container-2 .icon{
   position: absolute;
   right:0;
   top: 10%;
   margin-left: 10px;
   margin-top: 10px;
   z-index: 1;
   color: #fff;
   }
   .container-2 input#search:focus, .container-2 input#search:active{
   outline:none;
   width: 150px;
   color: #65737e;  
   }
   .container-2:hover input#search{
   width: 150px;
   opacity:1;
   }
   .container-2:hover .icon{
   color: #fff;
   }
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin-laundry.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/pusher-angular@latest/lib/pusher-angular.min.js"></script>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->     
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin-laundry.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin-laundry.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none;" ></textarea>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">        
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content" id="tab-content"  >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header"> </h2>
                                 </div>
                              </div>
                              <div class="row" ng-controller="logisticsController" ng-cloak  style="display: flex; ">
                                 <div class="col-sm-6 col-lg-8" style="padding:0px">
                                    <div class="form-horizontal"  >
                                       <div id="dvMap" style="width:100%; height: 700px"></div>
                                       <!-- <locationpicker options="locationpickerOptions" style="width:100%;height:700px;" ></locationpicker>-->
                                       <label class="control-label"> </label>
                                       <div class="search-input" >
                                          <input type="hidden" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                       </div>
                                       <!--Map by element. Also it can be attribute-->
                                       <div class="m-t-small form-inline "> 
                                          <input type="hidden" class="form-control" id="us3-lat"  >,
                                          <input type="hidden" class="form-control"  id="us3-lon"  />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 col-lg-4"style="padding:0px" >
                                    <!--------------------------TAB hEADER STRATS HERE-------------------------------->
                                    <ul class="nav nav-tabs" >
                                       <li class="active"><a href="#tasks" data-toggle="tab" ng-click="tasks();">Tasks</a></li>
                                       <li class=""><a href="#drivers" data-toggle="tab"  ng-click="drivers();">Drivers</a></li>
                                       <li class="icon plus-icon" style="position:absolute;right:0px;padding-right:0px;top:3px;"><a href="driver" ><i class="fa fa-plus"></i></a></li>
                                       <li class="container-2"> <span class="icon"><i class="fa fa-search"></i></span>
                                          <input type="search" id="search" placeholder="Search..." ng-model='searchText'/> 
                                       </li>
                                    </ul>
                                    <!--------------------------TAB HEADER ENDS HERE-------------------------------->
                                    <div class="tab-content  tab-content-data" >
                                       <!-------------------------task panel starts here--------------->
                                       <div id="tasks" class="tab-pane fade in active" >
                                          <div class=" user-info" >
                                             <md-content class=" ">
                                                <md-tabs    >
                                                   <!------------------------UNASSIGNED TASK SECTION STARTS HERE--------------------------->
                                                   <md-tab id="tab2" ng-click="unassignedTask()" >
                                                      <md-tab-label>Pending</md-tab-label>
                                                      <md-tab-body >
                                                         <div  class="container-fluid" style="padding:10px 25px;background:#f5f5f5;">
                                                            <div class="row" ng-repeat="values in unassigned.data.data | filter:searchText "  style="border:1px solid #e5e5e5;margin-bottom:10px;padding:8px 3px 0px;background:#fff;">
                                                               <button ng-click="getMap(values)" class="btn" style="background: #498CE3;padding: 2px 10px; color:#fff;border-radius: 0px;  margin-left: 5px;">Route</button>
                                                               <!--<button ng-click="getDriverLocation(values)">GetDriver</button>-->
                                                               <div class="col-sm-4" style="padding-left:5px;padding-right:0px; ">
                                                                  <h5 class="text-left text-uppercase" style="margin-top:0px ;background:#486490;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;"><a href="order_detail-laundry/{{values.order_id}}" style="color:#fff;text-decoration:none;">Order #{{values.order_id}}</a></h5>
                                                               </div>
                                                               <div class="col-sm-8" style="padding-right:7px">
                                                                  <div style="display:flex;float:right" ng-hide="!values.driver_details[0].first_name">
                                                                     <a href="#" style="top:-20px;  position:absolute;  right: 7px;z-index:10;color:#466491" ng-click="getDriver(values)"  >Re-assign</a> 
                                                                     <img class="" src="products/{{values.photo}}" style="height:60px;width:60px;padding:12px;border-radius:50px;background:#466491!important;float:right"  ng-hide="!values.photo"> 
                                                                     <div>
                                                                        <a  ng-click="getDriverLocation(values)">
                                                                           <p class="text-center" style="height:60px;width:60px;padding:12px;border-radius:50%;background:#466491!important;float:right;color:#fff;font-size:25px;cursor:pointer"  ng-hide="!values.driver_details[0].first_name">{{values.driver_details[0].first_name |limitTo:1}}</p>
                                                                           <!-- <img class="img-responsive text-right" style="height:60px;width:60px;padding:12px;border-radius:50%;background:#466491!important;float:right" src="../admin/assets/images/boy.png" ng-hide="!values.driver_details[0].first_name">--> 
                                                                        </a>
                                                                        <md-tooltip
                                                                           md-direction="left" style="background:#eaedf3;height:auto;padding:0px 5px 0;">
                                                                           <span style="background:#fff; ">
                                                                              <p  class="text-left" style="color:#466491;margin-bottom:0px; font-size:14px;">{{values.driver_details[0].first_name}}</p>
                                                                              <p style="margin-bottom:0px;margin-top:0px;color:#a5a5a5;font-size:13px;"><small style="color:#a5a5a5;">{{values.driver_details[0].phone}}</small></p>
                                                                           </span>
                                                                        </md-tooltip>
                                                                        <p class="text-center" style="font-size:13px;position: absolute;   right: 15; top: 65px;">{{values.driver_details[0].first_name}}</p>
                                                                     </div>
                                                                  </div>
                                                                  <p class="text-right" ng-show="!values.driver_details[0].first_name"style="margin:0px;">
                                                                     <md-button class="btn md-raised text-center" ng-click="getDriver(values)" style="padding:14px 8px 10px; border-radius:50%; font-size:12px;margin:0px;background:#466491!important; color:#A5B4C9;line-height:20px;min-height:60px;min-width:60px">Assign <br>Now</md-button>
                                                                  </p>
                                                               </div>
                                                               <div class="col-sm-12">
                                                                  <ul class="StepProgress unassigned" style="margin-bottom:0px;bottom:40px; margin-left:15px; margin-bottom: -10px;">
                                                                     <li class="StepProgress-item-first StepProgress-item is-done">
                                                                        <p class="time"> {{values.pickup_time}}</p>
                                                                        <p class="contact-title">{{values.pickup_contact_title}} </p>
                                                                        <p class="address">{{values.pickup_address}}</p>
                                                                     </li>
                                                                     <li class="StepProgress-item current">
                                                                        <p class="time"> {{values.dropoff_time}}</p>
                                                                        <strong class="contact-title">{{values.dropoff_contact_title}} </strong>
                                                                        <p ng-show="values.dropoff_address_string" class="address">{{values.dropoff_address_string}}</p>
                                                                        <!-- <br ng-show="values.dropoff_address_string == ' ' && values.dropoff_time == ''" /> -->
                                                                        <br ng-show="values.dropoff_address_string == ' ' && values.dropoff_contact_title == ''" />
                                                                        <br ng-show="values.dropoff_contact_title == '' && values.dropoff_time == ''" />
                                                                     </li>
                                                                  </ul>
                                                                  <img style="right: 6px; bottom: 8px;  width: 25px; position: absolute;" src="../admin/assets/images/corner-logistics.png">
                                                                  <br ng-hide="values.dropoff_address_string || values.dropoff_time">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </md-tab-body>
                                                   </md-tab>
                                                   <!------------------------UNASSIGNED TASK SECTION ENDS HERE--------------------------->
                                                   <!------------------------ASSIGNED TASK SECTION STARTS HERE--------------------------->
                                                   <md-tab id="tab1" ng-click="assignedTask()">
                                                      <md-tab-label>In Progress</md-tab-label>
                                                      <md-tab-body>
                                                         <div  class="container-fluid" style="padding:10px 25px;background:#f5f5f5;">
                                                            <div class="row" ng-repeat="values in assigned.data.data | filter:searchText "  style="border:1px solid #e5e5e5;margin-bottom:10px;padding:8px 3px 0px;background:#fff;">
                                                              <button ng-click="getMap(values)" class="btn" style="background: #498CE3;padding: 2px 10px; color:#fff;border-radius: 0px;  margin-left: 5px;">Route</button>
                                                               <div class="col-sm-4" style="padding-left:5px;padding-right:0px; ">
                                                                  <h5 class="text-left text-uppercase" style="margin-top:0px ;background:#486490;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;"><a href="order_detail-laundry/{{values.order_id}}" style="color:#fff;text-decoration:none;">Order #{{values.order_id}}</a></h5>
                                                               </div>
                                                               <div class="col-sm-8" style="padding-right:7px">
                                                                  <div style="display:flex;float:right" ng-hide="!values.driver_details[0].first_name">
                                                                     
                                                                    <img class="" src="products/@{{values.photo}}" style="height:60px;width:60px;padding:12px;border-radius:50px;background:#466491!important;float:right"  ng-hide="!values.photo"> 
                                                                     <div>
                                                                         <a  ng-click="getDriverLocation(values)">
                                                                           <p class="text-center" style="height:60px;width:60px;padding:12px;border-radius:50%;background:#466491!important;float:right;color:#fff;font-size:25px;cursor:pointer"  ng-hide="!values.driver_details[0].first_name">{{values.driver_details[0].first_name |limitTo:1}}</p> 
                                                                        </a>
                                                                        <md-tooltip
                                                                           md-direction="left" style="background:#eaedf3;height:auto;padding:4px 6px;">
                                                                           <span style="background:#fff; ">
                                                                              <p  class="text-left" style="color:#466491;margin-bottom:0px; font-size:14px;"> {{values.driver_details[0].first_name}} </p>
                                                                              <p style="margin-bottom:0px;margin-top:0px;color:#a5a5a5;font-size:13px;"><small style="color:#a5a5a5;">{{values.driver_details[0].phone}}</small></p>
                                                                           </span>
                                                                        </md-tooltip>
                                                                         <p  class="text-center"style="font-size:13px;position: absolute; right: 15;   top: 65px;">{{values.driver_details[0].first_name}}</p>
                                                                     </div> 
                                                                  </div>
                                                                  <p class="text-right" ng-show="!values.driver_details[0].first_name" style="margin:0px;">
                                                                     <md-button class="btn md-raised text-center" ng-click="getDriver(values)" style="padding:14px 8px 10px; border-radius:50%; font-size:12px;margin:0px;background:#466491!important; color:#A5B4C9;line-height:20px;min-height:60px;min-width:60px">Assign <br>Now</md-button>
                                                                  </p>
                                                               </div>
                                                               <div class="col-sm-12">
                                                                  <ul class="StepProgress " style="margin-bottom:0px;bottom:20px; margin-left:15px; margin-bottom: -10px;">
                                                                     <li class="StepProgress-item-first StepProgress-item is-done">
                                                                        <img style="left:-20px; top: 15px; z-index: 10;  width: 30px;  position: absolute;" src="../admin/assets/images/tick-logistics.png" ng-show="values.status == '2'">
                                                                        <p class="time"> {{values.pickup_time}}</p>
                                                                        <p class="contact-title">{{values.pickup_contact_title}} </p>
                                                                        <p class="address">{{values.pickup_address}}</p>
                                                                     </li>
                                                                     <li class="StepProgress-item current">
                                                                        <p class="time"> {{values.dropoff_time}}</p>
                                                                        <strong class="contact-title">{{values.dropoff_contact_title}} </strong>
                                                                        <p  class="address">{{values.dropoff_address_string}}</p>
                                                                        <!-- <br ng-show="values.dropoff_address_string == ' ' && values.dropoff_time == ''" /> -->
                                                                        <br ng-show="values.dropoff_address_string == ' ' && values.dropoff_contact_title == ''" />
                                                                        <br ng-show="values.dropoff_contact_title == '' && values.dropoff_time == ''" />
                                                                     </li>
                                                                  </ul>
                                                                  <img style="right: 6px; bottom: 8px;  width: 25px; position: absolute;" src="../admin/assets/images/corner-logistics.png">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </md-tab-body>
                                                   </md-tab>
                                                   <!------------------------ASSIGNED TASK SECTION ENDS HERE--------------------------->
                                                   <!------------------------COMPLETED TASK SECTION STARTS HERE--------------------------->
                                                   <md-tab id="tab3" ng-click="completedTask()">
                                                      <md-tab-label>Completed</md-tab-label>
                                                      <md-tab-body>
                                                         <div  class="container-fluid" style="padding:10px 25px;background:#f5f5f5;">
                                                            <div class="row" ng-repeat="values in completed.data.data | filter:searchText "  style="border:1px solid #e5e5e5;margin-bottom:10px;padding:8px 3px 0px;background:#fff;">
                                                              <button ng-click="getMap(values)" class="btn" style="background: #498CE3;padding: 2px 10px; color:#fff;border-radius: 0px;  margin-left: 5px;">Route</button>
                                                               <div class="col-sm-4" style="padding-left:5px;padding-right:0px; ">
                                                                  <h5 class="text-left text-uppercase" style="margin-top:0px ;background:#486490;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;"><a href="order_detail-laundry/{{values.order_id}}" style="color:#fff;text-decoration:none;">Order #{{values.order_id}}</a></h5>
                                                               </div>
                                                               <div class="col-sm-8" style="padding-right:7px">
                                                                  <div style="display:;float:" ng-hide="!values.driver_details[0].first_name">
                                                                    
                                                                    <img class="" src="products/@{{values.photo}}" style="height:60px;width:60px;padding:12px;border-radius:50px;background:#466491!important;float:right"  ng-hide="!values.photo"> 
                                                                     <div>
                                                                         <a  ng-click="getDriverLocation(values)">
                                                                           <p class="text-center" style="height:60px;width:60px;padding:12px;border-radius:50%;background:#466491!important;float:right;color:#fff;font-size:25px;cursor:pointer"  ng-hide="!values.driver_details[0].first_name">{{values.driver_details[0].first_name |limitTo:1}}</p> 
                                                                        </a>
                                                                        <md-tooltip
                                                                           md-direction="left" style="background:#eaedf3;height:auto;padding:4px 6px;">
                                                                           <span style="background:#fff; ">
                                                                              <p  class="text-left" style="color:#466491;margin-bottom:0px; font-size:14px;"> {{values.driver_details[0].first_name}} </p>
                                                                              <p style="margin-bottom:0px;margin-top:0px;color:#a5a5a5;font-size:13px;"><small style="color:#a5a5a5;">{{values.driver_details[0].phone}}</small></p>
                                                                           </span>
                                                                        </md-tooltip>
                                                                         <p class="text-center" style="font-size:13px;position: absolute;   right: 15;   top: 65px;">{{values.driver_details[0].first_name}}</p>
                                                                     </div> 
                                                                  </div>
                                                                  <p class="text-right" ng-show="!values.driver_details[0].first_name" style="margin:0px;">
                                                                     <md-button class="btn md-raised text-center" ng-click="getDriver(values)" style="padding:14px 8px 10px; border-radius:50%; font-size:12px;margin:0px;background:#466491!important; color:#A5B4C9;line-height:20px;min-height:60px;min-width:60px">Assign <br>Now</md-button>
                                                                  </p>
                                                               </div>
                                                               <div class="col-sm-12">
                                                                  <ul class="StepProgress completed" style= "margin-bottom:0px;bottom:40px; margin-left:15px; margin-bottom: -10px;">
                                                                     <li class="StepProgress-item-first StepProgress-item is-done">
                                                                        <img style="left: -20px; top: 15px; z-index: 10;  width: 30px;  position: absolute;" src="../admin/assets/images/tick-logistics.png">
                                                                        <p class="time"> {{values.pickup_time}}</p>
                                                                        <p class="contact-title">{{values.pickup_contact_title}} </p>
                                                                        <p class="address">{{values.pickup_address}}</p>
                                                                     </li>
                                                                     <li class="StepProgress-item is-done StepProgress-item-last">
                                                                        <img style="left:-20px; top: 15px; z-index: 10;  width: 30px;  position: absolute;" src="../admin/assets/images/tick-logistics.png">
                                                                        <p class="time"> {{values.dropoff_time}}</p>
                                                                        <strong class="contact-title">{{values.dropoff_contact_title}} </strong>
                                                                        <p  class="address">{{values.dropoff_address_string}}</p>
                                                                        <br ng-show="values.dropoff_address_string == ' ' && values.dropoff_contact_title == ''" />
                                                                        <br ng-show="values.dropoff_contact_title == '' && values.dropoff_time == ''" />
                                                                     </li>
                                                                  </ul>
                                                                  <img style="right: 6px; bottom: 8px;  width: 25px; position: absolute;" src="../admin/assets/images/corner-logistics.png">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </md-tab-body>
                                                   </md-tab>
                                                   <!------------------------COMPLETED TASK SECTION ENDS HERE--------------------------->
                                                </md-tabs>
                                             </md-content>
                                          </div>
                                       </div>
                                       <!----------------task panel ends here--------------------------->
                                       <div id="drivers" class="tab-pane fade"  >
                                          <md-content style="background:#fff;">
                                             <md-list style=";height:650px;">
                                                <md-list-item class="md-3-line"  ng-repeat="values in driver.data.data  ">
                                                   <p class="text-center" style="margin-top:17px;margin-right:5px;padding: 10px 20px;border-radius:50%;background:#466491!important; color:#fff;font-size:25px;cursor:pointer;flex:0;"   >{{values.first_name |limitTo:1}}</p>
                                                   <div class="md-list-item-text">
                                                      <h4 style="font-size:15px;"><a href="<?php echo e(URL::asset('driver-profile-laundry')); ?>/{{values.user_id}}" > {{values.first_name}} {{values.last_name}} </a> </h4>
                                                      <p>{{values.phone}}</p>
                                                   </div>
                                                   <div class="md-secondary md-list-item-text">
                                                      <p  class="text-center" style="border-radius: 50px;  height: 30px;  width: 30px;  border: 1px solid rgba(0,0,0,0.12);  padding: 3px;margin:0px;color:#a1c6de"><b>{{values.total_tasks}}</b></p>
                                                      <p>Tasks</p>
                                                   </div>
                                                   <div class="md-secondary md-list-item-text" style="padding:0 10px;"> 
                                                      <a href="driver-laundry/{{values.user_id}}"> <i class="fa fa-edit"></i></a>
                                                   </div>
                                                   <md-divider  ng-if="!$last"></md-divider>
                                                </md-list-item>
                                             </md-list>
                                          </md-content>
                                       </div>
                                    </div>
                                    <!--   <div ng-view></div>-->
                                 </div>
                                 <!-----------------------------------------------DRIVER ASSIGN MODAL STARTS HERE-------------------------------------------------------------------->
                                 <div id="driverModal" class="modal fade" data-backdrop="false" style="z-index:999" role="dialog">
                                    <div class="modal-dialog modal-lg category-add"  >
                                       <!-- Modal content-->
                                       <div class="modal-content "   >
                                          <div class="modal-header  "  >
                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                             <h4 class="modal-title text-uppercase"  >Assign <b> Driver</b></h4>
                                          </div>
                                          <div class="modal-body "  >
                                             <div class="row"   >
                                                <div class="col-sm-7">
                                                   <h5>Select Driver</h5>
                                                   <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedItem" md-search-text-change="searchDriverChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedDriverChange(data)" md-items="data in driverSearch(searchText)" md-item-text="data.first_name" md-min-length="0" placeholder="Select Driver"  md-menu-class="autocomplete-custom-template">
                                                      <md-item-template>
                                                         <span class="item-title"> 
                                                         <span> {{data.first_name}} </span> 
                                                         </span> 
                                                      </md-item-template>
                                                   </md-autocomplete>
                                                   <input type="hidden" id="driver_id" name="driver_id" ng-model="driver_id" value="{{driver_id}}" />
                                                   <br><br><br><br> 
                                                </div>
                                                <div class="col-sm-5">
                                                   <md-button ng-click="assignDriver()" class="md-raised bg-color md-submit">Assign</md-button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-----------------------------------------------DRIVER ASSIGN MODAL ENDS HERE-------------------------------------------------------------------->
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers-laundry/logistics.js')); ?>"></script> 
<script>
   $(function() {
   
   $("#wrapper")
   .width($(window).width()).height($(window).height());
   });
</script>
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-laundry.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>