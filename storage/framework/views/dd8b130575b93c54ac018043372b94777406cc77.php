 
 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu"> 
               
               <?php @$auth_user_type = @Auth::user()->user_type;   
                     @$auth_user_id =   @Auth::id();
                     App::setLocale('settings');  
                     if($auth_user_id =='' || $auth_user_id == null)
                     {
                           Auth::logout();
                           echo '<script>document.getElementById("logout").click();</script>';
                     }

                     if($auth_user_type == '4')
                     {
                     	$auth_store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                     }
                     else
                     {
                     	$auth_store_id = '';
                     }
                ?>
                  <input type="hidden" id="auth_user_type" value="<?php echo $auth_user_type;?>">
                  <input type="hidden" id="auth_user_id" value="<?php echo $auth_user_id;?>">
                  <input type="hidden" id="auth_store_id" value="<?php echo $auth_store_id;?>">
                  <input type="hidden" id="auth_app_type" value="grocery">

   
				    <li><a href="<?php echo e(URL::to('/v1/dashboard_data-grocery')); ?>"><i class="fas fa-tachometer-alt"></i><span>Dashboard  </span></a></li>
 	          
        			<li><a href="<?php echo e(URL::to('v1/orders-grocery')); ?>"><i class="fa fa-shopping-cart"></i><span>Orders</span></a></li>
				 
			        <li><a href="<?php echo e(URL::to('v1/logistics-grocery')); ?>"><i class="fa fa-truck"></i><span>Logistics</span></a></li>
		          
				    <li><a href="<?php echo e(URL::to('v1/items-grocery')); ?>"><i class="fa fa-list"></i><span>Items</span></a></li>
				  
				    <li style="display:none"><a href="<?php echo e(URL::to('v1/stores-grocery')); ?>"><i class="fa fa-home"></i><span>Stores</span></a></li>
				    
					<li ><a href="<?php echo e(URL::to('v1/coupons-grocery#/')); ?>"><i class="fas fa-ticket-alt"></i><span>Coupons</span></a></li>
				  
				    <li><a href="<?php echo e(URL::to('v1/customers-grocery')); ?>"><i class="fa fa-user"></i><span>Customers</span></a></li>
				  
				    <li><a href="<?php echo e(URL::to('v1/reviews-grocery')); ?>"><i class="fa fa-home"></i><span>Reviews</span></a></li>
				  
				    <li style="display:none"><a href="<?php echo e(URL::to('v1/order-cancel-reasons-grocery')); ?>"><i class="fa fa-question-circle"></i><span>Order Cancel Reasons</span></a></li>
 
				   <li  style="padding-right:0;margin-left:0;display:none">
                     <a href="javascript:;"><i class="fa fa-bell"></i> <span>Send Notifications</span></a>	
		                     <ul class="acc-menu" style="padding-right:0;margin-left:0">
		                                 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/emails-to-customers-grocery')); ?>"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Customers</span></a></li>
						    			 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/push-to-customers-grocery')); ?>"><i style="font-size:14px" class="fa fa-bell"></i><span style="font-size:14px"> Push Notifications</span></a></li>
						   				 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/emails-to-stores-grocery')); ?>"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Stores</span></a></li>
		                     </ul>
                  </li>
                 
                  <li style="display:none"><a href="<?php echo e(URL::to('v1/reports-grocery')); ?>"><i class="fa fa-file"></i><span>Reports</span></a></li>
 
			 
				    <li><a href="<?php echo e(URL::to('settings-grocery')); ?>"><i class="fa fa-wrench"></i><span>Settings</span></a></li>
					
					 
					<!--- <li style="display:none"><a href="<?php echo e(URL::to('v1/store-requests-grocery')); ?>"><i class="fa fa-wrench"></i><span>Approval Requests</span></a></li> -->
				 

               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>